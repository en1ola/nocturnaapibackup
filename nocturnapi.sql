-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: nocturnapi
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_login`
--

DROP TABLE IF EXISTS `admin_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_login`
--

LOCK TABLES `admin_login` WRITE;
/*!40000 ALTER TABLE `admin_login` DISABLE KEYS */;
INSERT INTO `admin_login` VALUES (1,'admin','123');
/*!40000 ALTER TABLE `admin_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `packageID` int(11) NOT NULL,
  `status` enum('pending','purchased','avail') NOT NULL,
  `qr` varchar(200) NOT NULL,
  `type` enum('myself','friend','split') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (1,8,1,'pending','','myself'),(2,7,1,'avail','1487751568_2.png','myself'),(4,19,3,'purchased','1491213461_1.png','friend'),(5,19,3,'purchased','1491213461_2.png','friend'),(13,40,4,'pending','','myself'),(17,101,5,'pending','','myself');
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `statusid` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,8,4,'anji.com '),(2,8,4,'anji.com '),(3,8,4,'anji.com '),(4,8,4,'anji.com '),(5,8,4,'anji.com '),(6,8,4,'anji.com '),(7,8,4,'anji.com '),(8,8,4,'anji.com '),(9,8,4,'anji.com '),(10,8,4,'anji.com '),(11,8,4,'anji.com '),(12,8,4,'anji.com '),(13,8,4,'anji.com '),(14,8,4,'anji.com '),(15,8,4,'anji.com '),(16,8,4,'anji.com '),(17,8,4,'anji.com '),(18,8,4,'anji.com '),(19,8,4,'anji.com '),(20,8,4,'anji.com '),(21,8,4,'anji.com '),(22,8,4,'anji.com '),(23,8,4,'anji.com '),(24,8,4,'anji.com 123 '),(25,8,4,'anji.com 123 I was a little while, so I\'m sure '),(26,8,4,'anji.com 123 I was a little while, so I\'m sure '),(27,8,4,'anji.com 123 I was a little while, so I\'m sure '),(28,8,4,'anji.com 123 I was a little while, so I\'m sure '),(29,8,4,'anji.com 123 I was a little while, so I\'m sure '),(30,8,4,'anji.com 123 I was a little while, so I\'m sure '),(31,8,4,'anji.com 123 I was a little while, so I\'m sure '),(32,8,4,'anji.com 123 I was a little while, so I\'m sure '),(33,8,4,'anji.com 123 I was a little while, so I\'m sure '),(34,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and '),(35,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and '),(36,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and '),(37,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and '),(38,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and '),(39,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and '),(40,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and we are a couple '),(41,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and we are a couple '),(42,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and we are a couple '),(43,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and we are a couple '),(44,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and we are a couple '),(45,8,4,'anji.com 123 I was a little while, so I\'m sure it was great and we are a couple '),(46,40,7,'Abi na hospital?'),(47,40,7,'Abi na hospital?'),(48,40,7,'Abi na hospital?'),(49,40,7,'Abi na hospital?'),(50,12,13,'I know this place...'),(51,12,13,'I know this place...'),(52,12,14,'Nice place'),(53,95,18,'nice'),(54,101,21,'Good but not good.'),(55,101,22,'hmmmm');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedBack`
--

DROP TABLE IF EXISTS `feedBack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedBack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `feedback` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedBack`
--

LOCK TABLES `feedBack` WRITE;
/*!40000 ALTER TABLE `feedBack` DISABLE KEYS */;
INSERT INTO `feedBack` VALUES (1,12,'Add a \'share-the-bill\' feature'),(2,30,'Type Your Suggestions..');
/*!40000 ALTER TABLE `feedBack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `friendID` int(11) NOT NULL,
  `status` enum('pending','accept') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (1,2,1,'pending'),(2,3,2,'pending'),(3,3,1,'pending'),(4,4,2,'pending'),(5,4,3,'pending'),(6,6,4,'pending'),(7,6,3,'pending'),(9,8,6,'pending'),(10,8,3,'pending'),(11,8,2,'pending'),(12,8,5,'pending'),(13,8,4,'pending'),(14,8,1,'pending'),(15,8,7,'accept'),(16,10,8,'pending'),(17,12,19,'accept'),(18,12,18,'pending'),(19,12,17,'accept'),(20,31,28,'pending'),(21,31,30,'pending'),(22,31,24,'pending'),(23,31,29,'pending'),(24,22,36,'pending'),(25,37,12,'accept'),(26,37,17,'pending'),(27,12,13,'pending'),(28,12,23,'pending'),(29,12,26,'pending'),(30,12,27,'pending'),(31,12,28,'pending'),(32,12,29,'pending'),(33,12,36,'pending'),(34,40,12,'pending'),(35,12,30,'pending'),(36,40,13,'pending'),(37,40,19,'pending'),(38,12,31,'pending'),(39,58,12,'pending'),(40,58,19,'pending'),(41,58,20,'pending'),(42,58,18,'pending'),(43,58,21,'pending'),(44,58,22,'pending'),(45,58,23,'pending'),(46,58,17,'pending'),(47,71,70,'pending'),(48,71,12,'pending'),(49,12,35,'pending'),(50,12,53,'pending'),(51,12,56,'pending'),(52,12,59,'pending'),(53,12,57,'pending'),(54,12,63,'pending'),(55,12,70,'pending'),(56,12,76,'pending'),(57,12,77,'pending'),(58,12,78,'pending'),(59,12,79,'pending'),(60,12,81,'pending'),(61,12,80,'pending'),(62,82,12,'pending'),(63,82,17,'pending'),(64,82,29,'pending'),(65,82,30,'pending'),(66,82,31,'pending'),(67,101,7,'pending'),(68,101,13,'pending'),(69,101,14,'pending'),(70,101,39,'pending');
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `statusid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (1,1,1),(2,1,2),(3,1,4),(4,1,7),(5,12,8),(6,1,8),(7,1,13),(8,1,11),(9,1,6),(10,12,14),(11,1,18),(12,1,20),(13,101,21),(14,101,22),(15,101,24);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `normaluser`
--

DROP TABLE IF EXISTS `normaluser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `normaluser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `googlePlus` varchar(200) NOT NULL,
  `faceBook` varchar(200) NOT NULL,
  `userName` varchar(50) NOT NULL DEFAULT ' ',
  `fullName` varchar(50) NOT NULL DEFAULT ' ',
  `age` int(11) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `city` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT ' ',
  `password` varchar(100) NOT NULL DEFAULT ' ',
  `phoneNumber` varchar(50) NOT NULL,
  `intrest` varchar(100) NOT NULL DEFAULT ' ',
  `dis` varchar(300) NOT NULL DEFAULT ' ',
  `deviceToken` varchar(400) NOT NULL DEFAULT ' ',
  `typeDevice` varchar(20) NOT NULL DEFAULT ' ',
  `profilePicture` varchar(200) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `normaluser`
--

LOCK TABLES `normaluser` WRITE;
/*!40000 ALTER TABLE `normaluser` DISABLE KEYS */;
INSERT INTO `normaluser` VALUES (1,'','','testuserapple','testappleuser',0,'','','testapple@user.com','12345','',' ',' ','cc25cf331904c4590027b77b56b87461142efb141f0d7e9bf9156e2bacc5dc0d','ios','961062067.jpg','2017-04-05 12:23:48'),(2,'','','flyflyerson@gmail.com','Fly',0,'','',' ','apple123','',' ',' ','8df646fcf31ca8355d54f0235cb909791c08fefa6cf044421c69a08f546f5d01','ios','','2017-04-05 12:23:48'),(3,'','','flyflyerson2','Fly Flyerson',0,'','',' ','apple123','',' ',' ','dmWsl-vEFv4:APA91bEkWZRQ3eOfPs1T_yQ6wMNcGhFNQyfeYs_lp_ipuf18KpjPiFhV4-9GVW-wdTi7cn_andUHFsLqRveLh_wYsybXVXZ6ZbRiVRP4q-RreXEDgjGZZsOW2cLV1u5hxu6ydvFkNv1k','android','','2017-04-05 12:23:48'),(4,'','','flyflyerson','fly flyerson',0,'','',' ','apple123','',' ',' ','8df646fcf31ca8355d54f0235cb909791c08fefa6cf044421c69a08f546f5d01','ios','','2017-04-05 12:23:48'),(5,'','','anji.com ','anji.com ',0,'','','industry@test.com','123','',' ',' ','eCdmaXbLyNU:APA91bGAo6lGZKT144OR7yIn6DTpob3qZ1qoIyKikAymQI8Ww1fpcsHD-03EF0kfHULTSdMaUFwmjX4VrkVHE7e1M99kdR7_0EFbfg30FMDayk4XaLzMER1PMzBqne049omNUCMR7y5Q','android','1275742531.jpg','2017-04-05 12:23:48'),(6,'','','anji.com123','anji.com ',0,'','','industry@test.com','123','',' ',' ','d6rnnjg_tPg:APA91bFo86rcaIDR05NiU1c60oaSXUY_uPrFtEigj227tIDbXU2QBey2AXgxobIpnNNO54tsr2BThiT7N3ZnDPlld2oAVKgoUzqnnkOAhJ4eldpMj3OZYImcLahUVKAcx2pu32koT1aZ','android','731101662.jpg','2017-04-05 12:23:48'),(7,'','','abc','test',0,'','','t@t.com','test','',' ',' ','cLTqnFEs8xM:APA91bHeDX1qZoDuMqPdwB6XNwExHNs2oujIYKIXpRamb7n0LVdvxCDBXfedoGTyz1ciry9zUCUX5n0Y_tJMLNd7gQEVz23CccLtnacFYAphQx3JMAz-yq8IaQ_hBVab-h3llW5Y1FKE','android','860205369.jpg','2017-04-05 12:23:48'),(8,'','','zz','zz',0,'','','zz@gmail.com','123','',' ',' ','eCdmaXbLyNU:APA91bGAo6lGZKT144OR7yIn6DTpob3qZ1qoIyKikAymQI8Ww1fpcsHD-03EF0kfHULTSdMaUFwmjX4VrkVHE7e1M99kdR7_0EFbfg30FMDayk4XaLzMER1PMzBqne049omNUCMR7y5Q','android','1030908202.jpg','2017-04-05 12:23:48'),(9,'','','ehmar','EhmarMemon Ehmar',0,'','','ehmar@gmail.com','abcdefgh','',' ',' ','37bdb64f412b700de037e772b0987fdf299bf2b29f7e9396f032d2a5bd721e4d','ios','','2017-04-05 12:23:48'),(10,'','','testmtr','tahidn',0,'','','test@t.com','123','',' ',' ','eK6VgWE7-pE:APA91bFtkY5Mv6wO2SgLG1ZqvUv3YZ9itJWN3KMs3yG8cT07ssjoacxUAVH0aLckI1ulohx_IWAwyjfm6aTbB7iIlYh_NQZv0kG-JDhIXf7uFO9yaLVlF-vDqtjqV11y0oswS41bhLeO','android','1147862060.jpg','2017-04-05 12:23:48'),(11,'','','mtsidd','hhyhd',0,'','','t@t.com','123','',' ',' ','ducoF4E613E:APA91bFKk67WHNVLcZaogNkrAD83mS3CYk6LoHeMbMAWD4BUkeTI9yP17YtqgL8uqYs2K0kJzy7Wz5gB0hgIR0jjA9_iCViRmeGrhZSKB7Z8YYi84ez6gHfoFnEDoTy3hnVZz-O-HBW_','android','2030451106.jpg','2017-04-05 12:23:48'),(12,'','','agujoe','Agujoe',0,'','','agujagu@yahoo.com','sweetchics','',' innovating while lounging......','','fJ5nlFwIeD0:APA91bH6cqOzMp4hoAWTIiUBqaBB7GMZjTGlSpysMCw0ki-T0EWaQU56Mu03WJVYZrtMPiKPqadLmBRn_VcuHg9X_Wa4osbg1xDvmJN6XRB7p61wt2IwKtZkVVokoZuGtrjp_tNpqoRQ','android','1845613723.jpg','2017-04-05 12:23:48'),(13,'','','DEE','Douglas',0,'','','denahoro@gmail.com','douggy33','','fun','Lagos knows my name','3704d78d6e6a93edfd11143bda1df82434670669a1b899f0e85bdaca8c5b1b30','ios','1283707097.jpg','2017-04-05 12:23:48'),(14,'','','a1','a1',0,'','','a@gmail.com','123','',' ',' ','fjTFq1oZ6wk:APA91bHvj6cq71OxBN4MIh6LQ0Ya4Gucn6hhM1xv9NYphglGICn-7_RIFct5iEpBbLweHuH5zLa6boNVB8t2Dyj38ww1BWIJVBaDfb2678TAMgoTL0kuW5swhPkDW2SqIqnbHhmpEQc2','android','1569845183.jpg','2017-04-05 12:23:48'),(15,'','','a10','a10',0,'','','a@gmail.com','123','',' ',' ','fjTFq1oZ6wk:APA91bHvj6cq71OxBN4MIh6LQ0Ya4Gucn6hhM1xv9NYphglGICn-7_RIFct5iEpBbLweHuH5zLa6boNVB8t2Dyj38ww1BWIJVBaDfb2678TAMgoTL0kuW5swhPkDW2SqIqnbHhmpEQc2','android','343502112.jpg','2017-04-05 12:23:48'),(16,'','','rg','rgr',0,'','','rc@rc.com','123','',' ',' ','dfvhfB0StWM:APA91bF9p1mLvlLeh6QyuT24Dz7eBPse-dHgx3Qeit25UtjkeMhkR1uzDR54cbq4RCM2_68ZQSZzBHCLXA6xkG7Jf5qkNQJtyuJNDZA2RZQg57jY7MkEyitEIF0Y2bdYTMCXXko8ADed','android','388536994.jpg','2017-04-05 12:23:48'),(17,'','','Jay','Joseph Agu',0,'','','jossie215@yahoo.com','doctorjoe1','',' ',' ','dk2r2c0dfNU:APA91bEmnH1fYKmMRMZOWPkVee3H-k_kZlRfRg4Cj46Hz8FIUfi0MwqjwJ3FcmPefygazZrAlysakCpwUHxq0kksaurRiNc4wy7T0994U-GoMY-0uqS7SUuWOBRNVZ58C5Ay4Z-17qFj','android','1885515357.jpg','2017-04-05 12:23:48'),(18,'','','ollyroland ','Sarah',0,'','','ollyroland@ymail.com','agujagu','',' ',' ','cqJ76TwUG8c:APA91bHWgkB8tM9ztDQuhR9yZJC8DubAyHBMjmThpwLRipa6ig0W3SyY3VNdBtOOvofGA4DqyFemU2d9LLTK0CUk9TKOAR69B2TOCLieyTNXHcHxBGNJ3PItxB1fY0s6rd78YtwXm0SV','android','1592186600.jpg','2017-04-05 12:23:48'),(19,'','','nnannanduka','Nnanna Nduka',0,'','','nnanna.nduka@gmail.com','Adaora@abj4','',' ',' ','eTkf1TQj7-Y:APA91bFwyfbGZZKb8bW7ZZTkf0SRizDV3R9fLg9jWd096tFzf6v6lJ4K7oXX_sNvVBt_VVRLcEoiyjj82kIcKECiXjJos1rGfEg0swscNLKPGjRngdepBk1t8392XZfY24AYZs1f9Svi','android','2036822363.jpg','2017-04-05 12:23:48'),(20,'','','Azad','Azad ',0,'','','azad.seun@gmail.com','dragonballz','',' ',' ','fxFUVpCbv8o:APA91bH5uu_vM-3ogFvZf7kLiOC2t23soLn3n8PQwYLMtIiWM7e08e_G-J-roOg-p3Yp8bsHUQ9EgcOguJICNZRmBRDh2NhvYjerS2RsVLvyXpPqUOYkFwgUkBp4oEQTPbrrYH-fdJZn','android','1777389142.jpg','2017-04-05 12:23:48'),(21,'','','Dem Adewunmi','Dem Adewunmi',0,'','','adewunmi@gmail.com','nigeria','',' ',' ','dlk3b2tbVmA:APA91bFyUSfW2LgZWQX804fplQe8IGzzc7VGV4wcnf51_vNTrd24cJIwW-20Veo3GrnfTMLj6ym71yGcm1-dYimtCgVxyKKfUEO8qbu0XeZ8CtAXhWAeCcfDtSHjwZfazCX1eIwSThAj','android','1472496772.jpg','2017-04-05 12:23:48'),(22,'','','Dem','Dem Adewunmi',0,'','','adewunmi@gmail.com','nigeria','',' ',' ','dlk3b2tbVmA:APA91bFyUSfW2LgZWQX804fplQe8IGzzc7VGV4wcnf51_vNTrd24cJIwW-20Veo3GrnfTMLj6ym71yGcm1-dYimtCgVxyKKfUEO8qbu0XeZ8CtAXhWAeCcfDtSHjwZfazCX1eIwSThAj','android','463327910.jpg','2017-04-05 12:23:48'),(23,'','','tuck183','tochi',0,'','','toshiro.designs@gmail.com','arsenal02','',' ',' ','fLza-7izUAs:APA91bHk2B1cebUbfwTZMqCxhE3RUHw-yuDxdFrOSFA1GOKwYDxLxKSHegv2tmjU-vqMKY3UvO3tfhBwRTe6aKE2xdLFW4QnrZRsuq3_oAMR4iJctutfP7sZd8XY4Yp6HDDEyeYgnagq','android','1402034967.jpg','2017-04-05 12:23:48'),(24,'','','taiwomeg','Tee',0,'','','taibamise@gmail.com','Olubunmi17','',' ',' ','fyB6UBfXTfM:APA91bHqiSfwFNnSP6JJZUmwI5iPcRvtkuAHFJycuz4h8aN-7c4NgthfxmyPMqNHpMnUdIuzMJzQ-Yr-EWVoigV2J1ilTSJ9BJHCBa__NQe4jeqWjFC4PcXbAYS1habGjj6QMCYZSs6w','android','1069419283.jpg','2017-04-05 12:23:48'),(25,'','','tbam','Tee',0,'','','taibamise@gmail.com','Olubunmi17','',' ',' ','fyB6UBfXTfM:APA91bHqiSfwFNnSP6JJZUmwI5iPcRvtkuAHFJycuz4h8aN-7c4NgthfxmyPMqNHpMnUdIuzMJzQ-Yr-EWVoigV2J1ilTSJ9BJHCBa__NQe4jeqWjFC4PcXbAYS1habGjj6QMCYZSs6w','android','144417890.jpg','2017-04-05 12:23:48'),(26,'','','Cima ','Okey',0,'','',' ','cimalaski','',' ',' ','a12c7043a23d94a822ea53d071da7b4d105aff19502e66a58ade5e50cc6d6cad','ios','','2017-04-05 12:23:48'),(27,'','','Blessing','Blessing Amadi',0,'','','janeamadi14@yahoo.com','Sunday101','',' ',' ','eTkf1TQj7-Y:APA91bFwyfbGZZKb8bW7ZZTkf0SRizDV3R9fLg9jWd096tFzf6v6lJ4K7oXX_sNvVBt_VVRLcEoiyjj82kIcKECiXjJos1rGfEg0swscNLKPGjRngdepBk1t8392XZfY24AYZs1f9Svi','android','1352751558.jpg','2017-04-05 12:23:48'),(28,'','','Gbengz1','Gbenga Daodu',0,'','','gbengz1@yahoo.co.uk','gbengz27','',' ',' ','cIrPrsw5SNI:APA91bHjoB8WPo1KtIRDlRouZe0Pi5Ip8qs3_uPl-MNnSaFa3iWY5S9jLA0hLEE6VfkKOmDHHEqAiaLqS3AsEArtHxFPBxHEdRLWJVo3oGyTRFnNmjBENG9RuKb9jm6HznSUKVsHiah3','android','1110790349.jpg','2017-04-05 12:23:48'),(29,'','','Kaycee','Kaycee Clocks',0,'','','clarenceo69@yahoo.com','cum2me0069','',' ',' ','e3GhuJtD07A:APA91bFd8LTD_s26fy-YbZaP-WIOhyGt7vBjhj7f6J89Pxg8Nyd7Y3C8oQjU8ikMUL5y-VaLLiyEXMhbF58PWTVkpJaBImeu9LijfGBSbcbz41pX2Jn_xcRHFTXZ5lKg4EoTY975F3LO','android','814473953.jpg','2017-04-05 12:23:48'),(30,'','','Kaycee Clocks','Kaycee Clocks',0,'','','clarenceo69@yahoo.com','cum2me0069','',' ',' ','e3GhuJtD07A:APA91bFd8LTD_s26fy-YbZaP-WIOhyGt7vBjhj7f6J89Pxg8Nyd7Y3C8oQjU8ikMUL5y-VaLLiyEXMhbF58PWTVkpJaBImeu9LijfGBSbcbz41pX2Jn_xcRHFTXZ5lKg4EoTY975F3LO','android','810676397.jpg','2017-04-05 12:23:48'),(31,'','','Clarenceo69','Kaycee Clocks',0,'','','clarenceo69@yahoo.com','cum2me0069','',' ',' ','e3GhuJtD07A:APA91bFd8LTD_s26fy-YbZaP-WIOhyGt7vBjhj7f6J89Pxg8Nyd7Y3C8oQjU8ikMUL5y-VaLLiyEXMhbF58PWTVkpJaBImeu9LijfGBSbcbz41pX2Jn_xcRHFTXZ5lKg4EoTY975F3LO','android','2129498877.jpg','2017-04-05 12:23:48'),(32,'','','spy','Ile',0,'','','chuzimgaius@gmail.com','salvation','',' ',' ','fds0IrmufNM:APA91bEgv7j7Ml4CsPB7jbPXB85N-gnpB07b7r2Wvea9gWn6S2PE5dRLDp5gKZrEhOlj1l6NpA6UbhdVsmWJWnDDMomQx4-MGaG1e-ef1GL7u6i84bHn9V-vJC1tjNYMPjOcMvg8lNbW','android','1527106874.jpg','2017-04-05 12:23:48'),(33,'','','ile','Spy',0,'','','chuzimgaius@gmail.com','salvation','',' ',' ','fds0IrmufNM:APA91bEgv7j7Ml4CsPB7jbPXB85N-gnpB07b7r2Wvea9gWn6S2PE5dRLDp5gKZrEhOlj1l6NpA6UbhdVsmWJWnDDMomQx4-MGaG1e-ef1GL7u6i84bHn9V-vJC1tjNYMPjOcMvg8lNbW','android','619633328.jpg','2017-04-05 12:23:48'),(34,'','','imo','imo',0,'','',' ','martin321','',' ',' ','b7fb7d95145bb994f72f6252b68769a4287072339e73d9eb8727f847c313c0ca','ios','','2017-04-05 12:23:48'),(35,'','','Emembolu','CHARLES',0,'','',' ','London2015','','','','c44483d4aa13aed57a83825df48b47f3a95f19d512a59e127f29b77cd6f8c6cc','ios','','2017-04-05 12:23:48'),(36,'','','Depreye ','Don Depreye ',0,'','','akwa2k2@yahoo.com','akwa3001','',' ',' ','eCmVi8pe7bM:APA91bFqRFlC-TWJYJGYObX3z8F-ZkH4Z8oE8S0_0uWuYa8mXyhbapxBKy6fiqDP3PDNn_8_bcQNr0FaNS4BmvqEmaVDkljlEQclVYGUbW9SbTTKif3JQ2Sn3e64nAkXhsVsY7uJQLAF','android','310641136.jpg','2017-04-05 12:23:48'),(37,'','','bosslady',' Joy Okoro',0,'','','ijhollywood1@gmail.com','nnenna','',' ',' ','947a2dacb7fea5536fd086d6503779aa0a70c65fbb5d06df98b298a119d42f60','ios','','2017-04-05 12:23:48'),(38,'','','Web Artist Yaten','Web Artist Yaten Artist Yaten',0,'','','yatendraverma.agra@gmail.com','1212','',' ',' ','d9YneKvqxk0:APA91bGev2y1frKS0KPR-01qCB6zT3aekkVaKoDgasDUcba59XcFPRpmfqdw1o6mEIga284os8BkvibCWDL1soEcE2n7cjRN-wZsMDqFt5ntoN3RVnKOekYoDxhBrGjLtVBKbzmQSxZP','android','2106589093.jpg','2017-04-05 12:23:48'),(39,'','','Web','Web Artist Yaten Artist Yaten',0,'','','yatendraverma.agra@gmail.com','1212','',' ',' ','d9YneKvqxk0:APA91bGev2y1frKS0KPR-01qCB6zT3aekkVaKoDgasDUcba59XcFPRpmfqdw1o6mEIga284os8BkvibCWDL1soEcE2n7cjRN-wZsMDqFt5ntoN3RVnKOekYoDxhBrGjLtVBKbzmQSxZP','android','2063244934.jpg','2017-04-05 12:23:48'),(40,'','','nnanna.nduka','Nnanna Nduka',0,'','','nnanna.nduka@gmail.com','Adaora@abj4','',' ',' ','eTkf1TQj7-Y:APA91bFwyfbGZZKb8bW7ZZTkf0SRizDV3R9fLg9jWd096tFzf6v6lJ4K7oXX_sNvVBt_VVRLcEoiyjj82kIcKECiXjJos1rGfEg0swscNLKPGjRngdepBk1t8392XZfY24AYZs1f9Svi','android','1373534134.jpg','2017-04-05 12:23:48'),(41,'','','John','John ',0,'','',' ','sweetchics','',' ',' ','efbbba548e7d3c49e02d2f1e515afea25310cc13776066fa2f4a10b5bb62566b','ios','','2017-04-05 12:23:48'),(52,'','','Erwin','Erwin Pasaibu',0,'','','gbjkt14821@gmail.com','201945pridom','',' ',' ','dhvaNAox320:APA91bEyqt5Oz0ZTGLHMGeioF8OMmV9-ys4mltFLq9phnwAEA3tNIpY0U-jjzheC0IxzWre2gPzGLKw5LJHG2z9lci7PPprsX7-lk-MLg1xCERDOqPC9kx2ykvPWTa3xEC9osojzbktn','android','190950913.jpg','2017-05-13 10:19:19'),(53,'','','Dr Greg','Abali Maduawuchi Gregory',0,'','','greggymadu@gmail.com','aliuwatidi','',' ',' ','dWfj9Ds_Jao:APA91bHRHDipVt8h_Zz_sM4vyesFrfNC4Y-zeTnHTrXY6c0mdNj_xYjGQaUl9cMUs2h54LQz0SwUvqWvHfUmXwM4hm8XjVPRCSAOku-OX28m3RTqVFF57IxW0fdbF4zZJiYJutb4wJD7','android','2005857049.jpg','2017-05-31 19:54:29'),(54,'','','Javier','Javier Palacio',0,'','','null','javier2496,','',' ',' ','cMzLU7HVHro:APA91bEkA2QG95C8ynRt0rBukoT731ZAKfkYib1YCVRYN08WTMT24-wtPqop98OTN0n8opjHruFiHy0czZx3kwWYdeioT66KBPeN_0dtQx4Z6uYEsTlmtdA4ohZgqrUZJzifAmHrAzC8','android','45677188.jpg','2017-07-01 01:40:51'),(55,'','','Ignacio','ignacio gonzalez',0,'','','igdgondel@gmail.com','alicia123','',' ',' ','cAXCiluz24M:APA91bFa8k4--qQ3jJuuu-FgZLe2oBYzLe8yPKwKzN5-36MjPtDoipJN_O8pgF7j8Lf3QGP6SjKPg9VV5Z-tsT7YYvStElEg2FcijSLcYuPcWzzjnxsq-2ymooi53dZdG0vUMO_I2lnQ','android','724053023.jpg','2017-07-01 09:09:21'),(56,'','','christianjoe','agu uwakwe christian',0,'','','agu.uwakwe@gmail.com','pounds1213','',' ',' ','coWuTWSnU9U:APA91bGCcehuY98Lb9mbxfnEa3rOB7mguVJFH9lHsac6aqjIDglgANY_AoRQkLDnUn5z3HncofaGbITj4r9B_vR8_Q1qZarSmMy8tUvD0SioyhL5h4madiAKlDMy7yX0mvkKl683FG2Q','android','1431294243.jpg','2017-07-11 10:18:24'),(57,'','','chrisjoe','agu uwakwe christian',0,'','','agu.uwakwe@gmail.com','pounds1213','',' ',' ','coWuTWSnU9U:APA91bGCcehuY98Lb9mbxfnEa3rOB7mguVJFH9lHsac6aqjIDglgANY_AoRQkLDnUn5z3HncofaGbITj4r9B_vR8_Q1qZarSmMy8tUvD0SioyhL5h4madiAKlDMy7yX0mvkKl683FG2Q','android','807203657.jpg','2017-07-11 10:20:40'),(58,'','','chrisjoe21','agu uwakwe christian',0,'','','agu.uwakwe@gmail.com','pounds1213','',' strip clubs and lounges',' adventure','coWuTWSnU9U:APA91bGCcehuY98Lb9mbxfnEa3rOB7mguVJFH9lHsac6aqjIDglgANY_AoRQkLDnUn5z3HncofaGbITj4r9B_vR8_Q1qZarSmMy8tUvD0SioyhL5h4madiAKlDMy7yX0mvkKl683FG2Q','android','149848372.jpg','2017-07-11 10:28:06'),(59,'','','Tobi','Tobi Bakersmith',0,'','','tobi.bakersmith@gmail.com','itisgod12','',' ',' ','dMzcXM3gDfk:APA91bG4IgFpKNhTKSpmuLWh6AEm4Yuzy58Cwdrxqq_FD8NcYUlNRnu9wJGDrg3MlAQ03JPNZUUynoOd0I9u5zF-SCClYobsN73ZAlnbDSub9RxmpOYH0NdtdoMG7btH1UCo_UH96v2v','android','43997846.jpg','2017-07-19 21:39:49'),(60,'','','Daaymat','Daaymat',0,'','','mat_chdd@hotmail.com','nolosejaja','',' ',' ','d4J8KRY6mFs:APA91bHkgv8mWRsLo8lSDrHyCU-oCkxjzJV0tROAEbw27oxy0UBriN12wAg6x6DOy7AvXaI9CFRErtWQGbizlccEiXKS1-4LvMviRJvmF3_vB-klLU6zkQuTXoHfX7zApkxadwaiQqyZ','android','1677985657.jpg','2017-08-05 03:47:59'),(61,'','','Dayli','Dayli Mateos',0,'','','null','nolosejaja','',' ',' ','d4J8KRY6mFs:APA91bHkgv8mWRsLo8lSDrHyCU-oCkxjzJV0tROAEbw27oxy0UBriN12wAg6x6DOy7AvXaI9CFRErtWQGbizlccEiXKS1-4LvMviRJvmF3_vB-klLU6zkQuTXoHfX7zApkxadwaiQqyZ','android','620471845.jpg','2017-08-05 03:49:53'),(62,'','','Pau','Pau',0,'','','serranocasaj@gmail.com','pau619937711','',' ',' ','crSid4ojOrU:APA91bEgxoQ8Spkit-evhAd6WT00iEzmJCwRxLHFTQAE4Q9xvQ4nWSKg2-qNl45MATR39yCmcknrPDuudREC8ihoEN5EJiK7Ro2v8Dc-f8Z2UD7FXwf3PX1EyN0H1N1w_E99wAgqXIX2','android','1725042110.jpg','2017-08-11 19:09:07'),(63,'','','Daniel','Daniel Snchez Molina',0,'','','danidemon83@hotmail.com','0000','',' ',' ','e0rFnX-G_lo:APA91bELu8rQe38_QT_QsY-JdHor4HxrMU354IBRbWVYes-71GmvIP5AUbmloQ7_W8yBSG-YFs9GhxQmfP5qIPyKJf0dH-atG_9_vKWxyhHUMTVm5vUCUUeipW9y3v5HoLPNQLcwl8aD','android','1373413158.jpg','2017-08-11 21:10:15'),(64,'','','Ani','Ani Martinez',0,'','','animartinez@msn.com','panchita33','',' ',' ','fn31b5q0ikI:APA91bErHB8cKEfODIBdDKWJhczM4OuDM9JvTvwru7Htks7pRAm2KI0qponxoxfx5xRJm-qzzG0Xz3_GhbV7uh0ub5ZHnKOAnuhvF42LI3kSkLbpaeW-IC6Gjd3z0zXpVr6mEFqvVX2W','android','1947770826.jpg','2017-08-11 21:43:25'),(65,'','','Elena','Elena Aguilar Garcia',0,'','','tortugap5@hotmail.com','ma325700','',' ',' ','ftkO3N7JrCg:APA91bEnY4_AxnjpO_iI9YfndCiUZ85HEzqnDOIPZ_VJssvqxLAoaJnQDO3wdt7ho8r-TaIQKPKmSuuthb6SQPo8mttXaiEWjGoVeS322rYjv2H-OYsc6WYssaW6vNFCTOV4wgfgaMbl','android','1584652947.jpg','2017-08-12 10:05:26'),(66,'','','Cristinarim','Cristina Rimbau ',0,'','','cristinarim@gmail.com','cristi70','',' ',' ','f7zsr140ZuI:APA91bEgnQmdUY10hAJW0t7aCmhnd7M-KITZBh8_f7IFjahpBedU4RBqunMEezFi25biPEeubhVgjiT_8Hasfg5VkQ6juhxjHaSGfkGWI_NyWnoqHF7pDna1qWNKJoJ9BMtTDl7SvvOK','android','850840443.jpg','2017-08-12 15:36:39'),(67,'','','Cristina Rimbau','Cristina Rimbau Rimbau',0,'','','cristinarim@gmail.com','cristi70','',' ',' ','f7zsr140ZuI:APA91bEgnQmdUY10hAJW0t7aCmhnd7M-KITZBh8_f7IFjahpBedU4RBqunMEezFi25biPEeubhVgjiT_8Hasfg5VkQ6juhxjHaSGfkGWI_NyWnoqHF7pDna1qWNKJoJ9BMtTDl7SvvOK','android','100913981.jpg','2017-08-12 16:01:56'),(68,'','','Crisrimbauphoto','Cristina Rimbau ',0,'','','cristinarim@gmail.com','cristi70','',' ',' ','f7zsr140ZuI:APA91bEgnQmdUY10hAJW0t7aCmhnd7M-KITZBh8_f7IFjahpBedU4RBqunMEezFi25biPEeubhVgjiT_8Hasfg5VkQ6juhxjHaSGfkGWI_NyWnoqHF7pDna1qWNKJoJ9BMtTDl7SvvOK','android','1906492739.jpg','2017-08-12 16:03:55'),(69,'','','Albert','Albert ',0,'','','gall85@hotmail.com','22126969','',' ',' ','d2mklGSHjXo:APA91bFOrLu3HKzBSWwhBzb73NBsMzCvnjOBWQJ5N73op80rJtYPqgPlkqROL2UidikXGVor6eeb7lS8r4U6gc3SgdQsOapZmIefmoIKNxADRKuCvktuJCyO1kMJNMJW0iAIC1DrXfT4','android','58244675.jpg','2017-08-12 17:27:38'),(70,'','','Princess Chichi','Princess ',0,'','','princessudeagha@gmail.com','Chince2s','',' ',' ','coa5pf2a9PA:APA91bEp26MqGNnM6bNMnAJR8UJFcpZCU29Q_90rlVFGmlqnxJFLOoMdIak9lism-4uSYWVsp_cWq-gk4A9CB9M3uMR0UWglS87x4bPhb0xN_NCL4QgLDGjGkKoQ3Q6D6fi--FRilKpQ','android','1034996051.jpg','2017-08-24 12:13:31'),(71,'','','Princess Chi','Princess ',0,'','','princessudeagha@gmail.com','Chince2s','',' ',' ','coa5pf2a9PA:APA91bEp26MqGNnM6bNMnAJR8UJFcpZCU29Q_90rlVFGmlqnxJFLOoMdIak9lism-4uSYWVsp_cWq-gk4A9CB9M3uMR0UWglS87x4bPhb0xN_NCL4QgLDGjGkKoQ3Q6D6fi--FRilKpQ','android','1635887276.jpg','2017-08-24 12:13:58'),(72,'','','Okoro','Okoronkwo',0,'','','igwe827@gmail.com','ikenga1','',' ',' ','eI0jzWSmX-I:APA91bFWNgtAkYP7n9BHCmG0MIvwRWajusfkVTnzhPSkPzA8-Nr9A3JK4ISRx9gHty3rTDbR00XeqYmM0W1hdYcHLWfDpjDewbktKhe2d1hbMUb2_NRZIfbwIXdw30MHwpJhcA1Mgjgk','android','895193936.jpg','2017-09-13 16:55:16'),(73,'','','Luis Ricardo Romero','Luis Ricardo Romero Ricardo Romero',0,'','','gerentecasas@gmail.com','luis310864','',' ',' ','fnEEicEb-zM:APA91bHHJxkhsaTn_7dOfqGtl2gnWVcA8xGC61UmtHfXOngsYmO6RAQOfesu0DhXqes2HbmpJJgAvvMxuWCVT_YW6VrU4cx4WRVU40TI-kmh5DcL1MWtn2PSLerl6OdPUiSmCsc8VUQU','android','2068727226.jpg','2017-09-23 20:19:31'),(74,'','','Mostafa','Mostafa Abdelhamid',0,'','','moos13@live.com','handball','',' ',' ','ehQmW7fanaM:APA91bH7oAebEaJdeHIuC_7beKHgaYvX2qGgXS9yhkLGDo1p_XEfJ3khbEiHdCXdtdU-r0-y1cTLQNacbnG9txLPcCKzwm_ynm2cPfnq_4Fc8q9xoGKtXIvsB8Jadp1WPK1rtvfYM6AV','android','65454178.jpg','2017-10-10 16:54:10'),(75,'','','test','tester tester',0,'','','tester@test.com','123456','',' ',' ','testtokentesttokentesttokentesttokentesttokentesttoken','android','','2017-11-02 16:43:45'),(76,'','','cungo','Cynthia',0,'','','mayflower290@yahoo.co.uk','tchindycungo','',' ',' ','eHTzH_ZzbVs:APA91bEx3Zxrm40ch8kKBUVnOVswaCJzK4La9pRfwjttrXc81RXaYidDPygJTJeMgFmFlMMgXB1JFAD5Mu3dd9JHZiMiwkcoEFXDBOGa_tMCQBQ3fGUOVlMPg1YHXNrTxzGDfbi-MeeK','android','481570604.jpg','2017-11-13 21:38:47'),(77,'','','majikprof','Meje Obofukoro ',0,'','','meje.obofs@gmail.com','tomemajek7979','',' ',' ','djsEHtzcGsQ:APA91bEAcUscLetGLp9rhlQStkHlDi8MFzow1JO8oFof0Zs4If-wwgs60899MoqKte3mYve3ijqTtTr0ljmBGEErIV97Xqcl0ew6zix4mIQj4h7_gasslY2wdd5p9FnhjJTvGU5o1bHI','android','184706395.jpg','2017-12-19 09:51:25'),(78,'','','majikprof79','Meje Obofukoro ',0,'','','meje.obofs@gmail.com','tomemajek7979','',' ',' ','djsEHtzcGsQ:APA91bEAcUscLetGLp9rhlQStkHlDi8MFzow1JO8oFof0Zs4If-wwgs60899MoqKte3mYve3ijqTtTr0ljmBGEErIV97Xqcl0ew6zix4mIQj4h7_gasslY2wdd5p9FnhjJTvGU5o1bHI','android','1726357452.jpg','2017-12-19 09:51:49'),(79,'','','Ochiwar','Ochiwar',0,'','',' ochiwar007@hotmail.com','Tulsa','','','','5d850429a3fddaa296039f538747a7b463924929452534d410dc77bc206411e6','ios','','2017-12-31 07:37:03'),(80,'','','abetunji','Gbenga abe',0,'','','abetunji@gmail.com','Gb3ng@2718','',' ',' ','fYdsDCloeeo:APA91bGtTnuDSUWULHd-hTeu0PIcDHyFSSTCP3Zc3UH0sfMwhQjbCxNkk4b5aIQvlRuicxYV9SzPkP7ZO1XCNOfQb-7QhHn7DNzbEM94Hcf06pteZ8V0aGNFC5HxY0KjEGoobTbr8szW','android','246850083.jpg','2018-01-10 18:16:41'),(81,'','','Habila',' Habila Malgwi',0,'','','malgwihabila@gmail.com','Prince','',' ',' ','387d591105deaaf5bf759da8e859367ac000b7c5bffceaab9705fc42cefebdb2','ios','','2018-02-10 18:34:22'),(82,'','','Chris','Chris Chukwunta',0,'','','chris.chukwunta@gmail.com','25%okoct84','','','','9b2dcb0347ee4b18e9b6d8eede8b7d461e7c6a10904ac4360c9cf57097347c66','ios','1294194390.jpg','2018-05-11 17:57:17'),(83,'','','Carolyn Agu','Carolyn Agu null',0,'','','princessagu1988@gmail.com','ugochiagu1988','',' ',' ','ejGX-mnep-E:APA91bGnZZbFdBudY3j4k1qG_JhWvYBh-UX82k8FO8Sb85Aa811F3l-RS2V4g3bb7GdTcvq_ObwKhQV23Zh4Z9MsyVaUmKQ3A7cdmM9LgrM9eZ47RhEkcKNYwyHgFkAijxmzPdMeTKvU','android','675983951.jpg','2018-06-04 14:03:09'),(84,'','','Onuoha ','Nwagbo ',0,'','',' ','kelechi1','',' ',' ','e787a41a2df436b6165aa600a71b8f3b1d21333407c44508cd6928eccddebe28','ios','','2018-06-04 14:29:54'),(85,'','','bjfy','Ben',0,'','',' ','udelsa13','',' ',' ','6002e2610b1707465f07185946b58fbcbd53e12d55eaa8bf0fbb75b1f2a9dbd1','ios','','2018-06-05 03:54:47'),(86,'','','Nazubem ','Chidinma ',0,'','','nazubem@gmail.com','praise','',' ',' ','fxByEqr4MTs:APA91bEhG-iUTBjiuVoLn9XPQcucfceB_iNaCu7TZKR-7fP1E98ZQQmLCn_5hTFqqwAl77yYkRhYd7K8_bnbe863SW6wvrJzYJuWiD0D4S7aYUXl_NSRd7L4PruarcrIx69arVfjajii','android','228691314.jpg','2018-06-05 07:02:40'),(87,'','','Tommy','Thompson',0,'','','tommyak25@gmail.com','aka150','',' ',' ','f07KHb_7_FU:APA91bGP3xskJ8YdWSUa9QCVnZyfipQBGLnHegybPTZ1OKFso-f3qCuHS8kvhZWlIvWbbwwLF44pdREIriU6cDJB86H0HZ-W-4nD_qH2KLHbir4QTQhHrEhc2OqKGpj12NFtBKo9qqiE','android','1834104229.jpg','2018-06-05 10:24:29'),(88,'','','djambol','Daz',0,'','','djambol@yahoo.com','5ZPxreru','',' ',' ','c_KLGSlBzws:APA91bEyuip0YU481GEt8QkIX8FxXlI831MfaQZNqDBjf6qFiTEXCQyFum2zZxKpjbYIfJyVzpUO40syob3z-ICu_qOJR1eEM92i4iyJpSWNRjTbKzHF5VirdN6l6L3LWttwbgLi8btt','android','1813185715.jpg','2018-06-05 21:24:59'),(89,'','','Jiggamynigga','Yugbovwre Agberia ',0,'','','ayugbovski@gmail.com','edu200','',' ',' ','eJMUbni-96Q:APA91bGh-j_neEIHXRVxNFeHihXNoL8TggcqQ39A4RzS1aM8D312faRq21ODTEtcgypBBzDmPtJ8KP9xnKW7dBjgu-O3_pHxNIn0CdOqhLttx01gHtUyynasyH4eSPEJSeZRvIBGi8a_','android','1866303579.jpg','2018-06-06 04:45:00'),(90,'','','Blackqueen','Miriel',0,'','','mirielgoodluck@gmail.com','xclusivemi','',' ',' ','edBiyKlvab4:APA91bEDOiFX9eUl-Yo8PqIbrrCDwj7Bc07eiz0xLRAlv-bdimNx3UUSHhlxqZsV7xdEJFSQMWWCiwfTsBzcOhhunQj7aQ1aG1bb_eOvLXTxSKbRCwvMnrgqHsjgGD8ajcwWPP4wyzRR','android','1070184811.jpg','2018-06-07 07:06:48'),(91,'','','afurei ','afure',0,'','',' ','olukay123$','',' ',' ','455fc7d300bf57e9e1f38bc7b4b429740d4fb1c8ca9cff12a69fd43aeb340321','ios','','2018-06-08 04:00:51'),(92,'','','threech','chija',0,'','','chikachuku@gmail.com','amarachika2$','',' ',' ','cs6hl8efj0k:APA91bE7A_4lVo2DQdSkG0Tx4TOvS1_3KV2kad9QN-4kkHRcuxHXccWU0YIvZg8rC9eJIk2cwKLp_VkDd6G-H8SkQdI0lCrP8oAThN3or_rAw4506mlJqImmqUuOObdzemWovPOzVcJT','android','1674533162.jpg','2018-06-09 06:03:13'),(93,'','','cresha20 ','lacretia griffiths',0,'','',' ','Iloveme1.','',' ',' ','a2305b7d1474dd924315639fe56171428403272a764169f979d032bde24ecc75','ios','','2018-06-09 11:14:59'),(94,'','','Daz','Daz',0,'','','djambol@yahoo.com','5ZPxreru','',' ',' ','c_KLGSlBzws:APA91bEyuip0YU481GEt8QkIX8FxXlI831MfaQZNqDBjf6qFiTEXCQyFum2zZxKpjbYIfJyVzpUO40syob3z-ICu_qOJR1eEM92i4iyJpSWNRjTbKzHF5VirdN6l6L3LWttwbgLi8btt','android','1323679071.jpg','2018-06-18 05:34:47'),(95,'','','tushgeek','Kelvin',0,'','','tushgeek@gmail.com','Hackathon','',' Technology',' ','e6GljtdnaxI:APA91bHzv-CNmvAwTTjWgHH65ec8nT_gVuqPO5XMO3K3jSlwAQvCQi1bftireJxqG4INVgO981-PE-lQwV8inA-Lg4-hrpDOb_6DK8u8NKr6FwVfy92YSr5z6MKyXNij2DwbWLGKreqp_B5Sj-0SxD8hxlcfQKZrsw','android','276628128.jpg','2018-06-26 13:53:45'),(96,'','','Kelvin','Kelvin',0,'','','tushgeek@gmail.com','Hackathon','',' ',' ','e6GljtdnaxI:APA91bHzv-CNmvAwTTjWgHH65ec8nT_gVuqPO5XMO3K3jSlwAQvCQi1bftireJxqG4INVgO981-PE-lQwV8inA-Lg4-hrpDOb_6DK8u8NKr6FwVfy92YSr5z6MKyXNij2DwbWLGKreqp_B5Sj-0SxD8hxlcfQKZrsw','android','1970782317.jpg','2018-06-26 13:54:46'),(97,'','','Steaze','Folake Steaze Johnson',0,'','','steaze.johnson@gmail.com','captainsteaze030883','',' Anything nightlife...and some Guinness too',' ','cXC4mJCp75U:APA91bGDzdEM52CwcyxnvFnfbyXhO2bzNvUrYE6MX34tXHcyhvBb4RUpSex1fQ3z919O_j95onecdFlWnb5MSm0O0qtS6JB8ocCjtdRhsv468X5i2WyHEVZ_mHWDinLJVjvl9V3uDWJM','android','1648474875.jpg','2018-06-28 23:00:35'),(98,'','','Capt.Steaze','Folake Steaze Johnson',0,'','','steaze.johnson@gmail.com','captainsteaze030883','',' ',' ','cXC4mJCp75U:APA91bGDzdEM52CwcyxnvFnfbyXhO2bzNvUrYE6MX34tXHcyhvBb4RUpSex1fQ3z919O_j95onecdFlWnb5MSm0O0qtS6JB8ocCjtdRhsv468X5i2WyHEVZ_mHWDinLJVjvl9V3uDWJM','android','1103287391.jpg','2018-06-28 23:01:26'),(99,'','','edsly','edem Sylvester ',0,'','','justedsly@yahoo.com','Edems007','',' ',' ','06fc39090cb9a78539daa0c0a31cad537a377efbd994673316b724dd94942e70','ios','','2018-06-29 08:11:06'),(100,'','','Olaoluwani','Olaoluwani Onafowope',0,'','','olaoluwanionafowope@gmail.com','akinsanmi1993','',' ',' ','dxInEK6mZCw:APA91bHL-AJXdX5McsAf0oHyRQDswkEgLTOOlfBRKK-svvMT-zm27cHLbwlZRt3J5NO9dNE7okTWwl88NMGqdLoPccbdElUJaySvbqMNrjPblQZ1XbK3EsIid98TLpB_G_ZSiBIfr-f1dLVuKhtCPteTc0ee60KGbw','android','713636331.jpg','2018-06-29 12:35:32'),(101,'','','Sam','Ezeoke Samuel ',0,'','',' ','sam','',' ',' ','9b46929d71b36cebd5c8eaffb2013554fa1b173906a65616ae1e6176cebfe90b','ios','','2018-07-04 14:50:01'),(102,'','','Ayokunle','Ayokunle Paul',0,'','','apexzdgr8est@gmail.com','AyokunlePaul100','',' ',' ','cpvrs33bYBE:APA91bGluTov9AiEE-Eg5os-3nM_t_skoVMpPJoh2k_57JcHFNNx0Twcilvd3OAsd_5QFrtbkufvBu3QREEJf1gZzJgpeG7lz2lzqSk8mvy7tfKBtUKwEJj2-r0_B8alvu0GHTsD4qGNAXzeEaO65GQBHA_QQBGLRA','android','1243007569.jpg','2018-07-04 18:25:13'),(103,'','','barclays williams','barclays williams williams',0,'','','willbarclays@gmail.com','guren9000','',' ',' ','flK8y0CofpU:APA91bFsv60-qshK6mEBVJ1nzmSQpM0q16tITYJhGK01rvWv1L3SIC_O6uNwJGGR6t0E_XRTYNArtjC2246RMqllSQZ2f5fYQgpRWklKYbBg8ZOvwZy9F5Y1l05u4qNRblrX1_GpHICILaqtzWHgjo4PhRA0lLYx0g','android','249161270.jpg','2018-07-10 07:12:47'),(104,'','','Ayo','Ayokunle Paul',0,'','','apexz@gmail.com','123456789','',' ',' ','d0E5JOvznA8:APA91bED4GT6SoQCwTHPy3iMxIP3lVo01I55ju2HTK3eGZKX2P0mOcR6wGiX738RB1HmxL4q85W4zybLoQ1O7HLiccYSd69kfX6OzY23K1OSyZHJUPA9K70rej3M_13d3_dlKZ3sS7nq1V5XSiPKCeAkB0JfT6IuQg','android','1560580854.jpg','2018-07-12 18:27:26'),(105,'','','Alex','Alex Bond',0,'','','alex_desma@live.com.mx','artesito','',' ',' ','dl9RqrRcfWQ:APA91bH6lsV5uTJqE7zY6zqAo76o_PgimbkX2KWHePwgOrKXBge3Sh8afF0XXESW89wov_obUDoI5upyoExqiEw7DHJ1oqzbGgulUsssLDFqZqd985NHk5-hIpKKJWFw2d0OUAiGRM6bM8cjaaZlxWH5oycmXdzUqQ','android','870373043.jpg','2018-07-13 17:25:49'),(106,'','','eniola','Eniola Ipoola',0,'','','eniolaipoola@gmail.com','test','',' ',' ','testDevice','android','','2018-08-07 21:33:16');
/*!40000 ALTER TABLE `normaluser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `friendID` int(11) NOT NULL,
  `text` varchar(100) NOT NULL,
  `notificationType` varchar(20) NOT NULL,
  `isRead` int(11) NOT NULL,
  `date` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (1,7,8,'zz send you a friend Request','friend',0,'2017-02-21 10:02:15'),(2,6,8,'zz send you a friend Request','friend',0,'2017-02-21 10:02:20'),(3,5,8,'zz send you a friend Request','friend',0,'2017-02-21 10:37:27'),(4,8,1,'testuserapple Like your Post','like',0,'2017-02-21 10:38:17'),(5,8,1,'testuserapple Like your Post','like',0,'2017-02-21 10:38:23'),(6,7,8,'zz send you a friend Request','friend',0,'2017-02-21 11:10:40'),(7,8,10,'testmtr send you a friend Request','friend',0,'2017-02-21 12:19:30'),(8,19,12,'agujoe send you a friend Request','friend',0,'2017-03-04 20:36:26'),(9,18,12,'agujoe send you a friend Request','friend',0,'2017-03-04 20:36:29'),(10,17,12,'agujoe send you a friend Request','friend',0,'2017-03-04 20:36:32'),(11,28,31,'Clarenceo69 send you a friend Request','friend',0,'2017-03-09 21:36:52'),(12,30,31,'Clarenceo69 send you a friend Request','friend',0,'2017-03-09 21:36:58'),(13,24,31,'Clarenceo69 send you a friend Request','friend',0,'2017-03-09 21:37:08'),(14,29,31,'Clarenceo69 send you a friend Request','friend',0,'2017-03-09 21:37:12'),(15,36,22,'Dem send you a friend Request','friend',0,'2017-03-20 18:11:35'),(16,12,37,'bosslady send you a friend Request','friend',0,'2017-03-22 09:56:14'),(17,17,37,'bosslady send you a friend Request','friend',0,'2017-03-22 09:56:21'),(18,23,12,'agujoe send you a friend Request','friend',0,'2017-04-01 01:06:35'),(19,27,12,'agujoe send you a friend Request','friend',0,'2017-04-01 01:06:57'),(20,28,12,'agujoe send you a friend Request','friend',0,'2017-04-01 01:06:59'),(21,29,12,'agujoe send you a friend Request','friend',0,'2017-04-01 01:07:14'),(22,36,12,'agujoe send you a friend Request','friend',0,'2017-04-01 01:07:19'),(23,30,12,'agujoe send you a friend Request','friend',0,'2017-04-01 22:04:01'),(24,19,40,'nnanna.nduka send you a friend Request','friend',0,'2017-04-02 07:58:05'),(25,19,12,'Agujoe! Buy a package for you.','packagePurchased',0,'2017-04-03 09:57:41'),(26,31,12,'agujoe send you a friend Request','friend',0,'2017-04-03 10:06:47'),(27,19,58,'chrisjoe21 send you a friend Request','friend',0,'2017-07-11 11:46:08'),(28,20,58,'chrisjoe21 send you a friend Request','friend',0,'2017-07-11 11:46:09'),(29,18,58,'chrisjoe21 send you a friend Request','friend',0,'2017-07-11 11:52:58'),(30,21,58,'chrisjoe21 send you a friend Request','friend',0,'2017-07-11 11:52:59'),(31,22,58,'chrisjoe21 send you a friend Request','friend',0,'2017-07-11 11:53:00'),(32,23,58,'chrisjoe21 send you a friend Request','friend',0,'2017-07-11 11:53:00'),(33,17,58,'chrisjoe21 send you a friend Request','friend',0,'2017-07-11 11:53:11'),(34,70,71,'Princess Chi send you a friend Request','friend',0,'2017-08-24 13:25:29'),(35,53,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:12'),(36,56,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:16'),(37,59,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:19'),(38,57,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:22'),(39,63,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:25'),(40,70,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:31'),(41,76,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:42'),(42,77,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:46'),(43,78,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:47'),(44,80,12,'agujoe send you a friend Request','friend',0,'2018-04-22 18:46:58'),(45,17,82,'Chris send you a friend Request','friend',0,'2018-05-11 19:00:24'),(46,29,82,'Chris send you a friend Request','friend',0,'2018-05-11 19:01:26'),(47,30,82,'Chris send you a friend Request','friend',0,'2018-05-11 19:01:33'),(48,31,82,'Chris send you a friend Request','friend',0,'2018-05-11 19:01:35'),(49,95,1,'testuserapple Like your Post','like',0,'2018-06-26 14:59:14'),(50,100,1,'testuserapple Like your Post','like',0,'2018-06-29 15:59:34'),(51,7,101,'Sam send you a friend Request','friend',0,'2018-07-05 06:05:44'),(52,14,101,'Sam send you a friend Request','friend',0,'2018-07-05 06:06:39'),(53,39,101,'Sam send you a friend Request','friend',0,'2018-07-05 06:07:20');
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owneruser`
--

DROP TABLE IF EXISTS `owneruser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owneruser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL DEFAULT ' ',
  `nameOfClub` varchar(100) NOT NULL DEFAULT ' ',
  `startTime` varchar(100) NOT NULL DEFAULT ' ',
  `endTime` varchar(100) NOT NULL DEFAULT ' ',
  `clubDis` varchar(500) NOT NULL DEFAULT ' ',
  `password` varchar(50) NOT NULL DEFAULT ' ',
  `phoneNumber` varchar(50) NOT NULL DEFAULT ' ',
  `address` varchar(200) NOT NULL DEFAULT ' ',
  `email` varchar(50) NOT NULL DEFAULT ' ',
  `deviceToken` varchar(400) NOT NULL DEFAULT ' ',
  `clubType` varchar(20) NOT NULL DEFAULT ' ',
  `typeDevice` varchar(20) NOT NULL DEFAULT ' ',
  `profile` varchar(400) NOT NULL,
  `cover` varchar(400) NOT NULL,
  `lat` varchar(200) NOT NULL DEFAULT ' ',
  `long` varchar(200) NOT NULL DEFAULT ' ',
  `verificationCode` varchar(200) NOT NULL DEFAULT ' ',
  `accountActive` enum('deactivate','active') NOT NULL,
  `audio` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userUpd` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owneruser`
--

LOCK TABLES `owneruser` WRITE;
/*!40000 ALTER TABLE `owneruser` DISABLE KEYS */;
INSERT INTO `owneruser` VALUES (1,'testownerapple','Barcelona ','03:02 AM','03:02 AM','Test for apple','12345','1234884844','Barcelona, Spain','testowner@apple.com','1941878303ee12c04d33e5ebbdb2ef462915ee516d06b3c3ce18c68afcd60393','Day Club','ios','428413332.jpg','','41.3850639','2.1734035',' ','active','','2017-04-05 12:24:27',0),(25,'The Boss','The Boss Lounge ','10:00 PM','05:00 AM','You better Recognize ','playas','07041236851','Port Harcourt, Nigeria','','67a4b790100ef37b041cbd4f91a3160a6ca704f2608da8b2105166a9fe841e88','Night Club','ios','1821407899.jpg','','4.8137560','7.0024012',' ','active','','2017-04-05 12:24:27',0),(26,'nocturna','Nocturna Entertainment','10:00 PM','03:00 AM','Innovative entertainment, nightlife','sweetchics','08027003012','Haruk Estate Link Road, Port Harcourt, Nigeria','agu.agu@nocturnaapp.com','efbbba548e7d3c49e02d2f1e515afea25310cc13776066fa2f4a10b5bb62566b','Night Club','ios','1896187231.jpg','','4.8610412','6.9800087',' ','active','1657205055.mp3','2017-04-05 12:24:27',0),(27,'Outnighters ','Outnighters Live Band Club','22:0','5:0','Hot music, great ambience, best celebrities and classic cuisine and drinks all live!','Adaora@abj4','08034074367','null','outnighters@gmail.com','1a7154dcb55d1982','Night Club','android','1412409531.jpg','','35.7104208','-81.2860607',' ','active','','2017-04-09 10:29:59',0),(29,'Eamizzy','Port Harcourt','20:0','13:0','Eccentric','Emmawops007','08102899010','null','emmaops@hotmail.com','39d1d1f9765ca159','Night Club','android','621165632.jpg','','37.2265820','-95.7052230',' ','active','','2018-06-05 12:01:35',0),(30,'Dan','but','11:20 AM','11:20 AM','Ff','sam','0780','Hämeenpuisto 28, 33200 Tampere, Finland','eze','9b46929d71b36cebd5c8eaffb2013554fa1b173906a65616ae1e6176cebfe90b','Day Club','ios','1164389061.jpg','','61.4956272','23.7515923',' ','active','','2018-07-06 09:20:42',0),(31,'kachi','husband','12:59 PM','01:00 PM','We cares','kachi','085','Ikeja GRA, Ikeja, Nigeria','kachi','9b46929d71b36cebd5c8eaffb2013554fa1b173906a65616ae1e6176cebfe90b','Night Club','ios','332379106.jpg','','6.5789970','3.3494666',' ','active','','2018-07-06 11:00:59',0),(41,'eipoola','Man United','6:00pm','now','Red','test','07063738144','Felicia Koleosho, Opebi Lagos','eniolaipoola@gmail.com','testDevice','football','android','','',' ',' ',' ','active','','2018-08-07 21:12:46',0);
/*!40000 ALTER TABLE `owneruser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package`
--

DROP TABLE IF EXISTS `package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ownerID` int(11) NOT NULL,
  `nameOfClub` varchar(100) NOT NULL,
  `price` varchar(50) NOT NULL,
  `description_attributes` varchar(200) NOT NULL,
  `validity` varchar(20) NOT NULL,
  `discount` varchar(20) NOT NULL,
  `image` text NOT NULL,
  `date` datetime NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package`
--

LOCK TABLES `package` WRITE;
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
INSERT INTO `package` VALUES (1,30,'yyy','2255','hhh','','8% off','1981830117.jpg','2018-07-06 10:21:54','2018-07-06 09:21:54'),(2,31,'shoprite','2','Home of all items','','10% off','1202366006.jpg','2018-07-06 12:02:03','2018-07-06 11:02:03'),(3,31,'Jumia','25','Online market store','','10% off','919356244.jpg','2018-07-06 12:02:58','2018-07-06 11:02:58'),(4,32,'Heb','1','hhh','','5% off','615081118.jpg','2018-07-06 16:22:43','2018-07-06 15:22:43'),(5,32,'Henshaw','2','Hen','','2% off','1869316960.jpg','2018-07-06 16:23:26','2018-07-06 15:23:26');
/*!40000 ALTER TABLE `package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` varchar(100) NOT NULL,
  `picture` varchar(400) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT ' ',
  `checkIn` varchar(100) NOT NULL DEFAULT ' ',
  `video` varchar(400) NOT NULL,
  `statusType` enum('picture','checkin','video') NOT NULL,
  `date` varchar(100) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'1','214886211.jpg','Test',' ','','picture','2017-02-15 08:00:56','2017-02-05 12:25:21'),(2,'8','1549711726.jpg','anji.com fvxd the same. I ',' ','','picture','2017-02-21 10:16:42','2017-02-05 12:25:21'),(3,'8','','anji.com anji.com. 134',' ','1415499823.mp4','video','2017-02-21 10:17:10','2014-02-05 12:25:21'),(4,'8','','anji.com anji.com. 134',' ','387359272.mp4','video','2017-02-21 10:17:13','2017-02-05 12:25:21'),(5,'22','','Home','Dele Onabule Street, Lagos, Nigeria','','checkin','2017-03-20 18:03:46','2017-05-05 12:25:21'),(6,'12','','Checking .... it\'s been fun','Casablanca','','checkin','2017-04-01 01:15:18','2017-06-05 12:25:21'),(7,'12','','The place to be. Just be the boss','The Boss Lounge','','checkin','2017-04-01 01:18:28','2017-07-05 12:25:21'),(8,'12','','Nice one..','Lesukaa Concepts Ltd','','checkin','2017-04-02 22:38:04','2017-08-05 12:25:21'),(9,'40','','Not bad','Claridon Hotels and Resorts, Port Harcourt, Nigeria','','checkin','2017-04-08 21:54:55','2017-04-08 20:54:55'),(10,'40','','Not bad','Claridon Hotels and Resorts, Port Harcourt, Nigeria','','checkin','2017-04-08 21:54:59','2017-04-08 20:54:59'),(11,'12','','Check this place...','The Clarendon Hotel and Spa','','checkin','2017-04-08 21:57:41','2017-04-08 20:57:41'),(12,'12','','rockstar and gentleman combo','Club Quilox, Ozumba Mbadiwe Ave, Lagos, Nigeria','','checkin','2018-04-22 08:03:28','2018-04-22 07:03:28'),(13,'12','520225914.jpg','when you can fly...',' ','','picture','2018-04-22 08:04:28','2018-04-22 07:04:28'),(14,'12','','Nice place','Evalyn Suites & Lounge','','checkin','2018-06-01 18:35:41','2018-06-01 17:35:41'),(15,'12','','playhouse....welll',' ','1740280693.mp4','video','2018-06-13 22:12:59','2018-06-13 21:12:59'),(18,'95','935026143.jpg','Ensure you visit Asowunmi for your dresses. ',' ','','picture','2018-06-26 14:57:16','2018-06-26 13:57:16'),(17,'12','','well','Orange Room Lounge, Port Harcourt, Nigeria','','checkin','2018-06-13 22:19:46','2018-06-13 21:19:46'),(19,'100','','I work here','34 Jaiye Oyedotun Street, Lagos, Nigeria','','checkin','2018-06-29 15:53:45','2018-06-29 14:53:45'),(20,'100','','I work here','34 Jaiye Oyedotun Street, Lagos, Nigeria','','checkin','2018-06-29 15:53:52','2018-06-29 14:53:52'),(21,'101','','Add caption...',' ','610874876.mp4','video','2018-07-05 05:58:12','2018-07-05 04:58:12'),(22,'101','1349844984.jpg','Add caption...',' ','','picture','2018-07-05 05:59:28','2018-07-05 04:59:28'),(23,'101','','Add status...','Maryland','','checkin','2018-07-05 06:01:02','2018-07-05 05:01:02'),(24,'101','','I was here','Maryland','','checkin','2018-07-05 06:01:26','2018-07-05 05:01:26'),(25,'101','','TM','Ilupeju Road','','checkin','2018-07-06 11:37:40','2018-07-06 10:37:40');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchased`
--

DROP TABLE IF EXISTS `purchased`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchased` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `packageID` int(11) DEFAULT NULL,
  `qr` varchar(200) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchased`
--

LOCK TABLES `purchased` WRITE;
/*!40000 ALTER TABLE `purchased` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchased` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `split`
--

DROP TABLE IF EXISTS `split`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `split` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartID` int(11) NOT NULL,
  `friendID` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `status` enum('pending','approve') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `split`
--

LOCK TABLES `split` WRITE;
/*!40000 ALTER TABLE `split` DISABLE KEYS */;
INSERT INTO `split` VALUES (1,6,19,0,'pending'),(2,12,19,0,'pending');
/*!40000 ALTER TABLE `split` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-08 17:57:43
