<?php


class Model_club extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**Counts number of users in a particular club*/
    public function clubsCount($nameOfClub)
    {
        $q = "SELECT 
			owneruser.`id`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT('".base_url()."uploads/owner_user/profile/',owneruser.`profile`) as profile,
			CONCAT('".base_url()."uploads/owner_user/cover/',owneruser.`cover`) as cover,
			owneruser.`lat`,
			owneruser.`long`
			FROM 
			`owneruser`
			WHERE 
			owneruser.`nameOfClub` LIKE '".$nameOfClub."%'";
        $sql = $this->db->query($q);
        $count = $sql->num_rows();
        return $count;
    }

    /**Fetches the users details */
    public function clubs($nameOfClub)
    {
        $q = "SELECT 
			owneruser.`id`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT('".base_url()."uploads/owner_user/profile/',owneruser.`profile`) as profile,
			CONCAT('".base_url()."uploads/owner_user/cover/',owneruser.`cover`) as cover,
			CONCAT('".base_url()."uploads/owner_user/audio/',owneruser.`audio`) as audio,
			owneruser.`lat`,
			owneruser.`long`
			FROM 
			`owneruser`
			WHERE 
			owneruser.`nameOfClub` LIKE '".$nameOfClub."%'";
        $sql = $this->db->query($q);
        $response = $sql->result_array();
        return $response;
    }

    public function nearClubsCount($lat,$long,$radius)
    {
        //6371
        $q='SELECT `id`,`userName`,`nameOfClub`,`startTime`,`endTime`,`clubDis`,`password`,`phoneNumber`,`address`,`email`,`deviceToken`,
        `clubType`,`typeDevice`,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",`profile`) AS profile, 
        CONCAT("'.BASE_URL().'uploads/owner_user/cover/",`cover`) AS cover,`lat`,`long`,`verificationCode`,`accountActive`,
         ( 3959 * ACOS( COS( RADIANS( '.$lat.' ) ) * COS( RADIANS( `lat` ) ) * COS( RADIANS( owneruser.`long` ) - RADIANS( '.$long.' ) ) + SIN( RADIANS( '.$lat.' ) )
        * SIN( RADIANS( `lat` ) ) ) ) AS distance FROM `owneruser` HAVING distance < "'.$radius.'"
        ORDER BY distance ';
        $sql = $this->db->query($q);
        $count = $sql->num_rows();
        return $count;
    }


    public function nearClubs($lat,$long,$radius)
    {
        $q='SELECT `id`,`userName`,`nameOfClub`,`startTime`,`endTime`,`clubDis`,`password`,`phoneNumber`,`address`,`email`,`deviceToken`,
		`clubType`,`typeDevice`,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",`profile`) AS profile,
		CONCAT("'.BASE_URL().'uploads/owner_user/cover/",`cover`) AS cover,`lat`,`long`,`verificationCode`,`accountActive`, 
		( 3959 * ACOS( COS( RADIANS( '.$lat.' ) ) * COS( RADIANS( `lat` ) ) * COS( RADIANS( owneruser.`long` ) - RADIANS( '.$long.' ) ) 
		+ SIN( RADIANS( '.$lat.' ) ) * SIN( RADIANS( `lat` ) ) ) ) AS distance FROM `owneruser` HAVING distance < "'.$radius.'"
        ORDER BY distance ';
        $sql = $this->db->query($q);
        $responce = $sql->result_array();
        return $responce;
    }

    public function clubsPackagesCount($ownerID)
    {
        $sql = $this->db->get_where("package",array("ownerID"=>$ownerID));
        $count = $sql->num_rows();
        return $count;
    }
    public function clubsPackages($ownerID)
    {
        $q='SELECT package.`id`,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,owneruser.`userName`,owneruser.`clubType`,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `package` INNER JOIN `owneruser` ON package.ownerID=owneruser.`id` WHERE package.ownerID='.$ownerID;
        $sql = $this->db->query($q);
        $responce = $sql->result_array();
        return $responce;
    }

}