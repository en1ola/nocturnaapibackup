<?php


class Services_model extends CI_Model
{
	private $table;

	public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	//************************** OWNER USER SERVICES MODEL ***************************//

    /**Checks if phone number exists in database
     * @param string
     * @return  object
     */
	public function phoneCount($phoneNumber)
    {
		$sql = $this->db->get_where("owneruser",array("phoneNumber"=>$phoneNumber,"accountActive"=>"active"));
		$phoneCount = $sql->num_rows();
		return $phoneCount;
	}

	/**Saves owneruser's phone number and verification code
     * @param array
     * @return int insert_id
     */
	public function phoneRegister($data)
    {
		$this->db->insert('owneruser', $data);
		return $phoneRegister = $this->db->insert_id();
	}

	/**Gets users phone details for message verification
     * @param $phoneRegister
     * @return object $phone
     */
	public function phone($phoneRegister)
    {
		$this->db->select('id,phoneNumber,verificationCode,accountActive');
		$sql = $this->db->get_where("owneruser",array("id"=>$phoneRegister));
		$phone = $sql->result_array();
		return $phone;
	}

	public function countUser($userID)
    {
		$sql = $this->db->get_where("owneruser",array("id"=>$userID));
		$countUser = $sql->result_array();
		return $countUser;
	}

	public function updateStatus($userID)
    {
		$this->db->where('id',$userID);
		$updateStatus=$this->db->update('owneruser', array("accountActive"=>"active"));
		return $updateStatus;
	}

	/** Validates username
     *@param string
     * @return object
	*/
	public function checkUserOwner($userName)
    {
		$sql = $this->db->get_where("owneruser",array("userName"=>$userName));
		$countRow = $sql->num_rows();
		return $countRow;
	}

	/** Saves users sign up data into table ownersignup
     * @param array $data
     *@return int $id
     */
	public function ownerSignup($data)
    {
		$this->db->insert('owneruser', $data);
		return $id = $this->db->insert_id();
	}

	/** It fetches required user data
     *@param int
     *@return object
     */
	public function ownerSignupData($id)
    {
		$this->db->select('id,userName,nameOfClub,startTime,endTime,clubDis,password,phoneNumber,address,email,deviceToken,
		clubType,typeDevice,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",profile) AS profilePicture,
		CONCAT("'.BASE_URL().'uploads/owner_user/cover/",cover) AS cover,lat,long');
		$sql = $this->db->get_where("owneruser",array("id"=>$id));
		$result = $sql->row();
		return $result;
	}

	/** Fetches users with the supplied data from owneruser table
     * @param array
     * @return int
     */
	public function ownerLoginCount($data)
    {
		$sql = $this->db->get_where("owneruser",$data);
		$rowCount = $sql->num_rows();
		return $rowCount;
	}

	/** Gets specific user information
     * @param array
     * @return int
     */
	public function ownerLogin($data)
    {
		$this->db->select('id,userName,nameOfClub,startTime,endTime,clubDis,password,phoneNumber,address,email,
		deviceToken,clubType,typeDevice,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",profile) 
		AS profilePicture,CONCAT("'.BASE_URL().'uploads/owner_user/cover/",cover) AS cover,
		CONCAT("'.BASE_URL().'/uploads/owner_user/audio/",audio) AS audio,lat,long');
		$sql = $this->db->get_where("owneruser",$data);
		$rowCount = $sql->row();
		return $rowCount;
	}

	public function ownerUserPicture($data){	
		$sql = $this->db->get_where("owneruser",$data);
		$Count = $sql->num_rows();
		return $Count;
	}
	public function ownerGetPicture($data){	
		$sql = $this->db->get_where("owneruser",$data);
		$getPicture = $sql->result_array();
		return $getPicture;
	}
	public function ownerUpdatePicture($userID,$data){
		$this->db->where('id',$userID);
		$updatePicture=$this->db->update('owneruser', $data);
		return $updatePicture;
	}


	public function addPackages($data)
    {
		$this->db->insert('package', $data);
		return $response = $this->db->insert_id();
	}

	public function GetPackages($response)
    {
		$this->db->select('id,ownerID,nameOfClub,price,description_attributes,validity,discount,CONCAT("'.BASE_URL().'uploads/normal_user/profile/",image) AS image');	
		$sql = $this->db->get_where("package",array("id"=>$response));
		$getPackage = $sql->result();
		return $getPackage;
	}
	public function packageUpdate($packageID,$data){	
		$this->db->where('id',$packageID);
		$responce=$this->db->update('package', $data);
		return $responce;
	}
	public function getPackageImage($packageID){	
		$this->db->select('image');	
		$sql = $this->db->get_where("package",array("id"=>$packageID));
		$getImage = $sql->result_array();
		return $getImage;
	}
	public function deletePackage($packageID){	
		$this->db->where('id', $packageID);
      	$responce=$this->db->delete('package'); 
		return $responce;
	}
	public function ownerUpdateTokenDeviceCount($userID){	
		$sql = $this->db->get_where("owneruser",array("id"=>$userID));
		$count = $sql->num_rows();
		return $count;
	}
	public function ownerUpdateTokenDevice($userID,$data){	
		$this->db->where('id',$userID);
		$responce=$this->db->update('owneruser', $data);
		return $responce;
	}
	//************************** NORMAL USER SERVICES MODEL ***************************//
    /** It verifies username from normaluser table and ensures that its not taken
     * @param string
     * @return object
     */
	public function checkUserNormal($userName)
    {
		$sql = $this->db->get_where("normaluser",array("userName"=>$userName));
		$countRow = $sql->num_rows();
		return $countRow;
	}

	/** Saves new normal user data in database
     * @param array $data
     * @return int $id
     */
	public function normalSignup($data)
    {
		$this->db->insert('normaluser', $data);
		return $id = $this->db->insert_id();
	}

    /** It fetches required user data
     *@param int
     *@return object
     */
	public function normalSignupData($id)
    {
		$this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,CONCAT("'.BASE_URL().'/uploads/normal_user/profile/",profilePicture) AS profilePicture');
		$sql = $this->db->get_where("normaluser",array("id"=>$id));
		$result = $sql->row();
		return $result;
	}

	/** Gets a user from table normaluser given the id
     * @param int
     *@return int
     */
	public function normalLoginCount($data)
    {
		$sql = $this->db->get_where("normaluser",$data);
		$rowCount = $sql->num_rows();
		return $rowCount;
	}

	/** It gets user data required on login
     * @param int
     * @return array
     */
	public function normalLogin($data)
    {
		$this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) AS profilePicture');
		$sql = $this->db->get_where("normaluser",$data);
		$rowCount = $sql->row();
		return $rowCount;
	}

	/** Selects user's details from normaluser table
     * @param int
     * @return array
     */
	public function normalUsersDetails($data)
    {
		$this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) AS profilePicture');
		$sql = $this->db->get_where("normaluser",$data);
		$rowCount = $sql->result_array();
		return $rowCount;
	}

	/** Get places where users has previously checked in
     * @param int
     * @return int
     */
	public function getPlacesCount($userID)
    {
		$this->db->select('checkIn');	
		$sql = $this->db->get_where("post",array("userID"=>$userID,"statusType"=>"checkin"));
		$getPlacesCount = $sql->num_rows();
		return $getPlacesCount;
	}

	/** Gets user's checkin from post table
     * @param int
     * @return array
     */
	public function getPlaces($userID)
    {
		$this->db->select('checkIn');
		$sql = $this->db->get_where("post",array("userID"=>$userID,"statusType"=>"checkin"));
		$getPlaces = $sql->result_array();
		return $getPlaces;
	}

	/**Inserts user's status update data into post table */
	public function status($dataInsert)
    {
		$this->db->insert('post', $dataInsert);
		return $responce = $this->db->insert_id();
	}
	public function countStatus($statusID){	
		$sql = $this->db->get_where("post",array("id"=>$statusID));
		$countStatus = $sql->num_rows();
		return $countStatus;
	}
	public function statusEdit($statusID,$status){	
		$data=array('status'=>$status);
		$this->db->where("id",$statusID);
		$responce = $this->db->update('post',$data);
		return $responce;
	}
	public function statusDelete($statusID){	
		$this->db->where('id', $statusID);
		$responce=$this->db->delete('post');
		return $responce;
	}

	/**Checks the likes given the userID and statusid
     * @param array, int
     * @return int
     */
	public function likeCheck($data)
    {
		$sql = $this->db->get_where("likes",$data);
		$Count = $sql->num_rows();
		return $Count;
	}

	/** Gets the insert_id of a saved like action
     * @param array
     * @return int
     */
	public function like($data)
    {
		$this->db->insert('likes', $data);
		return $response = $this->db->insert_id();
	}

	/**Saves comment in db
     * @param array
     * @response int
     */
	public function comment($data)
    {
		$this->db->insert('comments', $data);
		return $responce = $this->db->insert_id();
	}

	/**Gets count of all comments on a particular status
     * @param int
     * @return int
     */
	public function getCommentsCount($statusID)
    {
		$sql = $this->db->get_where("comments",array("statusid"=>$statusID));
		$count = $sql->num_rows();
		return $count;
	}

    /**Fetches details about comments
     * @param int
     * @return array
     */
	public function getAllComments($statusID)
    {
		$q="SELECT `id`,`userID`,`statusid`,`comment`,
			(SELECT `fullName` FROM `normaluser` WHERE `normaluser`.`id`=`comments`.`userID`) AS userName, 
			(SELECT CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) FROM `normaluser` WHERE `normaluser`.`id`=`comments`.`userID`) AS userPicture
			FROM `comments` WHERE `statusid` = '".$statusID."'";	
		$sql = $this->db->query($q);
		$response = $sql->result_array();
		return $response;
	}

    /**Gets number of friends a particular user has
     * @param int
     * @return int
     */
	public function getFriendsCountF($userID)
    {
		$q="SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept')";
		$sql = $this->db->query($q);
		$getFriendsCount = $sql->num_rows();
		return $getFriendsCount;
	}

    /**Returns friends detail
     * @param int
     * @return array
     */
	public function getFriendsF($userID)
    {
		$q="SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept')";
		$sql = $this->db->query($q);
		$getFriends = $sql->result_array();
		return $getFriends;
	}
	
	/** Selects all friends to a user, returns their count
     * @param int $userID
     * @return int $getFriendsCount
     */
	public function getFriendsCount($userID)
    {
		$q="SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept' OR `status` = 'pending')";
		$sql = $this->db->query($q);
		$getFriendsCount = $sql->num_rows();
		return $getFriendsCount;
	}

	/** Gets all friends of a particular user
     * @param int
     * @request array
     */
	public function getFriends($userID)
    {
		$q="SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept' OR `status` = 'pending')";
		$sql = $this->db->query($q);
		$getFriends = $sql->result_array();
		return $getFriends;
	}

	/** Gets the news feed from all friends attached to a user
     * @param array, int $comma_separated, $userID
     * @return  int $count
     */
	public function newsFeedsCheck($comma_separated,$userID)
    {
		$q="SELECT `id`,`userID`,CONCAT('".base_url()."uploads/pictureStatus/',`picture`) AS picture,`status`,`checkin`,CONCAT('".base_url()."uploads/videoStatus/',`video`) AS video,`statusType`,`date`,
			(SELECT COUNT(id) FROM `comments` WHERE comments.`statusid`=post.`id`) AS comments,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id`) AS likes,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id` AND likes.`userID`='".$userID."') AS isLike,
			(SELECT `fullName` FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userPicture
			FROM `post` WHERE post.`userID` IN (".$comma_separated.")
			UNION ALL
			SELECT `id`,`ownerID`,CONCAT('".base_url()."uploads/owner_user/package/',`image`) AS picture,`description_attributes`,`nameOfclub`,`discount`,CONCAT('package') AS `statusType`,`date`,NULL,NULL,NULL,
			(SELECT `nameOfClub` FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/owner_user/profile/',`profile`) FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userPicture
			FROM `package`
			ORDER BY `date` DESC";
		$sql = $this->db->query($q);
		$count = $sql->num_rows();
		return $count;
	}


	public function newsFeeds($comma_separated,$userID)
    {
		$q="SELECT `id`,`userID`,CONCAT('".base_url()."uploads/pictureStatus/',`picture`) AS picture,`status`,`checkin`,CONCAT('".base_url()."uploads/videoStatus/',`video`) AS video,`statusType`,`date`,
			(SELECT COUNT(id) FROM `comments` WHERE comments.`statusid`=post.`id`) AS comments,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id`) AS likes,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id` AND likes.`userID`='".$userID."') AS isLike,
			(SELECT `fullName` FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userPicture
			FROM `post` WHERE post.`userID` IN (".$comma_separated.")
			UNION ALL
			SELECT `id`,`ownerID`,CONCAT('".base_url()."uploads/owner_user/package/',`image`) AS picture,`description_attributes`,`nameOfclub`,`discount`,CONCAT('package') AS `statusType`,`date`,NULL,NULL,NULL,
			(SELECT `nameOfClub` FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/owner_user/profile/',`profile`) FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userPicture
			FROM `package`
			ORDER BY `date` DESC";
		$sql = $this->db->query($q);
		$response = $sql->result_array();
		return $response;
	}

    /**Checks if new friend request is already a friend*/
	public function friendsCheck($userID,$friendID)
    {
		$q="SELECT * FROM `friends` WHERE (`userID` = '".$userID."' AND `friendID` = '".$friendID."') OR (`userID` = '".$friendID."' AND `friendID` = '".$userID."')";	
		$sql = $this->db->query($q);
		$count = $sql->num_rows();
		return $count;
	}


	public function friends($userID,$friendID)
    {
		$this->db->insert('friends', array("userID"=>$userID,"friendID"=>$friendID,"status"=>"pending"));
		return $responce = $this->db->insert_id();
	}

	/**Updates friends request from friends table whose status is pending
     * @param int,int
     * @return int
     */
	public function acceptCheck($userID,$friendID)
    {
		$q="SELECT `id` FROM `friends` WHERE (`userID` = '".$userID."' AND `friendID` = '".$friendID."') 
		OR (`userID` = '".$friendID."' AND `friendID` = '".$userID."')";
		$sql = $this->db->query($q);
		$count = $sql->result_array();
		foreach($count as $row){
			return $count = $row['id'];
		}
	}

	/**Changes status on friends table to accept from pending*/
	public function accept($count)
    {
		$data=array('status'=>'accept');
		$this->db->where("id",$count);
		$responce = $this->db->update('friends',$data);
		return $responce;
	}

	/**Removes a friend*/
	public function remove($count)
    {
		$this->db->where('id', $count);
		$response=$this->db->delete('friends');
		return $response;
	}

	/***/
	public function checkUsers($comma_separated){
		$q="SELECT * FROM `normaluser` WHERE `id` IN (".$comma_separated.")";	
		$sql = $this->db->query($q);
		$checkUsers = $sql->num_rows();
		return $checkUsers;
	}


	public function getFriendsList($comma_separated)
    {
		$q="SELECT id,userName,fullName,email,password,phoneNumber,deviceToken,typeDevice,
		CONCAT('".base_url()."/uploads/normal_user/profile/',profilePicture) as profilePicture FROM `normaluser` WHERE `id` IN (".$comma_separated.")";
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}

	/** Returns number of friends*/
	public function findFriendsCount($comma_separated,$search)
    {
		$q="SELECT * FROM `normaluser` WHERE id NOT IN (".$comma_separated.") AND `fullName` LIKE '%".$search."%' AND `userName` LIKE '%".$search."%'";	
		$sql = $this->db->query($q);
		$countSearch = $sql->num_rows();
		return $countSearch;
	}

    /**Returns details that pertains to friend*/
	public function findFriendsSearch($comma_separated,$search)
    {
		$q="SELECT id, googlePlus, faceBook, userName, fullName, age, gender, city, email, password, phoneNumber, intrest, dis, deviceToken, typeDevice,
        CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) as profilePicture FROM `normaluser` 
        WHERE id NOT IN (".$comma_separated.") AND `fullName` LIKE '%".$search."%' AND `userName` LIKE '%".$search."%'";
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}

	/*public function clubsCountper($nameOfClub){
		$q="SELECT * FROM `owneruser` WHERE `nameOfClub` LIKE '".$nameOfClub."%'";	
		$sql = $this->db->query($q);
		$countClubs = $sql->num_rows();
		return $countClubs;
	}
	public function ALLclubs($nameOfClub){
		$q="SELECT * FROM `owneruser` WHERE `nameOfClub` LIKE '".$nameOfClub."%'";	
		$sql = $this->db->query($q);
		$responceClubs = $sql->result_array();
		return $responceClubs;
	}*/

	/**Counts number of users in a particular club*/
	public function clubsCount($nameOfClub)
    {
		$q="SELECT 
			owneruser.`id`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT('".base_url()."uploads/owner_user/profile/',owneruser.`profile`) as profile,
			CONCAT('".base_url()."uploads/owner_user/cover/',owneruser.`cover`) as cover,
			owneruser.`lat`,
			owneruser.`long`
			FROM 
			`owneruser`
			WHERE 
			owneruser.`nameOfClub` LIKE '".$nameOfClub."%'";	
		$sql = $this->db->query($q);
		$count = $sql->num_rows();
		return $count;
	}

	/**Fetches the users details */
	public function clubs($nameOfClub)
    {
		$q="SELECT 
			owneruser.`id`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT('".base_url()."uploads/owner_user/profile/',owneruser.`profile`) as profile,
			CONCAT('".base_url()."uploads/owner_user/cover/',owneruser.`cover`) as cover,
			CONCAT('".base_url()."uploads/owner_user/audio/',owneruser.`audio`) as audio,
			owneruser.`lat`,
			owneruser.`long`
			FROM 
			`owneruser`
			WHERE 
			owneruser.`nameOfClub` LIKE '".$nameOfClub."%'";
		$sql = $this->db->query($q);
		$response = $sql->result_array();
		return $response;
	}


	public function nearClubsCount($lat,$long,$radius)
    {
		//6371
        $q='SELECT `id`,`userName`,`nameOfClub`,`startTime`,`endTime`,`clubDis`,`password`,`phoneNumber`,`address`,`email`,`deviceToken`,
        `clubType`,`typeDevice`,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",`profile`) AS profile, 
        CONCAT("'.BASE_URL().'uploads/owner_user/cover/",`cover`) AS cover,`lat`,`long`,`verificationCode`,`accountActive`,
         ( 3959 * ACOS( COS( RADIANS( '.$lat.' ) ) * COS( RADIANS( `lat` ) ) * COS( RADIANS( owneruser.`long` ) - RADIANS( '.$long.' ) ) + SIN( RADIANS( '.$lat.' ) )
        * SIN( RADIANS( `lat` ) ) ) ) AS distance FROM `owneruser` HAVING distance < "'.$radius.'"
        ORDER BY distance ';
		$sql = $this->db->query($q);
		$count = $sql->num_rows();
		return $count;
	}


	public function nearClubs($lat,$long,$radius)
    {
		$q='SELECT `id`,`userName`,`nameOfClub`,`startTime`,`endTime`,`clubDis`,`password`,`phoneNumber`,`address`,`email`,`deviceToken`,
		`clubType`,`typeDevice`,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",`profile`) AS profile,
		CONCAT("'.BASE_URL().'uploads/owner_user/cover/",`cover`) AS cover,`lat`,`long`,`verificationCode`,`accountActive`, 
		( 3959 * ACOS( COS( RADIANS( '.$lat.' ) ) * COS( RADIANS( `lat` ) ) * COS( RADIANS( owneruser.`long` ) - RADIANS( '.$long.' ) ) 
		+ SIN( RADIANS( '.$lat.' ) ) * SIN( RADIANS( `lat` ) ) ) ) AS distance FROM `owneruser` HAVING distance < "'.$radius.'"
        ORDER BY distance ';
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}

	/**Pushs like notification
     * @param string
     * @return array
     */
	public function pushNotification($pushID){	
		$sql = $this->db->get_where("normaluser",array("id"=>$pushID));
		$pushNotification = $sql->result_array();
		return $pushNotification; 
	}

	/**Selects the details of a particular liked post
     * @param array
     * @return array
     */
	public function notificationLikes($response)
    {
		$q="SELECT normaluser.`userName`,post.`id`,post.`userID` FROM likes 
			INNER JOIN `normaluser` ON likes.`userID`=normaluser.`id`
			INNER JOIN `post` ON likes.`statusid`=post.`id`
			WHERE likes.`id`=".$response;
		$sql = $this->db->query($q);
		$notificationGetResult = $sql->result_array();
		return $notificationGetResult;
	}

	/**Selects details of a particular comment
     * @param array
     * @return array
     */
	public function notificationComments($response)
    {
		$q="SELECT normaluser.`userName`,post.`id`,post.`userID` FROM comments 
			INNER JOIN `normaluser` ON comments.`userID`=normaluser.`id`
			INNER JOIN `post` ON comments.`statusid`=post.`id`
			WHERE comments.`id`=".$response;
		$sql = $this->db->query($q);
		$notificationGetResult = $sql->result_array();
		return $notificationGetResult;
	}


	public function notificationFriends($responce)
    {
		$q="SELECT normaluser.`userName` FROM friends 
			INNER JOIN `normaluser` ON normaluser.`id`=friends.`userID`
			WHERE friends.`id`=".$responce;
		$sql = $this->db->query($q);
		$notificationGetResult = $sql->result_array();
		return $notificationGetResult;
	}

	/**Saves the details related to the like activity
     * @param array
     * @return  int
     */
	public function addNotification($addDataNotification)
    {
		$this->db->insert('notification', $addDataNotification);
		return $responseNotification = $this->db->insert_id();
	}


	public function notificationsCount($userID)
    {
		$q="SELECT 
		notification.`id`,
		notification.`userID`,
		notification.`friendID`,
		notification.`text`,
		notification.`notificationType`,
		notification.`isRead`,
		notification.`date`,
		(SELECT userName FROM normaluser WHERE id=notification.friendID) AS userName,
		(SELECT CONCAT('".BASE_URL()."/uploads/normal_user/profile/',profilePicture) FROM normaluser WHERE id=notification.friendID) AS profilePicture 
		FROM `notification`
		INNER JOIN `friends` ON notification.userID=friends.`friendID` AND notification.friendID=friends.`userID` AND friends.`status` = 'pending'
		WHERE notification.`userID`='".$userID."'";
		$sql = $this->db->query($q);
		$count = $sql->num_rows();
		return $count;
	}

	public function notifications($userID)
    {
		//SELECT id,userID,friendID,text,notificationType,isRead,date,
		//(SELECT userName FROM normaluser WHERE id=friendID) as userName,
		//(SELECT CONCAT('".BASE_URL()."/uploads/normal_user/profile/',profilePicture) FROM normaluser WHERE id=friendID) as profilePicture 
		//FROM `notification` WHERE `userID`='".$userID."'
		$q="SELECT 
		notification.`id`,
		notification.`userID`,
		notification.`friendID`,
		notification.`text`,
		notification.`notificationType`,
		notification.`isRead`,
		notification.`date`,
		(SELECT userName FROM normaluser WHERE id=notification.friendID) AS userName,
		(SELECT CONCAT('".BASE_URL()."/uploads/normal_user/profile/',profilePicture) FROM normaluser WHERE id=notification.friendID) AS profilePicture 
		FROM `notification`
		INNER JOIN `friends` ON notification.userID=friends.`friendID` AND notification.friendID=friends.`userID` AND friends.`status` = 'pending'
		WHERE notification.`userID`='".$userID."'";
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}


	public function packageNotification($userID,$friendID)
    {
		$q="SELECT `fullName`,(SELECT `deviceToken` FROM normaluser WHERE `id` = '".$friendID."') as friendDeviceToken,(SELECT `typeDevice`
		FROM `normaluser` WHERE `id` = '".$friendID."') as friendTypeDevice FROM normaluser WHERE `id` = '".$userID."'";
		$sql = $this->db->query($q);
		$responceNotificationPackage = $sql->result_array();
		return $responceNotificationPackage;		
	}

	/**Updates user's profile*/
	public function normalUpdate($userID,$data)
    {
		$this->db->where('id',$userID);
		$updatePicture=$this->db->update('normaluser', $data);
		return $updatePicture;
	}

	/**Gets details of the supplied userID*/
	public function normalUserPicture($data)
    {
		$sql = $this->db->get_where("normaluser",$data);
		$Count = $sql->num_rows();
		return $Count;
	}

	/**Gets the current picture*/
	public function normalGetPicture($data)
    {
		$sql = $this->db->get_where("normaluser", $data);
		$getPicture = $sql->result_array();
		return $getPicture;
	}

	/**Updates user's picture*/
	public function normalUpdatePicture($userID,$data)
    {
		$this->db->where('id',$userID);
		$updatePicture=$this->db->update('normaluser', $data);
		return $updatePicture;
	}

    /**Gets user object/full details from db*/
	public function userFullObj($userID)
    {
		$this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,
		CONCAT("'.BASE_URL().'/uploads/normal_user/profile/",profilePicture) AS profilePicture');
		$sql = $this->db->get_where("normaluser",array("id"=>$userID));
		$response = $sql->row();
		return $response;
	}

	public function packagePurchaseCount($data){	
		$sql = $this->db->get_where("purchased",$data);
		$count = $sql->num_rows();
		return $count;
	}
	public function packagePurchase($data){	
		$this->db->insert('purchased', $data);
		return $responce = $this->db->insert_id();
	}
	public function packagePurchasedCount($userID){	
		$sql = $this->db->get_where("purchased",array("userID"=>$userID));
		$count = $sql->num_rows();
		return $count;
	}
	public function packagePurchased($userID){
		$q='SELECT purchased.id,purchased.userID,purchased.packageID,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `purchased` INNER JOIN `package` ON purchased.packageID=package.`id` WHERE purchased.userID='.$userID.' AND type = 0';	
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}
	public function clubsPackagesCount($ownerID){	
		$sql = $this->db->get_where("package",array("ownerID"=>$ownerID));
		$count = $sql->num_rows();
		return $count;
	}
	public function clubsPackages($ownerID){
		$q='SELECT package.`id`,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,owneruser.`userName`,owneruser.`clubType`,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `package` INNER JOIN `owneruser` ON package.ownerID=owneruser.`id` WHERE package.ownerID='.$ownerID;	
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}

	/***/
	public function updateTokenDeviceCount($userID,$data)
    {
		$sql = $this->db->get_where("normaluser",array("id"=>$userID));
		$count = $sql->num_rows();
		return $count;
	}

	/***/
	public function updateTokenDevice($userID,$data)
    {
		$this->db->where('id',$userID);
		$response=$this->db->update('normaluser', $data);
		return $response;
	}

	public function visitedPlacesCount($userID){	
		$q='SELECT 
			cart.`packageID`,
			package.`ownerID`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			package.`date`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT("'.BASE_URL().'uploads/owner_user/profile/",owneruser.`profile`) AS profile,
			CONCAT("'.BASE_URL().'uploads/owner_user/cover/",owneruser.`cover`) AS cover,
			owneruser.`lat`,
			owneruser.`long`,
			CONCAT("package") AS statusType
			FROM 
			`cart`
			INNER JOIN `package` ON package.id=cart.packageID
			INNER JOIN `owneruser` ON package.ownerID=owneruser.id			
			WHERE cart.`userID` = "'.$userID.'" AND cart.`status`="purchased"			
			UNION ALL			
			SELECT `id`,`userID`,(SELECT fullName FROM normaluser WHERE normaluser.`id`=post.`userID`) AS username,`status`,`checkin`,`date`,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(SELECT CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) FROM normaluser WHERE normaluser.`id`=post.`userID`) AS profile,NULL,NULL,NULL,`statusType` FROM `post` WHERE userID="'.$userID.'" AND `statusType` = "checkin" ORDER BY `date` DESC';	
		$sql = $this->db->query($q);
		$count = $sql->num_rows();
		return $count;
	}
	public function visitedPlaces($userID){
		$q='SELECT 
			cart.`packageID`,
			package.`ownerID`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			package.`date`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT("'.BASE_URL().'uploads/owner_user/profile/",owneruser.`profile`) AS profile,
			CONCAT("'.BASE_URL().'uploads/owner_user/cover/",owneruser.`cover`) AS cover,
			owneruser.`lat`,
			owneruser.`long`,
			CONCAT("package") AS statusType
			FROM 
			`cart`
			INNER JOIN `package` ON package.id=cart.packageID
			INNER JOIN `owneruser` ON package.ownerID=owneruser.id			
			WHERE cart.`userID` = "'.$userID.'" AND cart.`status`="purchased"			
			UNION ALL			
			SELECT `id`,`userID`,(SELECT fullName FROM normaluser WHERE normaluser.`id`=post.`userID`) AS username,`status`,`checkin`,`date`,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(SELECT CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) FROM normaluser WHERE normaluser.`id`=post.`userID`) AS profile,NULL,NULL,NULL,`statusType` FROM `post` WHERE userID="'.$userID.'" AND `statusType` = "checkin" ORDER BY `date` DESC';	
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}
	public function CheckUserForget($checkUser){
		$sql = $this->db->get_where("normaluser",$checkUser);
		$countRow = $sql->num_rows();
		return $countRow;
	}
	public function getUser($checkUser){
		$this->db->select("`id`,`fullName`,`email`,`password`");
		$this->db->where($checkUser);
		$sql=$this->db->get('normaluser');
		$getResponce = $sql->result_array();
		return $getResponce;
	}
	public function feedBack($data){
		$this->db->insert('feedBack', $data);
		return $responce = $this->db->insert_id();
	}
	public function checkCart($userID,$packageID){
		$sql = $this->db->get_where("cart",array("userID"=>$userID,"packageID"=>$packageID,"status"=>"pending"));
		$check = $sql->num_rows();
		return $check;
	}
	public function addMyCart($userID,$packageID,$type){
		if($type==''){$type='myself';}
		$this->db->insert('cart', array("userID"=>$userID,"packageID"=>$packageID,"status"=>"pending","type"=>$type));
		return $responce = $this->db->insert_id();
	}
	public function addMyCartSplit($val,$responce,$price){
		$this->db->insert('split', array("cartID"=>$responce,"friendID"=>$val,"price"=>$price,"status"=>"pending"));
		return $responce = $this->db->insert_id();
	}
	public function splitDataCount($userID){
		$sql = $this->db->get_where("split",array("friendID"=>$userID,"status"=>"pending"));
		$resCount = $sql->num_rows();
		return $resCount;
	}
	public function getDetailsForQR($packageID){
		$q="SELECT package.nameOfclub as packageName,owneruser.nameOfClub as ownerName FROM `package` INNER JOIN owneruser ON package.ownerID=owneruser.id WHERE package.id='".$packageID."'";
		$sql = $this->db->query($q);
		$getDetailsForQR = $sql->result_array();
		return $getDetailsForQR;
	}
	public function splitData($userID){
		$q="SELECT 
			cart.`id`,
			cart.`userID`,
			cart.`type`,
			split.`id` AS splitID,
			split.`friendID`,
			split.`price`,
			split.`status`,
			normaluser.`userName`,
			normaluser.`fullName`,
			CONCAT('".BASE_URL()."/uploads/normal_user/profile/',normaluser.`profilePicture`) AS profilePicture
			FROM `cart`
			
			INNER JOIN `split` ON split.`cartID`=cart.`id`
			INNER JOIN `normaluser` ON normaluser.`id`=split.`friendID`
			
			WHERE split.`status` = 'pending' AND split.`friendID`=".$userID;
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}
	public function splitApproveCount($splitID){
		$sql = $this->db->get_where("split",array("id"=>$splitID,"status"=>"pending"));
		$resCount = $sql->num_rows();
		return $resCount;
	}
	public function splitApprove($splitID){
		$this->db->where('id',$splitID);
		$responce = $this->db->update('split',array('status'=>"approve"));
		return $responce;
	}
	public function checkUser($userID){
		$sql = $this->db->get_where("cart",array("userID"=>$userID));
		$check = $sql->num_rows();
		return $check;
	}
	public function myCartCount($userID){
		$q= 'SELECT package.id as packageID,
		cart.id as cartID,
		package.ownerID,
		package.nameOfClub,
		package.price,
		package.description_attributes,
		package.validity,
		package.discount,
		owneruser.userName,
		owneruser.clubType,
		CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image,
		cart.type
		FROM cart 
		INNER JOIN package ON cart.packageID=package.id
		INNER JOIN owneruser on package.ownerID=owneruser.id
		WHERE cart.userID="'.$userID.'" AND cart.status="pending"';
		$sql = $this->db->query($q);
		$responcePackageCount = $sql->num_rows();
		return $responcePackageCount;
	}
	public function myCart($userID){
		$q= 'SELECT package.id as packageID,
		cart.id as cartID,
		package.ownerID,
		package.nameOfClub,
		package.price,
		package.description_attributes,
		package.validity,
		package.discount,
		owneruser.userName,
		owneruser.clubType,
		CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image,
		cart.type
		FROM cart 
		INNER JOIN package ON cart.packageID=package.id
		INNER JOIN owneruser on package.ownerID=owneruser.id
		WHERE cart.userID="'.$userID.'" AND cart.status="pending"';
		$sql = $this->db->query($q);
		$responcePackage = $sql->result_array();
		return $responcePackage;
	}
	public function getSplitPrice($cartID){
		$q= 'SELECT price FROM split WHERE cartID="'.$cartID.'" GROUP BY price';
		$sql = $this->db->query($q);
		$splitPrice = $sql->result_array();
		return $splitPrice;
	}
	/*public function allPackagesCart($commaValues){
		$q='SELECT package.`id`,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,owneruser.`userName`,owneruser.`clubType`,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `package` INNER JOIN `owneruser` ON package.ownerID=owneruser.`id` WHERE package.id IN ('.$commaValues.')';
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}*/
	public function cartPackageRemove($userID,$packageID){
		$this->db->where('userID', $userID);
		$this->db->where('packageID', $packageID);
      	$responce=$this->db->delete('cart'); 
		return $responce;
	}
	public function getClubTypesCount(){
		$q="SELECT `clubType` FROM `owneruser` GROUP BY clubType";
		$sql = $this->db->query($q);
		$countClubType = $sql->num_rows();
		return $countClubType;
	}
	public function getClubTypes(){
		$q="SELECT `clubType` FROM `owneruser` GROUP BY clubType";
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}
	/*public function checkCartPurchased($userID){
		$q="SELECT * FROM  `cart` WHERE `userID` = '".$userID."' AND `status` != 'purchased'";
		$sql = $this->db->query($q);
		$check = $sql->num_rows();
		return $check;
	}*/
	public function addMyCartPurchasedCounts($userID){
		$sql = $this->db->get_where("cart",array("userID"=>$userID,"status"=>"pending"));
		$responceCount = $sql->result_array();
		return $responceCount;
	}
	public function addMyCartPurchased($id,$QRname){
		$this->db->where('id',$id);
		$responce = $this->db->update('cart',array('status'=>"purchased","qr"=>$QRname));
		return $responce;
	}
	public function checkUserPurchased($userID){
		$sql = $this->db->get_where("cart",array("userID"=>$userID,"status"=>"purchased"));
		$check = $sql->num_rows();
		return $check;
	}
	
	public function myCartPurchased($userID){
		$sql = $this->db->get_where("cart",array("userID"=>$userID,"status"=>"purchased"));
		$responcePackage = $sql->result_array();
		return $responcePackage;
	}
	public function allPackagesCartPurchased($commaValues,$userID){
		$q='SELECT cart.`id` as availID,package.`id`,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,CONCAT("'.BASE_URL().'uploads/qr/",cart.qr) AS qr, owneruser.`userName`,owneruser.`clubType`,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `package` INNER JOIN `owneruser` ON package.ownerID=owneruser.`id` INNER JOIN cart ON cart.packageID=package.id WHERE package.id IN ('.$commaValues.') AND cart.`userID` = "'.$userID.'" AND cart.`status` = "purchased"';
		$sql = $this->db->query($q);
		$responce = $sql->result_array();
		return $responce;
	}
	public function purchasedFriend($dataFriend){
		$this->db->insert('cart', $dataFriend);
		return $responce = $this->db->insert_id();
		
	}
	public function availPackageCount($availID){
		$sql = $this->db->get_where("cart",array("id"=>$availID,"status"=>"avail"));
		$responceCount = $sql->num_rows();
		return $responceCount;
	}
	public function availPackage($availID){
		$this->db->where('id',$availID);
		$responce = $this->db->update('cart',array('status'=>"avail"));
		return $responce;
	}
}
?>