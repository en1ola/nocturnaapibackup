<?php


class Model_friend extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**Gets number of friends a particular user has
     * @param int
     * @return int
     */
    public function getFriendsCountF($userID)
    {
        $query = "SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept')";
        $result = $this->db->query($query);
        $getFriendsCount = $result->num_rows();
        return $getFriendsCount;
    }

    /**Returns friends detail
     * @param int
     * @return array
     */
    public function getFriendsF($userID)
    {
        $query = "SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept')";
        $result = $this->db->query($query);
        $getFriends = $result->result_array();
        return $getFriends;
    }

    public function getFriendsList($comma_separated)
    {
        $q = "SELECT id,userName,fullName,email,password,phoneNumber,deviceToken,typeDevice,
		CONCAT('".base_url()."/uploads/normal_user/profile/',profilePicture) as profilePicture FROM `normaluser` WHERE `id` IN (".$comma_separated.")";
        $sql = $this->db->query($q);
        $response = $sql->result_array();
        return $response;
    }

    /** Returns number of friends*/
    public function findFriendsCount($comma_separated,$search)
    {
        $q = "SELECT * FROM `normaluser` WHERE id NOT IN (".$comma_separated.") AND `fullName` LIKE '%".$search."%' AND `userName` LIKE '%".$search."%'";
        $sql = $this->db->query($q);
        $countSearch = $sql->num_rows();
        return $countSearch;
    }

    /**Returns details that pertains to friend*/
    public function findFriendsSearch($comma_separated,$search)
    {
        $q = "SELECT id, googlePlus, faceBook, userName, fullName, age, gender, city, email, password, phoneNumber, intrest, dis, deviceToken, typeDevice,
        CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) as profilePicture FROM `normaluser` 
        WHERE id NOT IN (".$comma_separated.") AND `fullName` LIKE '%".$search."%' AND `userName` LIKE '%".$search."%'";
        $sql = $this->db->query($q);
        $response = $sql->result_array();
        return $response;
    }

    /**Checks if new friend request is already a friend*/
    public function friendsCheck($userID,$friendID)
    {
        $q="SELECT * FROM `friends` WHERE (`userID` = '".$userID."' AND `friendID` = '".$friendID."') OR (`userID` = '".$friendID."' AND `friendID` = '".$userID."')";
        $sql = $this->db->query($q);
        $count = $sql->num_rows();
        return $count;
    }


    public function friends($userID,$friendID)
    {
        $this->db->insert('friends', array("userID"=>$userID,"friendID"=>$friendID,"status"=>"pending"));
        return $response = $this->db->insert_id();
    }

    /**Updates friends request from friends table whose status is pending
     * @param int,int
     * @return int
     */
    public function acceptCheck($userID,$friendID)
    {
        $q="SELECT `id` FROM `friends` WHERE (`userID` = '".$userID."' AND `friendID` = '".$friendID."') 
		OR (`userID` = '".$friendID."' AND `friendID` = '".$userID."')";
        $sql = $this->db->query($q);
        $count = $sql->result_array();
        foreach($count as $row){
            return $count = $row['id'];
        }
    }

    /**Changes status on friends table to accept from pending*/
    public function accept($count)
    {
        $data=array('status'=>'accept');
        $this->db->where("id",$count);
        $response = $this->db->update('friends',$data);
        return $response;
    }

    /**Removes a friend*/
    public function remove($count)
    {
        $this->db->where('id', $count);
        $response=$this->db->delete('friends');
        return $response;
    }

    public function notificationFriends($responce)
    {
        $q = "SELECT normaluser.`userName` FROM friends 
			INNER JOIN `normaluser` ON normaluser.`id`=friends.`userID`
			WHERE friends.`id`=".$responce;
        $sql = $this->db->query($q);
        $notificationGetResult = $sql->result_array();
        return $notificationGetResult;
    }

}