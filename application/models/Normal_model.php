<?php
/**
 * Created by PhpStorm.
 * User: lp-eniola
 * Date: 9/3/18
 * Time: 4:36 PM
 */

class Normal_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /** It verifies username from normaluser table and ensures that its not taken
     * @param string
     * @return array
     */
    public function checkUserNormal($userName)
    {
        $this->db->where("userName", $userName);
        $query = $this->db->get('normaluser')->result();
        $countRow = count($query);
        return  [
           'countRow' => $countRow,
            'user' => $query
        ];
    }

    /** Saves new normal user data in database
     * @param array $data
     * @return int $id
     */
    public function normalSignup($data)
    {
        $this->db->insert('normaluser', $data);
        return $id = $this->db->insert_id();
    }

    /** It fetches required user data
     *@param int
     *@return object
     */
    public function normalSignupData($id)
    {
        $this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,CONCAT("'.BASE_URL().'/uploads/normal_user/profile/",profilePicture) AS profilePicture');
        $query = $this->db->get_where("normaluser",array("id"=>$id));
        $result = $query->row();
        return $result;
    }

    /** Gets a user from table normaluser given the id
     * @param int
     *@return int
     */
    public function normalLoginCount($data)
    {
        $query = $this->db->get_where("normaluser",$data);
        $rowCount = $query->num_rows();
        return $rowCount;
    }

    /** It gets user data required on login
     * @param int
     * @return array
     */
    public function normalLogin($data)
    {
        $this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) AS profilePicture');
        $query = $this->db->get_where("normaluser",$data);
        $rowCount = $query->row();
        return $rowCount;
    }

    /** Selects user's details from normaluser table
     * @param int
     * @return array
     */
    public function normalUsersDetails($data)
    {
        $this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) AS profilePicture');
        $query = $this->db->get_where("normaluser",$data);
        $rowCount = $query->result_array();
        return $rowCount;
    }

    /** Get places where users has previously checked in
     * @param int
     * @return int
     */
    public function getPlacesCount($userID)
    {
        $this->db->select('checkIn');
        $query = $this->db->get_where("post",array("userID"=>$userID,"statusType"=>"checkin"));
        $getPlacesCount = $query->num_rows();
        return $getPlacesCount;
    }

    /** Gets user's checkin from post table
     * @param int
     * @return array
     */
    public function getPlaces($userID)
    {
        $this->db->select('checkIn');
        $query = $this->db->get_where("post",array("userID"=>$userID,"statusType"=>"checkin"));
        $getPlaces = $query->result_array();
        return $getPlaces;
    }

    /**Updates user's profile*/
    public function normalUpdate($userID,$data)
    {
        $this->db->where('id',$userID);
        $updatePicture=$this->db->update('normaluser', $data);
        return $updatePicture;
    }

    /**Gets details of the supplied userID*/
    public function normalUserPicture($data)
    {
        $sql = $this->db->get_where("normaluser",$data);
        $Count = $sql->num_rows();
        return $Count;
    }

    /**Gets the current picture*/
    public function normalGetPicture($data)
    {
        $sql = $this->db->get_where("normaluser", $data);
        $getPicture = $sql->result_array();
        return $getPicture;
    }

    /**Updates user's picture*/
    public function normalUpdatePicture($userID,$data)
    {
        $this->db->where('id',$userID);
        $updatePicture=$this->db->update('normaluser', $data);
        return $updatePicture;
    }

    /**Gets user object/full details from db*/
    public function userFullObj($userID)
    {
        $this->db->select('id,userName,fullName,email,password,intrest,dis,deviceToken,typeDevice,
		CONCAT("'.BASE_URL().'/uploads/normal_user/profile/",profilePicture) AS profilePicture');
        $sql = $this->db->get_where("normaluser",array("id"=>$userID));
        $response = $sql->row();
        return $response;
    }

    public function notificationsCount($userID)
    {
        $q = "SELECT 
		notification.`id`,
		notification.`userID`,
		notification.`friendID`,
		notification.`text`,
		notification.`notificationType`,
		notification.`isRead`,
		notification.`date`,
		(SELECT userName FROM normaluser WHERE id=notification.friendID) AS userName,
		(SELECT CONCAT('".BASE_URL()."/uploads/normal_user/profile/',profilePicture) FROM normaluser WHERE id=notification.friendID) AS profilePicture 
		FROM `notification`
		INNER JOIN `friends` ON notification.userID=friends.`friendID` AND notification.friendID=friends.`userID` AND friends.`status` = 'pending'
		WHERE notification.`userID`='".$userID."'";
        $sql = $this->db->query($q);
        $count = $sql->num_rows();
        return $count;
    }

    public function notifications($userID)
    {
        //SELECT id,userID,friendID,text,notificationType,isRead,date,
        //(SELECT userName FROM normaluser WHERE id=friendID) as userName,
        //(SELECT CONCAT('".BASE_URL()."/uploads/normal_user/profile/',profilePicture) FROM normaluser WHERE id=friendID) as profilePicture
        //FROM `notification` WHERE `userID`='".$userID."'
        $q = "SELECT 
		notification.`id`,
		notification.`userID`,
		notification.`friendID`,
		notification.`text`,
		notification.`notificationType`,
		notification.`isRead`,
		notification.`date`,
		(SELECT userName FROM normaluser WHERE id=notification.friendID) AS userName,
		(SELECT CONCAT('".BASE_URL()."/uploads/normal_user/profile/',profilePicture) FROM normaluser WHERE id=notification.friendID) AS profilePicture 
		FROM `notification`
		INNER JOIN `friends` ON notification.userID=friends.`friendID` AND notification.friendID=friends.`userID` AND friends.`status` = 'pending'
		WHERE notification.`userID`='".$userID."'";
        $sql = $this->db->query($q);
        $responce = $sql->result_array();
        return $responce;
    }

    public function addPackages($data)
    {
        $this->db->insert('package', $data);
        return $response = $this->db->insert_id();
    }

    public function GetPackages($response)
    {
        $this->db->select('id,ownerID,nameOfClub,price,description_attributes,validity,discount,CONCAT("'.BASE_URL().'uploads/owner_user/package/",image) AS image');
        $sql = $this->db->get_where("package",array("id"=>$response));
        $getPackage = $sql->result();
        return $getPackage;
    }

    public function CheckUserForget($checkUser)
    {
        $sql = $this->db->get_where("normaluser",$checkUser);
        $countRow = $sql->num_rows();
        return $countRow;
    }
    public function getUser($checkUser)
    {
        $this->db->select("`id`,`fullName`,`email`,`password`");
        $this->db->where($checkUser);
        $sql=$this->db->get('normaluser');
        $getResponse = $sql->result_array();
        return $getResponse;
    }
}