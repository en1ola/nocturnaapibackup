<?php

class Model_post extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**Inserts user's status update data into post table */
    public function status($dataInsert)
    {
        $this->db->insert('post', $dataInsert);
        return $response = $this->db->insert_id();
    }


    public function countStatus($statusID)
    {
        $sql = $this->db->get_where("post",array("id"=>$statusID));
        $countStatus = $sql->num_rows();
        return $countStatus;
    }

    public function statusEdit($statusID,$status)
    {
        $data=array('Post' =>$status);
        $this->db->where("id",$statusID);
        $response = $this->db->update('post',$data);
        return $response;
    }

    public function statusDelete($statusID)
    {
        $this->db->where('id', $statusID);
        $response=$this->db->delete('post');
        return $response;
    }

    /** Selects all friends to a user, returns their count
     * @param int $userID
     * @return int $getFriendsCount
     */
    public function getFriendsCount($userID)
    {
        $query ="SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept' OR `status` = 'pending')";
        $result = $this->db->query($query);
        $getFriendsCount = $result->num_rows();
        return $getFriendsCount;
    }

    /** Gets all friends of a particular user
     * @param int
     * @request array
     */
    public function getFriends($userID)
    {
        $query = "SELECT * FROM `friends` WHERE (`userID` = '".$userID."' OR `friendID` = '".$userID."') AND (`status` = 'accept' OR `status` = 'pending')";
        $result = $this->db->query($query);
        $getFriends = $result->result_array();
        return $getFriends;
    }

    /** Gets the news feed from all friends attached to a user
     * @param array, int $comma_separated, $userID
     * @return  int $count
     */
    public function newsFeedsCheck($comma_separated, $userID)
    {
        $query = "SELECT `id`,`userID`,CONCAT('".base_url()."uploads/pictureStatus/',`picture`) AS picture,`status`,`checkin`,CONCAT('".base_url()."uploads/videoStatus/',`video`) AS video,`statusType`,`date`,
			(SELECT COUNT(id) FROM `comments` WHERE comments.`statusid`=post.`id`) AS comments,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id`) AS likes,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id` AND likes.`userID`='".$userID."') AS isLike,
			(SELECT `fullName` FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userPicture
			FROM `post` WHERE post.`userID` IN (".$comma_separated.")
			UNION ALL
			SELECT `id`,`ownerID`,CONCAT('".base_url()."uploads/owner_user/package/',`image`) AS picture,`description_attributes`,`nameOfclub`,`discount`,CONCAT('package') AS `statusType`,`date`,NULL,NULL,NULL,
			(SELECT `nameOfClub` FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/owner_user/profile/',`profile`) FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userPicture
			FROM `package`
			ORDER BY `date` DESC";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        return $count;
    }


    public function newsFeeds($comma_separated,$userID)
    {
        $q = "SELECT `id`,`userID`,CONCAT('".base_url()."uploads/pictureStatus/',`picture`) AS picture,`status`,`checkin`,CONCAT('".base_url()."uploads/videoStatus/',`video`) AS video,`statusType`,`date`,
			(SELECT COUNT(id) FROM `comments` WHERE comments.`statusid`=post.`id`) AS comments,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id`) AS likes,
			(SELECT COUNT(id) FROM `likes` WHERE likes.`statusid`=post.`id` AND likes.`userID`='".$userID."') AS isLike,
			(SELECT `fullName` FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) FROM `normaluser` WHERE normaluser.`id`=post.userID) AS userPicture
			FROM `post` WHERE post.`userID` IN (".$comma_separated.")
			UNION ALL
			SELECT `id`,`ownerID`,CONCAT('".base_url()."uploads/owner_user/package/',`image`) AS picture,`description_attributes`,`nameOfclub`,`discount`,CONCAT('package') AS `statusType`,`date`,NULL,NULL,NULL,
			(SELECT `nameOfClub` FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userName,
			(SELECT CONCAT('".base_url()."uploads/owner_user/profile/',`profile`) FROM `owneruser` WHERE owneruser.`id`=package.ownerID) AS userPicture
			FROM `package`
			ORDER BY `date` DESC";
        $sql = $this->db->query($q);
        $response = $sql->result_array();
        return $response;
    }

    /**Checks the likes given the userID and statusid
     * @param array, int
     * @return int
     */
    public function likeCheck($data)
    {
        $query = $this->db->get_where("likes",$data);
        $Count = $query->num_rows();
        return $Count;
    }

    /** Gets the insert_id of a saved like action
     * @param array
     * @return int
     */
    public function like($data)
    {
        $this->db->insert('likes', $data);
        return $response = $this->db->insert_id();
    }

    /**Selects the details of a particular liked post
     * @param array
     * @return array
     */
    public function notificationLikes($response)
    {
        $query = "SELECT normaluser.`userName`,post.`id`,post.`userID` FROM likes 
			INNER JOIN `normaluser` ON likes.`userID`=normaluser.`id`
			INNER JOIN `post` ON likes.`statusid`=post.`id`
			WHERE likes.`id`=".$response;
        $sql = $this->db->query($query);
        $notificationGetResult = $sql->result_array();
        return $notificationGetResult;
    }

    /**Pushs like notification
     * @param string
     * @return array
     */
    public function pushNotification($pushID)
    {
        $query = $this->db->get_where("normaluser",array("id" => $pushID));
        $pushNotification = $query->result_array();
        return $pushNotification;
    }

    /**Saves the details related to the like activity
     * @param array
     * @return  int
     */
    public function addNotification($addDataNotification)
    {
        $this->db->insert('notification', $addDataNotification);
        return $responseNotification = $this->db->insert_id();
    }

    /**Saves comment in db
     * @param array
     * @response int
     */
    public function comment($data)
    {
        $this->db->insert('comments', $data);
        return $response = $this->db->insert_id();
    }

    /**Gets count of all comments on a particular status
     * @param int
     * @return int
     */
    public function getCommentsCount($statusID)
    {
        $query = $this->db->get_where("comments",array("statusid"=>$statusID));
        $count = $query->num_rows();
        return $count;
    }

    /**Fetches details about comments
     * @param int
     * @return array
     */
    public function getAllComments($statusID)
    {
        $query = "SELECT `id`,`userID`,`statusid`,`comment`,
			(SELECT `fullName` FROM `normaluser` WHERE `normaluser`.`id`=`comments`.`userID`) AS userName, 
			(SELECT CONCAT('".base_url()."uploads/normal_user/profile/',`profilePicture`) FROM `normaluser` WHERE `normaluser`.`id`=`comments`.`userID`) AS userPicture
			FROM `comments` WHERE `statusid` = '".$statusID."'";
        $result = $this->db->query($query);
        $response = $result->result_array();
        return $response;
    }

    /**Selects details of a particular comment
     * @param array
     * @return array
     */
    public function notificationComments($response)
    {
        $query = "SELECT normaluser.`userName`,post.`id`,post.`userID` FROM comments 
                  INNER JOIN `normaluser` ON comments.`userID`= normaluser.`id`
			      INNER JOIN `post` ON comments.`statusid`= post.`id`
			      WHERE comments.`id`=".$response;

        $result = $this->db->query($query);
        $notificationGetResult = $result->result_array();
        return $notificationGetResult;
    }
}