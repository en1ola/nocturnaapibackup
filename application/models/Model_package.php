<?php

class Model_package extends  CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function packagePurchaseCount($data)
    {
        $sql = $this->db->get_where("purchased",$data);
        $count = $sql->num_rows();
        return $count;
    }

    public function packagePurchase($data)
    {
        $this->db->insert('purchased', $data);
        return $responce = $this->db->insert_id();
    }

    public function packagePurchasedCount($userID)
    {
        $sql = $this->db->get_where("purchased",array("userID"=>$userID));
        $count = $sql->num_rows();
        return $count;
    }

    public function packagePurchased($userID)
    {
        $q = 'SELECT purchased.id,purchased.userID,purchased.packageID,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `purchased` INNER JOIN `package` ON purchased.packageID=package.`id` WHERE purchased.userID='.$userID.' AND type = 0';
        $sql = $this->db->query($q);
        $responce = $sql->result_array();
        return $responce;
    }

    public function clubsPackagesCount($ownerID)
    {
        $sql = $this->db->get_where("package",array("ownerID"=>$ownerID));
        $count = $sql->num_rows();
        return $count;
    }

    public function clubsPackages($ownerID)
    {
        $q='SELECT package.`id`,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,owneruser.`userName`,owneruser.`clubType`,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `package` INNER JOIN `owneruser` ON package.ownerID=owneruser.`id` WHERE package.ownerID='.$ownerID;
        $sql = $this->db->query($q);
        $response = $sql->result_array();
        return $response;
    }

    public function checkUserPurchased($userID)
    {
        $sql = $this->db->get_where("cart",array("userID"=>$userID,"status"=>"purchased"));
        $check = $sql->num_rows();
        return $check;
    }

    public function myCartPurchased($userID)
    {
        $sql = $this->db->get_where("cart",array("userID"=>$userID,"status"=>"purchased"));
        $responcePackage = $sql->result_array();
        return $responcePackage;
    }
    public function allPackagesCartPurchased($commaValues,$userID)
    {
        $q='SELECT cart.`id` as availID,package.`id`,package.`ownerID`,package.nameOfClub,package.price,package.description_attributes,package.validity,package.discount,CONCAT("'.BASE_URL().'uploads/qr/",cart.qr) AS qr, owneruser.`userName`,owneruser.`clubType`,CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image FROM `package` INNER JOIN `owneruser` ON package.ownerID=owneruser.`id` INNER JOIN cart ON cart.packageID=package.id WHERE package.id IN ('.$commaValues.') AND cart.`userID` = "'.$userID.'" AND cart.`status` = "purchased"';
        $sql = $this->db->query($q);
        $responce = $sql->result_array();
        return $responce;
    }

    public function splitDataCount($userID)
    {
        $sql = $this->db->get_where("split",array("friendID"=>$userID,"status"=>"pending"));
        $resCount = $sql->num_rows();
        return $resCount;
    }

    public function visitedPlacesCount($userID)
    {
        $q = 'SELECT 
			cart.`packageID`,
			package.`ownerID`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			package.`date`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT("'.BASE_URL().'uploads/owner_user/profile/",owneruser.`profile`) AS profile,
			CONCAT("'.BASE_URL().'uploads/owner_user/cover/",owneruser.`cover`) AS cover,
			owneruser.`lat`,
			owneruser.`long`,
			CONCAT("package") AS statusType
			FROM 
			`cart`
			INNER JOIN `package` ON package.id=cart.packageID
			INNER JOIN `owneruser` ON package.ownerID=owneruser.id			
			WHERE cart.`userID` = "'.$userID.'" AND cart.`status`="purchased"			
			UNION ALL			
			SELECT `id`,`userID`,(SELECT fullName FROM normaluser WHERE normaluser.`id`=post.`userID`) AS username,`status`,`checkin`,`date`,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(SELECT CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) FROM normaluser WHERE normaluser.`id`=post.`userID`) AS profile,NULL,NULL,NULL,`statusType` FROM `post` WHERE userID="'.$userID.'" AND `statusType` = "checkin" ORDER BY `date` DESC';
        $sql = $this->db->query($q);
        $count = $sql->num_rows();
        return $count;
    }
    public function visitedPlaces($userID)
    {
        $q = 'SELECT 
			cart.`packageID`,
			package.`ownerID`,
			owneruser.`userName`,
			owneruser.`nameOfClub`,
			owneruser.`startTime`,
			package.`date`,
			owneruser.`endTime`,
			owneruser.`clubDis`,
			owneruser.`password`,
			owneruser.`phoneNumber`,
			owneruser.`address`,
			owneruser.`email`,
			owneruser.`deviceToken`,
			owneruser.`clubType`,
			owneruser.`typeDevice`,
			CONCAT("'.BASE_URL().'uploads/owner_user/profile/",owneruser.`profile`) AS profile,
			CONCAT("'.BASE_URL().'uploads/owner_user/cover/",owneruser.`cover`) AS cover,
			owneruser.`lat`,
			owneruser.`long`,
			CONCAT("package") AS statusType
			FROM 
			`cart`
			INNER JOIN `package` ON package.id=cart.packageID
			INNER JOIN `owneruser` ON package.ownerID=owneruser.id			
			WHERE cart.`userID` = "'.$userID.'" AND cart.`status`="purchased"			
			UNION ALL			
			SELECT `id`,`userID`,(SELECT fullName FROM normaluser WHERE normaluser.`id`=post.`userID`) AS username,`status`,`checkin`,`date`,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(SELECT CONCAT("'.BASE_URL().'uploads/normal_user/profile/",profilePicture) FROM normaluser WHERE normaluser.`id`=post.`userID`) AS profile,NULL,NULL,NULL,`statusType` FROM `post` WHERE userID="'.$userID.'" AND `statusType` = "checkin" ORDER BY `date` DESC';
        $sql = $this->db->query($q);
        $responce = $sql->result_array();
        return $responce;
    }

    public function deletePackage($packageID)
    {
        $this->db->where('id', $packageID);
        $response = $this->db->delete('package');
        return $response;
    }

    public function GetPackages($response)
    {
        $this->db->select('id,ownerID,nameOfClub,price,description_attributes,validity,discount,CONCAT("'.BASE_URL().'uploads/normal_user/profile/",image) AS image');
        $sql = $this->db->get_where("package",array("id"=>$response));
        $getPackage = $sql->result_array();
        return $getPackage;
    }

    public function packageUpdate($packageID,$data)
    {
        $this->db->where('id',$packageID);
        $responce=$this->db->update('package', $data);
        return $responce;
    }

    public function getPackageImage($packageID){
        $this->db->select('image');
        $sql = $this->db->get_where("package",array("id"=>$packageID));
        $getImage = $sql->result_array();
        return $getImage;
    }

    public function splitData($userID){
        $q="SELECT cart.`id`, cart.`userID`, cart.`type`, split.`id` AS splitID, split.`friendID`, split.`price` as split_price, split.`status`,
	        normaluser.`userName`, normaluser.`fullName`, normaluser.`email`,
	        package.id as packageID, package.ownerID,  package.nameOfClub, package.price, package.description_attributes,
	        package.validity, package.discount,
	        owneruser.userName, owneruser.nameOfClub as owner_club, owneruser.clubType,
	        CONCAT('".BASE_URL()."/uploads/normal_user/profile/',normaluser.`profilePicture`) AS profilePicture
	        FROM `cart`
	        INNER JOIN `split` ON split.`cartID`=cart.`id`
	        INNER JOIN `normaluser` ON normaluser.`id`=split.`friendID`
	        INNER JOIN `package` ON cart.packageID = package.id
	        INNER JOIN owneruser on package.ownerID = owneruser.id 
            WHERE split.`status` = 'pending' AND split.`friendID`=".$userID;

        $sql = $this->db->query($q);
        $responce = $sql->result_array();
        return $responce;
    }

    /**
     * for split item that has been purchased, their status is updated to approved on split table
     */
    public function updatePurchasedSplitItem($cartID, $friendID)
    {
        $split_data = $this->db->get_where("split", array("cartID" => $cartID, "friendID" => $friendID, "status" => "pending"));
        $result = $split_data->num_rows();
        if($result > 0){
            $sql = $this->db->set(array("status" => "approve"));
            $this->db->where(array("cartID" => $cartID, "friendID" => $friendID));
            $update = $this->db->update("split");
            return $update;
        } else {
            return false;
        }
    }


    /**
     * Check if split information exists
     */
    public  function checkSplitData($cartID, $friendID)
    {
        $split_data = $this->db->get_where("split",array("cartID" => $cartID, "friendID" => $friendID, "status" => "pending"));

        $result = $split_data->result();
    }


}