<?php
/**
 * Created by PhpStorm.
 * User: lp-eniola
 * Date: 9/12/18
 * Time: 5:47 AM
 */

class Model_cart extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function checkCart($userID, $packageID)
    {
        $sql = $this->db->get_where("cart",array("userID"=>$userID,"packageID"=>$packageID,"status"=>"pending"));
        $check = $sql->num_rows();
        return $check;
    }

    public function addMyCart($userID,$packageID,$type)
    {
        if($type == '') {
            $type='myself';
        }
        $this->db->insert('cart', array("userID"=>$userID,"packageID"=>$packageID,"status"=>"pending","type"=>$type));
        return $response = $this->db->insert_id();
    }

    /*** at this point, insert into mycart backup table; temporary/quick fix for
     * splitdata loss if payment is made from originator's end first.
     */
    public function addMyCartBackup($userID, $packageID, $type)
    {
        if($type == ''){
            $type = "myself";
        }

        $this->db->insert('cart_split_backup', array("userID" => $userID, "packageID" => $packageID, "type" => $type));
        return $response = $this->db->insert_id();
    }


    public function addMyCartSplit($val,$responce,$price)
    {
        $this->db->insert('split', array("cartID"=>$responce,"friendID"=>$val,"price"=>$price,"status"=>"pending"));
        return $response = $this->db->insert_id();
    }

    public function splitDataCount($userID)
    {
        $sql = $this->db->get_where("split",array("friendID"=>$userID,"status"=>"pending"));
        $resCount = $sql->num_rows();
        return $resCount;
    }

    public function getDetailsForQR($packageID)
    {
        $q="SELECT package.nameOfclub as packageName,owneruser.nameOfClub as ownerName FROM `package` INNER JOIN owneruser ON package.ownerID=owneruser.id WHERE package.id='".$packageID."'";
        $sql = $this->db->query($q);
        $getDetailsForQR = $sql->result_array();
        return $getDetailsForQR;
    }

    public function purchasedFriend($dataFriend)
    {
        $this->db->insert('cart', $dataFriend);
        return $response = $this->db->insert_id();

    }

    public function packageNotification($userID,$friendID)
    {
        $q="SELECT `fullName`,(SELECT `deviceToken` FROM normaluser WHERE `id` = '".$friendID."') as friendDeviceToken,(SELECT `typeDevice`
		FROM `normaluser` WHERE `id` = '".$friendID."') as friendTypeDevice FROM normaluser WHERE `id` = '".$userID."'";
        $sql = $this->db->query($q);
        $responceNotificationPackage = $sql->result_array();
        return $responceNotificationPackage;
    }

    /**Saves the details related to the like activity
     * @param array
     * @return  int
     */
    public function addNotification($addDataNotification)
    {
        $this->db->insert('notification', $addDataNotification);
        return $responseNotification = $this->db->insert_id();
    }

    public function splitApproveCount($splitID)
    {
        $sql = $this->db->get_where("split",array("id"=>$splitID,"status"=>"pending"));
        $resCount = $sql->num_rows();
        return $resCount;
    }


    public function splitApprove($splitID)
    {
        $this->db->where('id',$splitID);
        $responce = $this->db->update('split',array('status'=>"approve"));
        return $responce;
    }

    public function checkUser($userID)
    {
        $sql = $this->db->get_where("cart", array( "userID" => $userID));
        $check = $sql->num_rows();
        return $check;
    }

    public function myCartCount($userID)
    {
        $q= 'SELECT package.id as packageID,
		cart.id as cartID,
		package.ownerID,
		package.nameOfClub,
		package.price,
		package.description_attributes,
		package.validity,
		package.discount,
		owneruser.userName,
		owneruser.nameOfClub as club,
		owneruser.clubType,
		CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image,
		cart.type
		FROM cart 
		INNER JOIN package ON cart.packageID=package.id
		INNER JOIN owneruser on package.ownerID=owneruser.id
		WHERE cart.userID="'.$userID.'" AND cart.status="pending"';
        $sql = $this->db->query($q);
        $responcePackageCount = $sql->num_rows();
        return $responcePackageCount;
    }

    public function myCart($userID)
    {
        $q = 'SELECT package.id as packageID,
		cart.id as cartID,
		package.ownerID,
		package.nameOfClub,
		package.price,
		package.description_attributes,
		package.validity,
		package.discount,
		owneruser.userName,
		owneruser.nameOfClub as owner_club,
		owneruser.clubType,
		CONCAT("'.BASE_URL().'uploads/owner_user/package/",package.image) AS image,
		cart.type
		FROM cart 
		INNER JOIN package ON cart.packageID=package.id
		INNER JOIN owneruser on package.ownerID=owneruser.id
		WHERE cart.userID="'.$userID.'" AND cart.status="pending"';
        $sql = $this->db->query($q);
        $responcePackage = $sql->result_array();
        return $responcePackage;
    }

    public function cartPackageRemove($userID, $packageID)
    {
        /*$this->db->where('userID', $userID);
        $this->db->where('packageID', $packageID);
        $response = $this->db->delete('cart');
        return $response;*/

        //instead of deleting this, just update it. Change the status to a default, 'bought'
        $check_cart = $this->db->get_where("cart", array("userID" => $userID, "packageID" => $packageID));
        $result = $check_cart->num_rows();

        if($result > 0){
            $column = array("status" => "bought");
            $this->db->set($column);
            $this->db->where(array("userID" => $userID, "packageID" => $packageID));
            $update = $this->db->update("cart");
            return $update;
        } else {
            return false;
        }
    }


    public function availPackageCount($availID)
    {
        $sql = $this->db->get_where("cart",array("id"=>$availID,"status"=>"avail"));
        $responceCount = $sql->num_rows();
        return $responceCount;
    }


    public function availPackage($availID)
    {
        $this->db->where('id',$availID);
        $responce = $this->db->update('cart',array('status'=>"avail"));
        return $responce;
    }

    public function addMyCartPurchasedCounts($userID)
    {
        $sql = $this->db->get_where("cart",array("userID"=>$userID,"status"=>"pending"));
        $responceCount = $sql->result_array();
        return $responceCount;
    }


    public function addMyCartPurchased($id,$QRname)
    {
        $this->db->where('id',$id);
        $responce = $this->db->update('cart',array('status'=>"purchased","qr"=>$QRname));
        return $responce;
    }

    public function checkUserPurchased($userID)
    {
        $sql = $this->db->get_where("cart",array("userID"=>$userID,"status"=>"purchased"));
        $check = $sql->num_rows();
        return $check;
    }

    public function getSplitPrice($cartID){
        $q= 'SELECT price FROM split WHERE cartID="'.$cartID.'" GROUP BY price';
        $sql = $this->db->query($q);
        $splitPrice = $sql->result_array();
        return $splitPrice;
    }
}