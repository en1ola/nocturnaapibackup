<?php
//defined('Basepath') or exit('No direct script is allowed here');

class Owner_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //model functions
    /**Checks if phone number exists in database
     * @param string
     * @return  object
     */
    public function phoneCount($phoneNumber)
    {
        $query = $this->db->get_where("owneruser",array("phoneNumber"=>$phoneNumber,"accountActive"=>"active"));
        $phoneCount = $query->num_rows();
        return $phoneCount;
    }

    /**Saves owneruser's phone number and verification code
     * @param array
     * @return int insert_id
     */
    public function phoneRegister($data)
    {
        $this->db->insert('owneruser', $data);
        return $phoneRegister = $this->db->insert_id();
    }

    /**Gets users phone details for message verification
     * @param $phoneRegister
     * @return object $phone
     */
    public function phone($phoneRegister)
    {
        $this->db->select('id,phoneNumber,verificationCode,accountActive');
        $query = $this->db->get_where("owneruser",array("id"=>$phoneRegister))->result_array();
        return $query;
    }

    /** Validates username
     *@param string
     * @return object
     */
    public function checkUserOwner($userName)
    {
        $sql = $this->db->get_where("owneruser",array("userName"=>$userName));
        $countRow = $sql->num_rows();
        return $countRow;
    }

    /** Saves users sign up data into table ownersignup
     * @param array $data
     *@return int $id
     */
    public function ownerSignup($data)
    {
        $this->db->insert('owneruser', $data);
        return $id = $this->db->insert_id();
    }

    /** It fetches required user data
     *@param int
     *@return object
     */
    public function ownerSignupData($id)
    {
        $this->db->select('id,userName,nameOfClub,startTime,endTime,clubDis,password,phoneNumber,address,email,deviceToken,
		clubType,typeDevice,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",profile) AS profilePicture,
		CONCAT("'.BASE_URL().'uploads/owner_user/cover/",cover) AS cover,lat,long');
        $sql = $this->db->get_where("owneruser",array("id"=>$id));
        $result = $sql->row();
        return $result;
    }

    /** Fetches users with the supplied data from owneruser table
     * @param array
     * @return int
     */
    public function ownerLoginCount($data)
    {
        $query = $this->db->get_where("owneruser",$data);
        $rowCount = $query->num_rows();
        return $rowCount;
    }

    /** Gets specific user information
     * @param array
     * @return int
     */
    public function ownerLogin($data)
    {
        $this->db->select('id,userName,nameOfClub,startTime,endTime,clubDis,password,phoneNumber,address,email,
		deviceToken,clubType,typeDevice,CONCAT("'.BASE_URL().'uploads/owner_user/profile/",profile) 
		AS profilePicture,CONCAT("'.BASE_URL().'uploads/owner_user/cover/",cover) AS cover,
		CONCAT("'.BASE_URL().'/uploads/owner_user/audio/",audio) AS audio,lat,long');
        $query = $this->db->get_where("owneruser",$data);
        $rowCount = $query->row();
        return $rowCount;
    }

    public function ownerUserPicture($data)
    {
        $sql = $this->db->get_where("owneruser",$data);
        $Count = $sql->num_rows();
        return $Count;
    }

    public function ownerGetPicture($data)
    {
        $sql = $this->db->get_where("owneruser",$data);
        $getPicture = $sql->result_array();
        return $getPicture;
    }

    public function ownerUpdatePicture($userID,$data)
    {
        $this->db->where('id',$userID);
        $updatePicture=$this->db->update('owneruser', $data);
        return $updatePicture;
    }

    public function updateTokenDeviceCount($userID, $data)
    {
        $sql = $this->db->get_where("normaluser",array("id" => $userID));
        $count = $sql->num_rows();
        return $count;
    }

    /***/
    public function updateTokenDevice($userID,$data)
    {
        $this->db->where('id',$userID);
        $response=$this->db->update('normaluser', $data);
        return $response;
    }

    public function feedBack($data)
    {
        $this->db->insert('feedBack', $data);
        return $response = $this->db->insert_id();
    }


    public function saveQRCode($data)
    {
        $query = $this->db->insert('owner_qrCode', $data);
        return $query;
    }

    public function getQrCode($userID)
    {
         $this->db->where('owner_id', $userID);
         $query = $this->db->get('owner_qrCode')->result_array();
         return $query;
    }

    public function savedQrCode($data)
    {
        $query = $this->db->get_where("owner_qrCode", $data);
        $count = $query->num_rows();
        return $count;
    }

    public function getOwnerUser()
    {
        $this->db->select('userName, email, password, address, deviceToken,nameOfClub');
        $query = $this->db->get('owneruser')->result_array();
        return $query;
    }
}
