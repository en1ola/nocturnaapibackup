<?php

require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Friend extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Model_friend');
        $this->load->model('Model_post');
        $this->load->helper('url');
    }

    public function index_get()
    {
        $this->response("Welcome to the controller that gives information about all your friends");
    }


    public function getFriendList_post()
    {
        $data = $this->post();
        $ids = array();
        try{
            if (!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $getFriendsCount=$this->Model_friend->getFriendsCountF($userID);
                if($getFriendsCount>0){
                    $getFriends=$this->Model_friend->getFriendsF($userID);
                    foreach($getFriends as $getIDS){
                        if($getIDS["friendID"] == $userID) {
                            $ids[] = $getIDS["userID"];
                        }else {
                            $ids[] = $getIDS["friendID"];
                        }
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No Friends Found",
                        "data" => ''
                    ];
                    $this->response($response);
                }
                $comma_separated = implode(",", $ids);
                if($comma_separated != ''){
                    $response_data = $this->Model_friend->getFriendsList($comma_separated);
                    if($response_data != ''){
                        $response = [
                            "status" => 1,
                            "message" => "Get all Friends list",
                            "data" => $response_data
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry! There was a problem",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! No user found",
                        "data" => ''
                    ];
                }

            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function findFriend_post()
    {
        $data = $this->post();
        $ids = array();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif (!isset($data['search'])){
                $response = [
                    "status" => 0,
                    "message" => "Search is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $search = $data['search'];
                $ids[] = $userID;

                $getFriendsCount = $this->Model_post->getFriendsCount($userID);
                if($getFriendsCount > 0){
                    $getFriends=$this->Model_post->getFriends($userID);
                    foreach($getFriends as $getIDS){
                        if($getIDS["friendID"] == $userID) {
                            $ids[] = $getIDS["userID"];
                        }else {
                            $ids[] = $getIDS["friendID"];
                        }
                    }
                }
                $comma_separated = implode(",", $ids);

                $countSearch = $this->Model_friend->findFriendsCount($comma_separated,$search);
                if($countSearch > 0){
                    $response_data = $this->Model_friend->findFriendsSearch($comma_separated,$search);
                    $response = [
                        "status" => 1,
                        "message" => "All Search Friends",
                        "data" => $response_data
                    ];
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No search result found for friend",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function newFriend_post()
    {
        $data = $this->post();
        $date = date('Y-m-d H:i:s');
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif (!isset($data['friendID'])){
                $response = [
                    "status" => 0,
                    "message" => "Friend ID is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $friendID = $data['friendID'];
                $count = $this->Model_friend->friendsCheck($userID,$friendID);
                if($count > 0){
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! you have already sent a request",
                        "data" => ''
                    ];
                }else{
                    $response_data = $this->Model_friend->friends($userID,$friendID);
                    if($response_data != ''){
                        $notificationGetResult = $this->Model_friend->notificationFriends($response_data);
                        foreach($notificationGetResult as $dt){
                            $addDataNotification = array(
                                "userID"=>$friendID,
                                "friendID"=>$userID,
                                "text"=>"".$dt['userName']." send you a friend Request","notificationType"=>"friend","isRead"=>"0",
                                "date"=>$date
                            );
                            $pushID = $friendID;
                            $pushNotification = $this->Model_post->pushNotification($pushID);
                            if($pushNotification[0]['typeDevice'] == 'ios'){
                                $deviceToken = $pushNotification[0]['deviceToken'];
                                $passphrase = 'abcd';
                                $message = $dt['userName']." send you a friend request";
                                $ctx = stream_context_create();
                                $ck_url = BASE_URL()."ck.pem";
                                stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
                                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                                $fp = stream_socket_client(
                                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                                if (!$fp)
                                    exit("Failed to connect: $err $errstr" . PHP_EOL);
                                $body['aps'] = array(
                                    'alert' => $message,
                                    'sound' => 'default'
                                );
                                $payload = json_encode($body);
                                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                                $result = fwrite($fp, $msg, strlen($msg));
                                fclose($fp);
                            }else if($pushNotification[0]['typeDevice'] == 'android'){
                                //$apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
                                $apiKey2 = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
                                $registrationIDs = $pushNotification[0]['deviceToken'];
                                $message = array(
                                    "message" => $dt['userName']." send you a friend request",
                                    "title" => "Nocturna",
                                    "vibrate" => 1,
                                    "sound" => 1,
                                    "smallIcon" => "small_icon"
                                );
                                $url = 'https://fcm.googleapis.com/fcm/send';
                                $fields = array(
                                    'registration_ids' =>  $registrationIDs,
                                    'data' => $message
                                );
                                $headers = array(
                                    'Authorization: key='. $apiKey2,
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt( $ch, CURLOPT_URL, $url );
                                curl_setopt( $ch, CURLOPT_POST, true );
                                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

                                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ));
                                $result = curl_exec($ch);
                                curl_close($ch);
                            }else{

                            }$responseNotification=$this->Model_post->addNotification($addDataNotification);
                        }

                        $response = [
                            "status" => 1,
                            "message" => "Friend request has been successfully sent",
                            "data" => ''
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry! An error occurred, please try again",
                            "data" => ''
                        ];
                    }
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function acceptRemoveFriend_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif (!isset($data['friendID'])){
                $response = [
                    "status" => 0,
                    "message" => "Friend ID is required",
                    "data" => ''
                ];
            } elseif (!isset($data['type'])){
                $response = [
                    "status" => 0,
                    "message" => "Type is required, can be of Type accept or remove",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $friendID = $data['friendID'];
                $type = $data['type'];
                if($type === 'accept'){
                    $count = $this->Model_friend->acceptCheck($userID,$friendID);
                    if($count != ''){
                        $response_data = $this->Model_friend->accept($count);
                        if($response_data == 1){
                            $response = [
                                "status" => 1,
                                "message" => "Friend successfully Accepted",
                                "data" => ''
                            ];
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry!  There was a problem, please try again",
                                "data" => ''
                            ];
                        }
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "No friend found",
                            "data" => ''
                        ];
                    }
                }else if($type === 'remove'){
                    $count = $this->Model_friend->acceptCheck($userID,$friendID);
                    if($count != ''){
                        $response_data = $this->Model_friend->remove($count);
                        if($response_data == 1){
                            $response = [
                                "status" => 1,
                                "message" => "Friend successfully removed",
                                "data" => ''
                            ];
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry! There is a problem, please try again",
                                "data" => ''
                            ];
                        }
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "No friend found",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Type is missing, can be of Type Accept And Remove",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }
}