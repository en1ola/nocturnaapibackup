<?php defined('BASEPATH') OR exit('No direct script access allowed');

header('Content-Type: application/json');


class Services extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->model('Services_model');
		$this->load->helper('url');
		$this->load->library('email');
    }
	private $_jsonData = array();
	//************************** OWNER USER SERVICES CONTROLLER ***************************//

    public function index()
    {
        echo "Okay, routing works on this project. That's good progress";
    }

	public function ownerSignup(){
		$accountType        = $this->input->get_post('accountType');
		$userName           = $this->input->get_post('userName');
		$nameOfClub         = $this->input->get_post('nameOfClub');
		$startTime          = $this->input->get_post('startTime');
		$endTime         	= $this->input->get_post('endTime');
		$clubDis         	= $this->input->get_post('clubDis');
		$password           = $this->input->get_post('password');
		$confirmPassword    = $this->input->get_post('confirmPassword');
		$phoneNumber        = $this->input->get_post('phoneNumber');
		$address        	= $this->input->get_post('address');
		$email        	  = $this->input->get_post('email');
		$deviceToken        = $this->input->get_post('deviceToken');
		$clubType           = $this->input->get_post('clubType');
		$typeDevice         = $this->input->get_post('typeDevice');
		$profilePicture     = isset($_FILES["profilePicture"]["name"])?$_FILES["profilePicture"]["name"]:'';
		try{
			if($accountType == 'phone'){
				if($phoneNumber == FALSE || $phoneNumber == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Phone Number is Required";
					$this->_jsonData['data']='';
				}else{
					$phoneCount=$this->Services_model->phoneCount($phoneNumber);
					if($phoneCount>0){
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Number is already registered";
						$this->_jsonData['data']='';
					}else{
						$code = rand();
						$data=array(
							"phoneNumber"=>$phoneNumber,
							"verificationCode"=>$code
						);
						$phoneRegister=$this->Services_model->phoneRegister($data);
						
						$phone=$this->Services_model->phone($phoneRegister);
						
						$sendNumber = $phone[0]['phoneNumber'];
						$verificationCode = $phone[0]['verificationCode'];

						//todo: test that this url works
						//send phone verification code via sms to phone number
						$url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
							array(
							  'api_key' =>  '85495afd',
							  'api_secret' => '8140f35b6784c6f1',
							  'to' => $sendNumber,
							  'from' => 'Nocturna',
							  'text' => $verificationCode.": Verification Code"
							)
						);
						
						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$curl_response = curl_exec($ch);
						
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Account Created Successfully.";
						$this->_jsonData['data']=$phone;
					}
				}

			}else if($accountType == 'menual'){
				if($userName == FALSE || $userName == '') {
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="User Name is Required";
					$this->_jsonData['data']='';
				}else if($nameOfClub == FALSE || $nameOfClub == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Name Of Club is Required";
					$this->_jsonData['data']='';
				}else if($password == FALSE || $password == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Password is Required";
					$this->_jsonData['data']='';
				}else if($confirmPassword == FALSE || $confirmPassword == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Confirm Password is Required";
					$this->_jsonData['data']='';
				}else if($confirmPassword != $password){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Password do not match";
					$this->_jsonData['data']='';
				}else if($phoneNumber == FALSE || $phoneNumber == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Phone Number is Required";
					$this->_jsonData['data']='';
				}else if($address == FALSE || $address == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Address is Required";
					$this->_jsonData['data']='';
				}else if($deviceToken == FALSE || $deviceToken == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Device Token is Required";
					$this->_jsonData['data']='';
				}else if($typeDevice == FALSE || $typeDevice == ''){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Type of device is Required";
					$this->_jsonData['data']='';
				}else{
					if($profilePicture == ''){
						$Address = urlencode($address);
						$api_key = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
						$request_url2 = "https://maps.googleapis.com/maps/api/geocode/xml?address=$Address&key=$api_key";
						//$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
						$xml = simplexml_load_file($request_url2) or die("url not loading");
						$status = $xml->status;

						if ($status=="OK"){
							$lat  = $xml->result->geometry->location->lat;
							$long = $xml->result->geometry->location->lng;
						} else {
                            $lat = null;
                            $long = null;

                            $this->_jsonData['status']=0;
                            $this->_jsonData['message']="Latitude and Longitude could not be generated from the given address";
                            $this->_jsonData['data']=$Address;
                        }
						$countRow = $this->Services_model->checkUserOwner($userName);
						if($countRow>0){
							$this->_jsonData['status']=0;
							$this->_jsonData['message']="This username already exists";
							$this->_jsonData['data']='';
						}else{
							$data=array(
								"userName"          =>$userName,
								"nameOfClub"        =>$nameOfClub,							
								"startTime"         =>$startTime,
								"endTime"           =>$endTime,
								"clubDis"           =>$clubDis,							
								"password"          =>$password,
								"phoneNumber"       =>$phoneNumber,
								"address"   		   =>$address,
								"email"        	 =>$email,
								"deviceToken"       =>$deviceToken,							
								"clubType"          =>$clubType,							
								"typeDevice"        =>$typeDevice,
								"lat" 		       =>$lat,
								"long"        	  =>$long,
								"accountActive"     =>"active",
								"audio"     		 =>""
							);
							$id = $this->Services_model->ownerSignup($data);
							$result = $this->Services_model->ownerSignupData($id);
							$this->_jsonData['status']=1;
							$this->_jsonData['message']="Owner User Signup Result";
							$this->_jsonData['data']= (array)$result;
						}
					}else{
						$Address = urlencode($address);
                        $api_key = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
						$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
                        $request_url2 = "https://maps.googleapis.com/maps/api/geocode/xml?address=$Address&key=$api_key";
						$xml = simplexml_load_file($request_url2) or die("url not loading");
						$status = $xml->status;
						if ($status=="OK"){
							$lat  = $xml->result->geometry->location->lat;
							$long = $xml->result->geometry->location->lng;
						}
						$profilePicture = mt_rand().".jpg";
						move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/owner_user/profile/".$profilePicture);						
						$countRow = $this->Services_model->checkUserOwner($userName);
						if($countRow>0){
							$this->_jsonData['status']=0;
							$this->_jsonData['message']="This User Name is Already Exist";
							$this->_jsonData['data']='';
						}else{
							$data=array(
								"userName"          =>$userName,
								"nameOfClub"        =>$nameOfClub,
								"startTime"         =>$startTime,
								"endTime"           =>$endTime,
								"clubDis"           =>$clubDis,
								"password"          =>$password,
								"phoneNumber"       =>$phoneNumber,
								"address"   		   =>$address,
								"email"        	 =>$email,
								"deviceToken"       =>$deviceToken,
								"clubType"          =>$clubType,
								"typeDevice"        =>$typeDevice,	
								"profile"           =>$profilePicture,
								"lat" 		       =>$lat,
								"long"        	  =>$long,
								"accountActive"     =>"active",
								"audio"     		 =>""
							);
							$id = $this->Services_model->ownerSignup($data);
							$result = $this->Services_model->ownerSignupData($id);
							$this->_jsonData['status']=1;
							$this->_jsonData['message']="Owner User Signup Result";
							$this->_jsonData['data']=$result;
						}
					}
				}
			}else{
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Account Type is required";
				$this->_jsonData['data']='';
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occurred";
			$this->_jsonData['data']='';
		}
	}

	public function vericitionCode(){
		$userID      = $this->input->get_post('userID');
		$code        = $this->input->get_post('code');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="user ID is Required";
				$this->_jsonData['data']='';
			}else if($code == FALSE || $code == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Code is Required";
				$this->_jsonData['data']='';
			}else{
				$countUser=$this->Services_model->countUser($userID);
				if($countUser[0]['verificationCode'] == $code){
					$updateStatus=$this->Services_model->updateStatus($userID);
					if($updateStatus == TRUE){
						$data=array("id"=>$userID);
						$result=$this->Services_model->ownerLogin($data);
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Owner user details";
						$this->_jsonData['data']=$result;
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Code Error";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}


	public function ownerLogin(){
		$userName = $this->input->get_post('userName');
		$password = $this->input->get_post('password');

		try{
			if($userName == FALSE || $userName == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="User Name is Required";
				$this->_jsonData['data']='';
			}else if($password == FALSE || $password == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Password is Required";
				$this->_jsonData['data']='';
			}else{
				$data=array(
					"userName"			=>$userName,
					"password"			=>$password,
					"accountActive"       =>"active"
				);
				$rowCount=$this->Services_model->ownerLoginCount($data);
				if($rowCount>0){
					$result=$this->Services_model->ownerLogin($data);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']="Owner Login Details.";
					$this->_jsonData['data']=$result;
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="No user found.";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occurred";
			$this->_jsonData['data']='';
		}
	}
	public function ownerEditProfile(){
		$userID             = $this->input->get_post('userID');
		$nameOfClub         = $this->input->get_post('nameOfClub');
		$password           = $this->input->get_post('password');
		$phoneNumber        = $this->input->get_post('phoneNumber');
		
		$startTime          = $this->input->get_post('startTime');
		$endTime        	= $this->input->get_post('endTime');
		$clubDis        	= $this->input->get_post('clubDis');
		$address        	= $this->input->get_post('address');
		$email        	  = $this->input->get_post('email');
		$clubType           = $this->input->get_post('clubType');
		
		$ownerProfile       = isset($_FILES["ownerProfile"]["name"])?$_FILES["ownerProfile"]["name"]:'';
		$ownerCover         = isset($_FILES["ownerCover"]["name"])?$_FILES["ownerCover"]["name"]:'';
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="User ID is Required";
				$this->_jsonData['data']='';
			}else{
				if($ownerProfile == '' && $ownerCover == ''){
					$data = array(
						"nameOfClub"  =>$nameOfClub,
						"password"    =>$password,
						"phoneNumber" =>$phoneNumber,
						"startTime"   =>$startTime,
						"endTime" 	 =>$endTime,
						"clubDis" 	 =>$clubDis,
						"address" 	 =>$address,
						"email" 	   =>$email,
						"clubType" 	=>$clubType
					);
					$updatePicture=$this->Services_model->ownerUpdatePicture($userID,$data);
					if($updatePicture == TRUE){
						$data=array(
							"id"			=>$userID
						);
						$result=$this->Services_model->ownerLogin($data);
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Profile Update Successfully";
						$this->_jsonData['data']=$result;
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Sorry there is some problem";
						$this->_jsonData['data']='';
					}
				}else if($ownerProfile != '' && $ownerCover == ''){
					$ownerProfile = mt_rand().".jpg";
					$data=array(
						"id"=>$userID
					);
					$Count=$this->Services_model->ownerUserPicture($data);
					if($Count>0){
						$getPicture=$this->Services_model->ownerGetPicture($data);
						$profile = $getPicture[0]['profile'];
						if($profile == ''){
							$data=array('profile'=>$ownerProfile);
							$updatePicture=$this->Services_model->ownerUpdatePicture($userID,$data);
							if($updatePicture == TRUE){
								move_uploaded_file($_FILES["ownerProfile"]["tmp_name"], getcwd() . "/uploads/owner_user/profile/" . $ownerProfile);
								$data=array(
									"id"			=>$userID
								);
								$result=$this->Services_model->ownerLogin($data);
								$this->_jsonData['status']=1;
								$this->_jsonData['message']="Profile Update Successfully";
								$this->_jsonData['data']=$result;
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="Sorry there is some problem";
								$this->_jsonData['data']='';
							}
						}else{
							$unlink = unlink(getcwd() . "/uploads/owner_user/profile/" . $profile);
							if($unlink==1){
								$data=array('profile'=>$ownerProfile);
								$updatePicture=$this->Services_model->ownerUpdatePicture($userID,$data);
								if($updatePicture == TRUE){
									move_uploaded_file($_FILES["ownerProfile"]["tmp_name"], getcwd() . "/uploads/owner_user/profile/" . $ownerProfile);	
									
									$data=array(
										"id"			=>$userID
									);
									$result=$this->Services_model->ownerLogin($data);
																	
									$this->_jsonData['status']=1;
									$this->_jsonData['message']="Profile Update Successfully";
									$this->_jsonData['data']=$result;
								}else{
									$this->_jsonData['status']=0;
									$this->_jsonData['message']="Sorry there is some problem";
									$this->_jsonData['data']='';
								}
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="There is some problem our database";
								$this->_jsonData['data']='';
							}
						}
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="No user found";
						$this->_jsonData['data']='';
					}
				}else if($ownerProfile == '' && $ownerCover != ''){
					$ownerCover = mt_rand().".jpg";
					$data=array(
						"id"=>$userID
					);
					$Count=$this->Services_model->ownerUserPicture($data);
					if($Count>0){
						$getPicture=$this->Services_model->ownerGetPicture($data);
						
						$profile = $getPicture[0]['cover'];
						
						if($profile == ''){							
							$data=array('cover'=>$ownerCover);
							$updatePicture=$this->Services_model->ownerUpdatePicture($userID,$data);
							if($updatePicture == TRUE){
								move_uploaded_file($_FILES["ownerCover"]["tmp_name"], getcwd() . "/uploads/owner_user/cover/" . $ownerCover);								
								$data=array(
									"id"			=>$userID
								);
								$result=$this->Services_model->ownerLogin($data);
																
								$this->_jsonData['status']=1;
								$this->_jsonData['message']="Cover Upload Successfully";
								$this->_jsonData['data']=$result;
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="Sorry there is some problem";
								$this->_jsonData['data']='';
							}
						}else{
							$unlink = unlink(getcwd() . "/uploads/owner_user/cover/" . $profile);
							if($unlink==1){
								$data=array('cover'=>$ownerCover);
								$updatePicture=$this->Services_model->ownerUpdatePicture($userID,$data);
								if($updatePicture == TRUE){
									move_uploaded_file($_FILES["ownerCover"]["tmp_name"], getcwd() . "/uploads/owner_user/cover/" . $ownerCover);									
									$data=array(
										"id"			=>$userID
									);
									$result=$this->Services_model->ownerLogin($data);
																	
									$this->_jsonData['status']=1;
									$this->_jsonData['message']="Cover Upload Successfully";
									$this->_jsonData['data']=$result;
								}else{
									$this->_jsonData['status']=0;
									$this->_jsonData['message']="Sorry there is some problem";
									$this->_jsonData['data']='';
								}
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="There is some problem our database";
								$this->_jsonData['data']='';
							}
						}
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="No user found";
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Required perameters";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function uploadAudio(){
		$userID             = $this->input->get_post('userID');
		$audio	    	  = isset($_FILES["audio"]["name"])?$_FILES["audio"]["name"]:'';
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='userID is required';
				$this->_jsonData['data']='';	
			}else if($audio == FALSE || $audio == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='audio is required';
				$this->_jsonData['data']='';
			}else{
				$audio = mt_rand().".mp3";
				$data=array(
					"id"=>$userID
				);
				$Count=$this->Services_model->ownerUserPicture($data);
				if($Count>0){
					$getPicture=$this->Services_model->ownerGetPicture($data);
					$audioGet = $getPicture[0]['audio'];
					if($audioGet == ''){
						$data=array('audio'=>$audio);
						$updatePicture=$this->Services_model->ownerUpdatePicture($userID,$data);
						if($updatePicture == TRUE){
							$move = move_uploaded_file($_FILES["audio"]["tmp_name"], getcwd() . "/uploads/owner_user/audio/" . $audio);
							if($move == 1){
								$data=array(
									"id"			=>$userID
								);
								$result=$this->Services_model->ownerLogin($data);
								$this->_jsonData['status']=1;
								$this->_jsonData['message']="audio Update Successfully";
								$this->_jsonData['data']=$result;
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="Sorry! file was not move.";
								$this->_jsonData['data']="";
							}
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']="Sorry there is some problem";
							$this->_jsonData['data']='';
						}
					}else{
						$unlink = unlink(getcwd() . "/uploads/owner_user/audio/" . $audioGet);
						if($unlink==1){
							$data=array('audio'=>$audio);
							$updatePicture=$this->Services_model->ownerUpdatePicture($userID,$data);
							if($updatePicture == TRUE){
								$move=move_uploaded_file($_FILES["audio"]["tmp_name"], getcwd() . "/uploads/owner_user/audio/" . $audio);									
								if($move == 1){
									$data=array(
										"id"			=>$userID
									);
									$result=$this->Services_model->ownerLogin($data);
									$this->_jsonData['status']=1;
									$this->_jsonData['message']="audio Update Successfully";
									$this->_jsonData['data']=$result;
								}else{
									$this->_jsonData['status']=0;
									$this->_jsonData['message']="Sorry! file was not move.";
									$this->_jsonData['data']="";
								}
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="Sorry there is some problem";
								$this->_jsonData['data']='';
							}
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']="There is some problem our database";
							$this->_jsonData['data']='';
						}
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='No user found';
					$this->_jsonData['data']='';	
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';	
		}
	}
	public function addPackages(){
		$ownerID = $this->input->get_post('ownerID');
		$nameOfClub = $this->input->get_post('nameOfClub');
		$price = $this->input->get_post('price');
		$description_attributes = $this->input->get_post('description_attributes');
		$validity = $this->input->get_post('validity');
		$discount = $this->input->get_post('discount');
		$image = isset($_FILES["image"]["name"])?$_FILES["image"]["name"]:'';
		$date = date('Y-m-d H:i:s');
		try{
			if($ownerID == FALSE || $ownerID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Owner ID is required";
				$this->_jsonData['data']='';
			}else if($nameOfClub == FALSE || $nameOfClub == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Name Of Club is required";
				$this->_jsonData['data']='';
			}else if($price == FALSE || $price == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Price is required";
				$this->_jsonData['data']='';
			}else if($description_attributes == FALSE || $description_attributes == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Description Attributes is required";
				$this->_jsonData['data']='';
			}else if($image == FALSE || $image == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Image is required";
				$this->_jsonData['data']='';
			}else{
				$image = mt_rand().".jpg";
				
				$discountNumber = intval($discount);
				$packageImage = move_uploaded_file($_FILES["image"]["tmp_name"], getcwd() . "/uploads/owner_user/package/" . $image);	
				if($packageImage == 1){
					$data=array(
						"ownerID"                  =>$ownerID,
						"nameOfClub"               =>$nameOfClub,
						"price"                    =>$price,
						"description_attributes"   =>$description_attributes,
						"validity"                 =>$validity,
						"discount"                 =>$discountNumber."% off",
						"image"                    =>$image,
						"date"                     =>$date
					);
					$response=$this->Services_model->addPackages($data);
					if($response != ''){
						$getPackage=$this->Services_model->GetPackages($response);
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Package Successfully inserted.";
						$this->_jsonData['data']=$getPackage;
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Package not inserted.";
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry there was a problem please try again later.';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}
	}
	public function packageUpdate(){
		$packageID                      = $this->input->get_post('packageID');
		$nameOfClub                     = $this->input->get_post('nameOfClub');
		$price                          = $this->input->get_post('price');
		$description_attributes         = $this->input->get_post('description_attributes');
		$discount                       = $this->input->get_post('discount');
		$image        				  = isset($_FILES["image"]["name"])?$_FILES["image"]["name"]:'';
		try{
			if($packageID == FALSE || $packageID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='PackageID is Required';
				$this->_jsonData['data']='';
			}else{
				if($image == FALSE || $image == ''){
					$data=array(
						"nameOfClub"               =>$nameOfClub,
						"price"                    =>$price,
						"description_attributes"   =>$description_attributes,
						"discount"                 =>$discount
					);
					$responce=$this->Services_model->packageUpdate($packageID,$data);
					if($responce == TRUE){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Package Update Successfully';
						$this->_jsonData['data']='';
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! Package Not Updated';
						$this->_jsonData['data']='';
					}
				}else{$image        				  = rand().".jpg";
					$getImage=$this->Services_model->getPackageImage($packageID);
					if($getImage[0]['image'] == FALSE || $getImage[0]['image'] == ''){
						$move = move_uploaded_file($_FILES["image"]["tmp_name"], getcwd() . "/uploads/owner_user/package/" . $image);
						if($move == 1){
							$data=array("image"=>$image);
							$responce=$this->Services_model->packageUpdate($packageID,$data);
							if($responce == TRUE){
								$this->_jsonData['status']=1;
								$this->_jsonData['message']='Package Image Successfully Updated';
								$this->_jsonData['data']='';
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']='Sorry! Name is not Insert';
								$this->_jsonData['data']='';
							}
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']='Sorry! File was not Move';
							$this->_jsonData['data']='';
						}
					}else{
						$getImage=$this->Services_model->getPackageImage($packageID);
						$unlink = unlink(getcwd() . "/uploads/owner_user/package/" . $getImage[0]['image']);
						if($unlink == 1){
							$move = move_uploaded_file($_FILES["image"]["tmp_name"], getcwd() . "/uploads/owner_user/package/" . $image);
							if($move == 1){
							$data=array("image"=>$image);
							$responce=$this->Services_model->packageUpdate($packageID,$data);
							if($responce == TRUE){
								$this->_jsonData['status']=1;
								$this->_jsonData['message']='Package Image Successfully Updated';
								$this->_jsonData['data']='';
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']='Sorry! Name is not Insert';
								$this->_jsonData['data']='';
							}
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']='Sorry! File was not Move';
							$this->_jsonData['data']='';
						}						
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']='Sorry! File was not Remove';
							$this->_jsonData['data']='';
						}
					}
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';
		}	
	}
	public function deletePackage(){
		$packageID                      = $this->input->get_post('packageID');
		try{
			if($packageID == FALSE || $packageID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='PackageID is Required';
				$this->_jsonData['data']='';
			}else{
				$responce=$this->Services_model->deletePackage($packageID);
				if($responce == 1){
					$this->_jsonData['status']=1;
					$this->_jsonData['message']='Package Delete Successfully.';
					$this->_jsonData['data']='';
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Request Failed';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
		}
	}
	public function ownerUpdateTokenDevice(){
		$userID             = $this->input->get_post('userID');
		$deviceToken        = $this->input->get_post('deviceToken');
		$typeDevice         = $this->input->get_post('typeDevice');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';	
			}else if($deviceToken == FALSE || $deviceToken == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Device Token is Required';
				$this->_jsonData['data']='';	
			}else if($typeDevice == FALSE || $typeDevice == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Device Type is Required';
				$this->_jsonData['data']='';	
			}else{
				$data=array("deviceToken"=>$deviceToken,"typeDevice"=>$typeDevice);
				$count=$this->Services_model->ownerUpdateTokenDeviceCount($userID,$data);
				if($count>0){
					$responce=$this->Services_model->ownerUpdateTokenDevice($userID,$data);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']='Device Token and Device Type update';
					$this->_jsonData['data']=$responce;
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry Device Token and Device Type not update';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}
	}

	//************************** NORMAL USER SERVICES CONTROLLER ***************************//
	public function normalSignup(){
		$userName           = $this->input->get_post('userName');
		$fullName           = $this->input->get_post('fullName');
		$email              = $this->input->get_post('email');
		$password           = $this->input->get_post('password');
		$confirmPassword    = $this->input->get_post('confirmPassword');
		$deviceToken        = $this->input->get_post('deviceToken');
		$typeDevice         = $this->input->get_post('typeDevice');
		$profilePicture     = isset($_FILES["profilePicture"]["name"])?$_FILES["profilePicture"]["name"]:'';
		
		try{
			if($userName == FALSE || $userName == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="User Name is Required";
				$this->_jsonData['data']='';
			}else if($fullName == FALSE || $fullName == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Full Name is Required";
				$this->_jsonData['data']='';
			}else if($password == FALSE || $password == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Password is Required";
				$this->_jsonData['data']='';
			}else if($confirmPassword == FALSE || $confirmPassword == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Confirm Password is Required";
				$this->_jsonData['data']='';
			}else if($confirmPassword != $password){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Password is not match";
				$this->_jsonData['data']='';
			}else if($deviceToken == FALSE || $deviceToken == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Device Token is Required";
				$this->_jsonData['data']='';
			}else if($typeDevice == FALSE || $typeDevice == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Type of device is Required";
				$this->_jsonData['data']='';
			}else{
				if($email == ''){
					$email = " ";
				}
				$countRow = $this->Services_model->checkUserNormal($userName);
				if($countRow>0){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="This username already exists";
					$this->_jsonData['data']='';
				}else{
					if($profilePicture == FALSE || $profilePicture == ''){
						$data=array(
							"userName"          =>$userName,
							"fullName"          =>$fullName,
							"email"             =>$email,
							"password"          =>$password,
							"deviceToken"       =>$deviceToken,
							"typeDevice"        =>$typeDevice
						);
						$id = $this->Services_model->normalSignup($data);
						$result = $this->Services_model->normalSignupData($id);
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="User Signup Result";
						$this->_jsonData['data']=$result;
					}else{
						$profilePicture = mt_rand().".jpg";
						move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/normal_user/profile/".$profilePicture);						
						$data=array(
							"userName"          =>$userName,
							"fullName"          =>$fullName,
							"email"             =>$email,
							"password"          =>$password,
							"deviceToken"       =>$deviceToken,
							"typeDevice"        =>$typeDevice,
							"profilePicture"    =>$profilePicture
						);
						$id = $this->Services_model->normalSignup($data);
						$result = $this->Services_model->normalSignupData($id);
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Normal User Signup Result";
						$this->_jsonData['data']=$result;						
					}
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occurred";
			$this->_jsonData['data']='';
		}
	}


	public function normalLogin(){
		$userName = $this->input->get_post('userName');
		$password = $this->input->get_post('password');
		try{
			if($userName == FALSE || $userName == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="User Name is Required";
				$this->_jsonData['data']='';
			}else if($password == FALSE || $password == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="Password is Required";
				$this->_jsonData['data']='';
			}else{
				$data=array(
					"userName"=>$userName,
					"password"=>$password
				);
				$rowCount=$this->Services_model->normalLoginCount($data);
				if($rowCount>0){
					$result=$this->Services_model->normalLogin($data);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']="Normal Login Details.";
					$this->_jsonData['data']=$result;
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="No user found.";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occurred";
			$this->_jsonData['data']='';
		}
	}

	public function normalUsers(){
		error_reporting(0);
		$userID 		= $this->input->get_post('userID');
		$type 		  = $this->input->get_post('type');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="User ID is Required";
				$this->_jsonData['data']='';
			}else{
				if($type == 'package'){
					$data=array(
						"id"=>$userID
					);
					$rowCount=$this->Services_model->ownerLoginCount($data);
					if($rowCount>0){
						$result=$this->Services_model->ownerLogin($data);
						echo json_encode(array("status"=>1,"message"=>"Owner Details.","data"=>$result));
					}else{
						echo json_encode(array("status"=>0,"message"=>"No user found.","data"=>""));
					}
				}else{
					$data=array(
						"id"=>$userID
					);
					$getPlacesCount=$this->Services_model->getPlacesCount($userID);
					if($getPlacesCount>0){
						$getPlaces=$this->Services_model->getPlaces($userID);
						foreach($getPlaces as $places){
							$placesArray[]=$places['checkIn'];
						}
						$commaPlaces=implode(" - ",$placesArray);
					}
					$rowCount=$this->Services_model->normalLoginCount($data);
					if($rowCount>0){
						$result=$this->Services_model->normalUsersDetails($data);
						if($commaPlaces == ''){
							$commaPlaces = 'No places';
						}
						foreach($result as $res){
							$rowArray['id']=$res['id'];
							$rowArray['userName']=$res['userName'];
							$rowArray['fullName']=$res['fullName'];
							$rowArray['phoneNumber']=$res['phoneNumber'];
							$rowArray['mostVisitedPlaces']=$commaPlaces;
							$rowArray['dis']=$res['dis'];
							$rowArray['profilePicture']=$res['profilePicture'];
						}
						echo json_encode(array("status"=>1,"message"=>"users Details","data"=>$rowArray));
					}else{
						echo json_encode(array("status"=>0,"message"=>"No user found.","data"=>""));
					}
				}
			}
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occurred";
			$this->_jsonData['data']='';
		}
	}

	public function status(){
		$userID 		              = $this->input->get_post('userID');
		$picture                     = isset($_FILES["picture"]["name"])?$_FILES["picture"]["name"]:'';
		$status 		              = $this->input->get_post('status');
		$checkin	                 = $this->input->get_post('checkin');
		$video	                   = $this->input->get_post('video');
		$statusType	              = $this->input->get_post('statusType');
		$date                        = date('Y-m-d H:i:s');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else{
				if($statusType == 'picture'){
					$picture = mt_rand().".jpg";
					$move = move_uploaded_file($_FILES["picture"]["tmp_name"], getcwd() . "/uploads/pictureStatus/" . $picture);
					if($move == 1){
						$dataInsert=array("userID"=>$userID,"picture"=>$picture,"status"=>$status,"statusType"=>$statusType,"date"=>$date);
                        $response=$this->Services_model->status($dataInsert);
						if($response != ''){
							$this->_jsonData['status']=1;
							$this->_jsonData['message']='Status Upload Successfully';
							$this->_jsonData['data']=$response;
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']='Sorry! a problem occurred';
							$this->_jsonData['data']='';
						}
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! File was not Moved';
						$this->_jsonData['data']='';
					}
				}else if($statusType == 'checkin'){
					$dataInsert=array("userID"=>$userID,"checkIn"=>$checkin,"status"=>$status,"statusType"=>$statusType,"date"=>$date);
                    $response=$this->Services_model->status($dataInsert);
					if($response != ''){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Status Upload Successfully';
						$this->_jsonData['data']=$response;
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! a problem occurred';
						$this->_jsonData['data']='';
					}
				}else if($statusType == 'video'){
					$video = mt_rand().".mp4";
					$move = move_uploaded_file($_FILES["video"]["tmp_name"], getcwd() . "/uploads/videoStatus/".$video);		
					if($move == 1){
						$dataInsert=array("userID"=>$userID,"status"=>$status,"video"=>$video,"statusType"=>$statusType,"date"=>$date);
                        $response=$this->Services_model->status($dataInsert);
						if($response != ''){
							$this->_jsonData['status']=1;
							$this->_jsonData['message']='Video Upload Successfully';
							$this->_jsonData['data']=$response;
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']='Sorry! a problem occurred';
							$this->_jsonData['data']='';
						}
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! File was not moved.';
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Status Type is not Defined';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}
	}


	public function statusEdit(){
		$statusID	= $this->input->get_post('statusID');
		$status	= $this->input->get_post('status');
		try{
			if($statusID == FALSE || $statusID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Status ID is required';
				$this->_jsonData['data']='';
			}else{
				$countStatus=$this->Services_model->countStatus($statusID);
				if($countStatus>0){
					$responce=$this->Services_model->statusEdit($statusID,$status);
					if($responce == TRUE){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Status Update Successfully';
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='There are no status found';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';
		}
	}
	public function statusDelete(){
		$statusID	= $this->input->get_post('statusID');
		try{
			if($statusID == FALSE || $statusID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Status ID is required';
				$this->_jsonData['data']='';
			}else{
				$countStatus=$this->Services_model->countStatus($statusID);
				if($countStatus>0){
					$responce=$this->Services_model->statusDelete($statusID);
					if($responce == 1){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Status Delete Successfully';
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='There are no status found';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';
		}
	}
	public function like(){
		$userID = $this->input->get_post('userID');
		$statusid  = $this->input->get_post('statusid');
		$date  = date('Y-m-d H:i:s');

		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else if($statusid == FALSE || $statusid == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Status ID is Required';
				$this->_jsonData['data']='';
			}else{
				
				$data = array(
					"userID"   => $userID,
					"statusid" => $statusid
				);
				$Count=$this->Services_model->likeCheck($data);
				if($Count>0){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry! You already Like this post.';
					$this->_jsonData['data']="";
				} else{
				    $response=$this->Services_model->like($data);
					if($response != ''){
						$notificationGetResult=$this->Services_model->notificationLikes($response);
						
						foreach($notificationGetResult as $dt){
							if($userID == $dt['userID']){

                            }else{
								$addDataNotification = array(
								"userID"		  =>$dt['userID'],
								"friendID"		=>$userID,
								"text"			=>$dt['userName']." Like your Post",
								"notificationType"=>"like",
								"isRead"		  =>"0",
								"date"			=>$date
                                );

								$pushID=$dt['userID'];
								$pushNotification=$this->Services_model->pushNotification($pushID);
								if($pushNotification[0]['typeDevice'] == 'ios'){
									$deviceToken = $pushNotification[0]['deviceToken'];
									$passphrase = 'abcd';
									$message = $dt['userName']." Like your Post";
									$ctx = stream_context_create();
									stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
									stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
									$fp = stream_socket_client(
										'ssl://gateway.sandbox.push.apple.com:2195', $err,
										$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

									if (!$fp)
									    exit("Failed to connect: $err $errstr" . PHP_EOL);
									$body['aps'] = array(
										'alert' => $message,
										'sound' => 'default'
									);
									$payload = json_encode($body);
									$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) .pack('n', strlen($payload)) . $payload;

									$result = fwrite($fp, $msg, strlen($msg));
									fclose($fp);
								} else if($pushNotification[0]['typeDevice'] == 'android'){
									$apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
									$registrationIDs = $pushNotification[0]['deviceToken'];
									$message = array(
										"message"      		  => $dt['userName']." Like your Post",
										"title"  				=> "Nocturna",
										"vibrate" 	  		  => 1,
										"sound"  				=> 1,
										"smallIcon" 			=> "small_icon"
									);
									$url = 'https://fcm.googleapis.com/fcm/send';
									$fields = array(
									   'registration_ids'      =>  $registrationIDs,
									   'data'				  =>  $message 
									);
									$headers = array( 
									   'Authorization: key='. $apiKey,
									   'Content-Type: application/json'
									);
									$ch = curl_init();
									curl_setopt( $ch, CURLOPT_URL, $url );
									curl_setopt( $ch, CURLOPT_POST, true );
									curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
									curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
									
									curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
									$result = curl_exec($ch);
									curl_close($ch);
								} else{
									
								}
							}
							$responseNotification=$this->Services_model->addNotification($addDataNotification);
						}
						
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Like! Successfully updated.';
						$this->_jsonData['data']="";
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! there is a problem updating like.';
						$this->_jsonData['data']='';
					}
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}	
	}


	public function comment(){
		$userID = $this->input->get_post('userID');
		$statusid = $this->input->get_post('statusid');
		$comment = $this->input->get_post('comment');
		$date = date('Y-m-d H:i:s');

		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else if($statusid == FALSE || $statusid == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Status ID is Required';
				$this->_jsonData['data']='';
			}else if($comment == FALSE || $comment == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Comment is Required';
				$this->_jsonData['data']='';
			}else{
				
				$data = array(
					"userID"   => $userID,
					"statusid" => $statusid,
					"comment"  => $comment
				);
				$response=$this->Services_model->comment($data);
				if($response != ''){
					$notificationGetResult=$this->Services_model->notificationComments($response);
					foreach($notificationGetResult as $dt){
						if($userID == $dt['userID']){
						}else{
						    $addDataNotification = array(
						        "userID"=>$dt['userID'],
							    "friendID"=>$userID,
							    "text"=>$dt['userName']." Commented on your Post",
							    "notificationType"=>"comment",
							    "isRead"=>"0",
							    "date"=>$date);

						    $pushID=$dt['userID'];
						    $pushNotification=$this->Services_model->pushNotification($pushID);
						    if($pushNotification[0]['typeDevice'] == 'ios'){
						        $deviceToken = $pushNotification[0]['deviceToken'];
								$passphrase = 'abcd';
								$message = $dt['userName']." Commented on your Post";
								$ctx = stream_context_create();
								stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
								stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
								$fp = stream_socket_client(
									'ssl://gateway.sandbox.push.apple.com:2195', $err,
									$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
								if (!$fp)
								    exit("Failed to connect: $err $errstr" . PHP_EOL);

								$body['aps'] = array(
								    'alert' => $message,
									'sound' => 'default'
                                );
								$payload = json_encode($body);
								$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken)
                                    .pack('n', strlen($payload)) . $payload;
								$result = fwrite($fp, $msg, strlen($msg));
								fclose($fp);

						    }else if($pushNotification[0]['typeDevice'] == 'android'){
								$apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
								$apiKey2 = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
								$registrationIDs = $pushNotification[0]['deviceToken'];
								$message = array(
									"message"      		  => $dt['userName']." Commented on your Post",
									"title"  				=> "Nocturna",
									"vibrate" 	  		  => 1,
									"sound"  				=> 1,
									"smallIcon" 			=> "small_icon"
								);
								$url = 'https://fcm.googleapis.com/fcm/send';
								$fields = array(
								   'registration_ids'      =>  $registrationIDs,
								   'data'				  =>  $message 
								);
								$headers = array( 
								   'Authorization: key='. $apiKey2,
								   'Content-Type: application/json'
								);
								$ch = curl_init();
								curl_setopt( $ch, CURLOPT_URL, $url );
								curl_setopt( $ch, CURLOPT_POST, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								
								curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
								$result = curl_exec($ch);
								curl_close($ch);
							}else{
								
							}
						}
						$responseNotification=$this->Services_model->addNotification($addDataNotification);
					}
					
					$this->_jsonData['status']=1;
					$this->_jsonData['message']='Comment! Successfully updated.';
					$this->_jsonData['data']="";
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry! there is a problem, please try again.';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}	
	}

	public function getAllComments(){
		$statusID = $this->input->get_post('statusID');
		try{
			if($statusID == FALSE || $statusID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Status ID is Required';
				$this->_jsonData['data']='';
			}else{
				$count = $this->Services_model->getCommentsCount($statusID);
				if($count>0){
					$response = $this->Services_model->getAllComments($statusID);
					if($response != ''){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Get All Comments.';
						$this->_jsonData['data']=$response;
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! There is a problem.';
						$this->_jsonData['data']='';
					}					
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry! No status found.';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';	
		}	
	}


	public function newsFeeds(){
        $userID = $this->input->get_post('userID');
		$ids = array();
		$ids[] = $userID;
		$JSON = array();
		try{
			if($userID == FALSE || $userID == ''){
				echo json_encode(array("status"=>0,"message"=>"User ID is Required","data"=>""));
			}else{
				$getFriendsCount=$this->Services_model->getFriendsCount($userID);
				if($getFriendsCount>0){
					$getFriends=$this->Services_model->getFriends($userID);
					foreach($getFriends as $getIDS){
						if($getIDS["friendID"] == $userID) {
							$ids[] = $getIDS["userID"];
						}
						else {
							$ids[] = $getIDS["friendID"];
						}
					}
				}else{
				}
				$comma_separated = implode(",", $ids);
				
				$count=$this->Services_model->newsFeedsCheck($comma_separated,$userID);
				if($count>0){
					$response=$this->Services_model->newsFeeds($comma_separated,$userID);
					foreach($response as $data){
						$date = $this->time_elapsed_string($data['date']);
						$rowArray=array();
						if($data['statusType'] == 'picture'){
							$rowArray['id']=$data['id'];
							$rowArray['userID']=$data['userID'];
							$rowArray['picture']=$data['picture'];
							$rowArray['status']=$data['status'];
							$rowArray['statusType']=$data['statusType'];
							$rowArray['comments']=$data['comments'];
							$rowArray['likes']=$data['likes'];
							$rowArray['isLike']=$data['isLike'];
							$rowArray['userName']=$data['userName'];
							$rowArray['userPicture']=$data['userPicture'];
							$rowArray['ago']=$date;
						}else if($data['statusType'] == 'checkin'){
							$rowArray['id']=$data['id'];
							$rowArray['userID']=$data['userID'];
							$rowArray['status']=$data['status'];
							$rowArray['checkin']=$data['checkin'];
							$rowArray['statusType']=$data['statusType'];
							$rowArray['comments']=$data['comments'];
							$rowArray['likes']=$data['likes'];
							$rowArray['isLike']=$data['isLike'];
							$rowArray['userName']=$data['userName'];
							$rowArray['userPicture']=$data['userPicture'];
							$rowArray['ago']=$date;
						}else if($data['statusType'] == 'video'){
							$rowArray['id']=$data['id'];
							$rowArray['userID']=$data['userID'];
							$rowArray['status']=$data['status'];
							$rowArray['video']=$data['video'];
							$rowArray['statusType']=$data['statusType'];
							$rowArray['comments']=$data['comments'];
							$rowArray['likes']=$data['likes'];
							$rowArray['isLike']=$data['isLike'];
							$rowArray['userName']=$data['userName'];
							$rowArray['userPicture']=$data['userPicture'];
							$rowArray['ago']=$date;
						}else if($data['statusType'] == 'package'){
							$rowArray['id']=$data['id'];
							$rowArray['userID']=$data['userID'];
							$rowArray['picture']=$data['picture'];
							$rowArray['description_attributes']=$data['status'];
							$rowArray['packageName']=$data['checkin'];
							$rowArray['discount']=$data['video'];
							$rowArray['statusType']=$data['statusType'];
							$rowArray['userName']=$data['userName'];
							$rowArray['userPicture']=$data['userPicture'];
							$rowArray['ago']=$date;
						}else{
						}
						array_push($JSON,$rowArray);
					}
					echo json_encode(array("status"=>1,"message"=>"News Feeds","data"=>$JSON));
				}else{
					echo json_encode(array("status"=>0,"message"=>"No News Feeds Found","data"=>""));
				}
			}
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}	
	}

	public function friends(){
		$userID             = $this->input->get_post('userID');
		$friendID           = $this->input->get_post('friendID');
		$date               = date('Y-m-d H:i:s');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else if($friendID == FALSE || $friendID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Friend ID is Required';
				$this->_jsonData['data']='';
			}else{
				$count = $this->Services_model->friendsCheck($userID,$friendID);
				if($count>0){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry! you have already sent request0.';
					$this->_jsonData['data']='';
				}else{
					$response = $this->Services_model->friends($userID,$friendID);
					if($response != ''){
						$notificationGetResult=$this->Services_model->notificationFriends($response);
						foreach($notificationGetResult as $dt){
							$addDataNotification = array(
							    "userID"=>$friendID,
                                "friendID"=>$userID,
                                "text"=>"".$dt['userName']." send you a friend Request","notificationType"=>"friend","isRead"=>"0",
                                "date"=>$date);
							$pushID=$friendID;								
							$pushNotification=$this->Services_model->pushNotification($pushID);
							if($pushNotification[0]['typeDevice'] == 'ios'){
								$deviceToken = $pushNotification[0]['deviceToken'];
								$passphrase = 'abcd';
								$message = $dt['userName']." send you a friend request";
								$ctx = stream_context_create();
								stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
								stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
								$fp = stream_socket_client(
									'ssl://gateway.sandbox.push.apple.com:2195', $err,
									$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
								if (!$fp)
									exit("Failed to connect: $err $errstr" . PHP_EOL);
								$body['aps'] = array(
									'alert' => $message,
									'sound' => 'default'
									);
								$payload = json_encode($body);
								$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
								$result = fwrite($fp, $msg, strlen($msg));
								fclose($fp);
							}else if($pushNotification[0]['typeDevice'] == 'android'){
								$apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
								$apiKey2 = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
								$registrationIDs = $pushNotification[0]['deviceToken'];
								$message = array(
									"message" => $dt['userName']." send you a friend request",
									"title" => "Nocturna",
									"vibrate" => 1,
									"sound" => 1,
									"smallIcon" => "small_icon"
								);
								$url = 'https://fcm.googleapis.com/fcm/send';
								$fields = array(
								   'registration_ids' =>  $registrationIDs,
								   'data' => $message
								);
								$headers = array( 
								   'Authorization: key='. $apiKey2,
								   'Content-Type: application/json'
								);
								$ch = curl_init();
								curl_setopt( $ch, CURLOPT_URL, $url );
								curl_setopt( $ch, CURLOPT_POST, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								
								curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ));
								$result = curl_exec($ch);
								curl_close($ch);
							}else{
								
							}$responseNotification=$this->Services_model->addNotification($addDataNotification);
						}
						
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Friend request has been successfully sent.';
						$this->_jsonData['data']='';
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! There an error occures, please try again.';
						$this->_jsonData['data']='';
					}
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}	
	}


	public function acceptRemoveFriend(){
		$userID = $this->input->get_post('userID');
		$friendID = $this->input->get_post('friendID');
		$type = $this->input->get_post('type');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else if($friendID == FALSE || $friendID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Friend ID is Required';
				$this->_jsonData['data']='';
			}else if($type == FALSE || $type == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Type is Required';
				$this->_jsonData['data']='';
			}else{
				if($type == 'accept'){
					$count = $this->Services_model->acceptCheck($userID,$friendID);
					if($count != ''){
						$responce=$this->Services_model->accept($count);
						if($responce == 1){
							$this->_jsonData['status']=1;
							$this->_jsonData['message']='Successfully updated';
							$this->_jsonData['data']='';
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']='Sorry! There was a problem, please try again';
							$this->_jsonData['data']='';
						}
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='No friend found';
						$this->_jsonData['data']='';
					}
				}else if($type == 'remove'){
					$count = $this->Services_model->acceptCheck($userID,$friendID);
					if($count != ''){
						$responce=$this->Services_model->remove($count);
						if($responce == 1){
							$this->_jsonData['status']=1;
							$this->_jsonData['message']='Successfully Removed';
							$this->_jsonData['data']='';
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']='Sorry! There is a problem, please try again';
							$this->_jsonData['data']='';
						}
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='No friend found';
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Type is missing, can be of Type Accept And Remove';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}	
	}

	public function getFriendList(){
		$userID = $this->input->get_post('userID');
		$ids = array();
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else{
				$getFriendsCount=$this->Services_model->getFriendsCountF($userID);
				if($getFriendsCount>0){
					$getFriends=$this->Services_model->getFriendsF($userID);
					foreach($getFriends as $getIDS){
						if($getIDS["friendID"] == $userID) {
							$ids[] = $getIDS["userID"];
						}else {
							$ids[] = $getIDS["friendID"];
						}
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='No Friends found';
					$this->_jsonData['data']='';
				}
				$comma_separated = implode(",", $ids);
				if($comma_separated != ''){
					$responce = $this->Services_model->getFriendsList($comma_separated);
					if($responce != ''){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='Get all Friends list.';
						$this->_jsonData['data']=$responce;
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']='Sorry! There is a problem.';
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry! No users found.';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}
	}


	public function findFriends(){
		$userID = $this->input->get_post('userID');
		$search = $this->input->get_post('search');
        $ids = array();
        $ids[] = $userID;
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else if($search == FALSE || $search == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Search is Required';
				$this->_jsonData['data']='';
			}else{
				$getFriendsCount=$this->Services_model->getFriendsCount($userID);
				if($getFriendsCount>0){
					$getFriends=$this->Services_model->getFriends($userID);
					foreach($getFriends as $getIDS){
						if($getIDS["friendID"] == $userID) {
							$ids[] = $getIDS["userID"];
						}else {
							$ids[] = $getIDS["friendID"];
						}
					}
				}
				$comma_separated = implode(",", $ids);
				
				$countSearch=$this->Services_model->findFriendsCount($comma_separated,$search);
				if($countSearch>0){
					$respone=$this->Services_model->findFriendsSearch($comma_separated,$search);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']='All Search Friends';
					$this->_jsonData['data']=$respone;
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='No search result found for friend';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}	
	}


	public function clubSearch(){
		$nameOfClub           = $this->input->get_post('nameOfClub');
		try{
			$count=$this->Services_model->clubsCount($nameOfClub);
			if($count>0){
				$response=$this->Services_model->clubs($nameOfClub);
				$this->_jsonData['status']=1;
				$this->_jsonData['message']='Clubs found.';
				$this->_jsonData['data']=$response;
			}else{
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Sorry! no places found.';
				$this->_jsonData['data']='';
			}
		echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}
	}

	public function nearClubs(){
		$lat = $this->input->get_post('lat');
		$long = $this->input->get_post('long');
		$radius = $this->input->get_post('radius');
		try{
			if($lat == FALSE || $lat == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Lat is Required';
				$this->_jsonData['data']='';
			}else if($long == FALSE || $long == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Long is Required';
				$this->_jsonData['data']='';
			}else{
				if($radius == FALSE || $radius == ''){
					$radius = 5;
				}
				$count = $this->Services_model->nearClubsCount($lat,$long,$radius);
				if($count>0){
					$responce = $this->Services_model->nearClubs($lat,$long,$radius);
					if($responce != ''){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='ALl near Clubs';
						$this->_jsonData['data']=$responce;
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry! No Clubs Found.';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';
		}
	}


	public function notifications(){
		$userID 			   = $this->input->get_post('userID');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';
			}else{
				$count = $this->Services_model->notificationsCount($userID);
				if($count>0){
					$responce = $this->Services_model->notifications($userID);
					if($responce != ''){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']='All Notifications';
						$this->_jsonData['data']=$responce;
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry! No Notification Found';
					$this->_jsonData['data']='';	
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';	
		}
	}

	public function normalEditProfile(){
		$userID = $this->input->get_post('userID');
		$fullName = $this->input->get_post('fullName');
		$email = $this->input->get_post('email');
		$password = $this->input->get_post('password');
		$intrest = $this->input->get_post('intrest');
		$userName = $this->input->get_post('userName');
		$dis = $this->input->get_post('dis');
		$profilePicture = isset($_FILES["profilePicture"]["name"])?$_FILES["profilePicture"]["name"]:'';
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="userID is Required";
				$this->_jsonData['data']='';
			}else{
				if($profilePicture != ""){
					$profilePicture = mt_rand().".jpg";
					$data=array(
						"id"=>$userID
					);
					$Count=$this->Services_model->normalUserPicture($data);
					if($Count>0){
						$getPicture=$this->Services_model->normalGetPicture($data);
						$profile = $getPicture[0]['profilePicture'];
						if($profile == ''){
							$data=array('profilePicture'=>$profilePicture);
							$updatePicture=$this->Services_model->normalUpdatePicture($userID,$data);
							if($updatePicture == TRUE){
								$move = move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/normal_user/profile/" . $profilePicture);
								if($move == 1){
									$response=$this->Services_model->userFullObj($userID);
									$this->_jsonData['status']=1;
									$this->_jsonData['message']="Profile Updated Successfully";
									$this->_jsonData['data']=$response;
								}else{
									$this->_jsonData['status']=0;
									$this->_jsonData['message']="Sorry! file was not moved.";
									$this->_jsonData['data']="";
								}
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="Sorry a problem occured, please try again";
								$this->_jsonData['data']='';
							}
						}else{
							$unlink = unlink(getcwd() . "/uploads/normal_user/profile/" . $profile);
							if($unlink==1){
								$data=array('profilePicture'=>$profilePicture);
								$updatePicture=$this->Services_model->normalUpdatePicture($userID,$data);
								if($updatePicture == TRUE){
									$move=move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/normal_user/profile/" . $profilePicture);									
									if($move == 1){
										$response=$this->Services_model->userFullObj($userID);
										$this->_jsonData['status']=1;
										$this->_jsonData['message']="Profile Update Successfully";
										$this->_jsonData['data']=$response;
									}else{
										$this->_jsonData['status']=0;
										$this->_jsonData['message']="Sorry! file was not move.";
										$this->_jsonData['data']="";
									}
									
								}else{
									$this->_jsonData['status']=0;
									$this->_jsonData['message']="Sorry there is some problem";
									$this->_jsonData['data']='';
								}
							}else{
								$this->_jsonData['status']=0;
								$this->_jsonData['message']="There is some problem our database";
								$this->_jsonData['data']='';
							}
						}
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="No user found";
						$this->_jsonData['data']='';
					}
				}else{
					$data = array(
						"fullName" => $fullName,
						"email" =>$email,
						"password" =>$password,
						"intrest" =>$intrest,
						"dis" =>$dis,
                        "userName" => $userName
					);
					$updatePicture=$this->Services_model->normalUpdate($userID,$data);
					if($updatePicture == TRUE){
						$responce=$this->Services_model->userFullObj($userID);
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Profile Updated Successfully";
						$this->_jsonData['data']=$responce;
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Sorry a problem occurred, please try again";
						$this->_jsonData['data']='';
					}
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';	
		}
	}
	public function packagePurchase(){
		$userID             = $this->input->get_post('userID');
		$packageID          = $this->input->get_post('packageID');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';	
			}else if($packageID == FALSE || $packageID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Package ID is Required';
				$this->_jsonData['data']='';
			}else{
				$data = array("userID"=>$userID,"packageID"=>$packageID,"type"=>"0");
				$count=$this->Services_model->packagePurchaseCount($data);
				if($count>0){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='this package is already purchased';
					$this->_jsonData['data']='';
				}else{
					$responce=$this->Services_model->packagePurchase($data);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']='Package successfully purchased';
					$this->_jsonData['data']=$responce;
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';	
		}
	}
	public function packagePurchased(){
		$userID             = $this->input->get_post('userID');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';	
			}else{
				$count=$this->Services_model->packagePurchasedCount($userID);
				if($count>0){
					$responce=$this->Services_model->packagePurchased($userID);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']='package purchased';
					$this->_jsonData['data']=$responce;
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='No package purchased';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';
		}
	}
	public function visitedPlaces(){
		$userID             = $this->input->get_post('userID');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';	
			}else{
				$count=$this->Services_model->visitedPlacesCount($userID);
				if($count>0){
					$json=array();
					$responce=$this->Services_model->visitedPlaces($userID);
					foreach($responce as $dt){
						$date = $this->time_elapsed_string($dt['date']);
						$row_array=array();
						if($dt['statusType'] == 'package'){
							$row_array['packageID']=$dt['packageID'];
							$row_array['ownerID']=$dt['ownerID'];
							$row_array['userName']=$dt['userName'];
							$row_array['nameOfClub']=$dt['nameOfClub'];
							$row_array['startTime']=$dt['startTime'];
							$row_array['date']=$date;
							$row_array['endTime']=$dt['endTime'];
							$row_array['clubDis']=$dt['clubDis'];
							$row_array['phoneNumber']=$dt['phoneNumber'];
							$row_array['address']=$dt['address'];
							$row_array['email']=$dt['email'];
							$row_array['deviceToken']=$dt['deviceToken'];
							$row_array['clubType']=$dt['clubType'];
							$row_array['typeDevice']=$dt['typeDevice'];							
							$row_array['profile']=$dt['profile'];
							$row_array['cover']=$dt['cover'];
							$row_array['lat']=$dt['lat'];
							$row_array['long']=$dt['long'];							
							$row_array['statusType']=$dt['statusType'];
						}else if($dt['statusType'] == 'checkin'){
							$row_array['id']=$dt['packageID'];
							$row_array['userID']=$dt['ownerID'];
							$row_array['userName']=$dt['userName'];
							$row_array['status']=$dt['nameOfClub'];
							$row_array['checkin']=$dt['startTime'];
							$row_array['date']=$date;							
							$row_array['profile']=$dt['profile'];					
							$row_array['statusType']=$dt['statusType'];
						}else{}
						array_push($json,$row_array);
					}
					echo json_encode(array("status"=>1,"message"=>"visited places","data"=>$json));
				}else{
					echo json_encode(array("status"=>0,"message"=>"No Visited Places","data"=>""));
				}
			}
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';
		}
	}
	public function updateTokenDevice(){
		$userID             = $this->input->get_post('userID');
		$deviceToken        = $this->input->get_post('deviceToken');
		$typeDevice         = $this->input->get_post('typeDevice');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='User ID is Required';
				$this->_jsonData['data']='';	
			}else if($deviceToken == FALSE || $deviceToken == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Device Token is Required';
				$this->_jsonData['data']='';	
			}else if($typeDevice == FALSE || $typeDevice == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='Device Type is Required';
				$this->_jsonData['data']='';	
			}else{
				$data=array("deviceToken"=>$deviceToken,"typeDevice"=>$typeDevice);
				$count=$this->Services_model->updateTokenDeviceCount($userID,$data);
				if($count>0){
					$responce=$this->Services_model->updateTokenDevice($userID,$data);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']='Device Token and Device Type has been updated';
					$this->_jsonData['data']=$responce;
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']='Sorry Device Token and Device Type cannot be updated';
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);	
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occurred';
			$this->_jsonData['data']='';
		}
	}
	public function clubsPackages(){
		$ownerID             = $this->input->get_post('ownerID');
		try{
			if($ownerID == FALSE || $ownerID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']='owner ID is Required';
				$this->_jsonData['data']='';	
			}else{
				$count=$this->Services_model->clubsPackagesCount($ownerID);
				if($count>0){
					$responce=$this->Services_model->clubsPackages($ownerID);
					$json=array();
					foreach($responce as $res){
						$percentage=intval($res['discount']);
						$perAmount = ($percentage / 100) * $res['price'];
						$netAmount=$res['price']-$perAmount;
						
						$rowArray['id']=$res['id'];
						$rowArray['ownerID']=$res['ownerID'];
						$rowArray['nameOfClub']=$res['nameOfClub'];
						$rowArray['price']=$res['price'];
						$rowArray['description_attributes']=$res['description_attributes'];
						$rowArray['validity']=$res['validity'];
						$rowArray['discount']=$res['discount'];
						$rowArray['actualAmount']=$netAmount;
						$rowArray['userName']=$res['userName'];
						$rowArray['clubType']=$res['clubType'];
						$rowArray['image']=$res['image'];
						array_push($json,$rowArray);
					}
					echo json_encode(array("status"=>1,"message"=>"club packages","data"=>$json));
				}else{
					echo json_encode(array("status"=>0,"message"=>"No Clubs packages","data"=>""));
				}
			}
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']='Error Occured';
			$this->_jsonData['data']='';
		}
	}
	public function forgetPassword(){
		$email=$this->input->get_post('email');
		try{
			if($email == FALSE || $email == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="email is Missing";
			}else{
				$checkUser=array("email"=>$email);
				$responceUser = $this->Services_model->CheckUserForget($checkUser);
				if($responceUser > 0){
					$getResponce = $this->Services_model->getUser($checkUser);
					$toName = $getResponce[0]['email'];
					$message = "Hi, ".$getResponce[0]['fullName']." 
					We need to make sure you are human. Please Check your Password and get started using your Profile account on Nocturna.				
					Your ID: ".$getResponce[0]['id']."								
					And your Password is this for login your profile. Password: ".$getResponce[0]['password']." And Email is this. Email ".$getResponce[0]['email']."";
					
					$this->email->from('Mailer@nocturna.com', 'Nocturna Mailer');
					$this->email->to($toName);
					$this->email->subject("Confirmation from Nocturna to ".$getResponce[0]['fullName']."");
					$this->email->message($message);
					$mailSent=$this->email->send();
					if($mailSent != 1){
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Sorry There is some problem.";
						$this->_jsonData['data']='';
					}else{
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="you can check your email.";
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="User is not exist";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function feedBack(){
		$userID		  = $this->input->get_post('userID');
		$feedBack		= $this->input->get_post('feedBack');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="userID is Missing";
			}else if($feedBack == FALSE || $feedBack == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="feedBack is Missing";
			}else{
				$data=array("userID"=>$userID,"feedback"=>$feedBack);
				$responce = $this->Services_model->feedBack($data);
				if($responce != ''){
					$this->_jsonData['status']=1;
					$this->_jsonData['message']="Feed Back is insert successfully.";
					$this->_jsonData['data']='';
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="User is not exist";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function addMyCart(){
		error_reporting(0);
		include('phpqrcode/qrlib.php');
		$userID		  = $this->input->get_post('userID');
		$packageID	   = $this->input->get_post('packageID');
		$type	        = $this->input->get_post('type'); //friend & split
		$friendID        = $this->input->get_post('friendID');
		$price	       = $this->input->get_post('price');
		$date            = date('Y-m-d H:i:s');
		$quantity        = $this->input->get_post('quantity');
		
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="UserID is Required";
				$this->_jsonData['data']='';
			}else if($packageID == FALSE || $packageID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="PackageID is Required";
				$this->_jsonData['data']='';
			}else{
				if($type == 'friend'){
					error_reporting(0);
					for($i=1;$i<=$quantity;$i++){
					$QRname          = time()."_".$i.'.png';
					$getDetailsForQR = $this->Services_model->getDetailsForQR($packageID);
					QRcode::png("Package ID: ".$packageID. " || Club Name: ".$getDetailsForQR[0]['ownerName'] ." || Package Name: ".$getDetailsForQR[0]['packageName'], 'uploads/qr/'.$QRname);
					$dataFriend=array(
						"userID"	=> $friendID,
						"packageID" => $packageID,
						"status"	=> "purchased",
						"qr"		=> $QRname,
						"type"	  => $type
					);
					$responce=$this->Services_model->purchasedFriend($dataFriend);
					}
					
					
					if($responce != ''){
						$responceNotificationPackage=$this->Services_model->packageNotification($userID,$friendID);
						foreach($responceNotificationPackage as $dt){
							$data=array(
								"userID"		  	  => $friendID,
								"friendID"			=> $userID,
								"text"				=> $dt['fullName']."! Buy a package for you.",
								"notificationType"	=> "packagePurchased",
								"isRead"		  	  => "0",
								"date"				=> $date
							);
							$responceNotification=$this->Services_model->addNotification($data);
							if($dt['friendTypeDevice'] == 'ios'){
								$deviceToken = $dt['friendDeviceToken'];
								$passphrase = 'abcd';
								$message = $dt['fullName']."! Buy a package for you.";
								$ctx = stream_context_create();
								stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
								stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
								$fp = stream_socket_client(
									'ssl://gateway.sandbox.push.apple.com:2195', $err,
									$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
								if (!$fp)
									exit("Failed to connect: $err $errstr" . PHP_EOL);
								echo 'Connected to APNS' . PHP_EOL;
								$body['aps'] = array(
									'alert' => $message,
									'sound' => 'default'
									);
								$payload = json_encode($body);
								$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
								$result = fwrite($fp, $msg, strlen($msg));
								
								if (!$result)
									echo 'Message not delivered' . PHP_EOL;
								else
									echo 'Message successfully delivered' . PHP_EOL;
								fclose($fp);
							}else if($dt['friendTypeDevice'] == 'android'){
								$apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
								$registrationIDs = $dt['friendDeviceToken'];
								$message = array(
									"message"      		  => $dt['fullName']."! Buy a package for you.",
									"title"  				=> "Nocturna",
									"vibrate" 	  		  => 1,
									"sound"  				=> 1,
									"smallIcon" 			=> "small_icon"
								);
								$url = 'https://fcm.googleapis.com/fcm/send';
								$fields = array(
								   'registration_ids'      =>  $registrationIDs,
								   'data'				  =>  $message 
								);
								$headers = array( 
								   'Authorization: key='. $apiKey,
								   'Content-Type: application/json'
								);
								$ch = curl_init();
								curl_setopt( $ch, CURLOPT_URL, $url );
								curl_setopt( $ch, CURLOPT_POST, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								
								curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
								$result = curl_exec($ch);
								curl_close($ch);
							}else{
							}
						}
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Friend Package Added Successfully";
						$this->_jsonData['data']='';
					}
				}else if($type == 'split'){
					$check = $this->Services_model->checkCart($userID,$packageID);
					if($check>0){
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Package is Already Added";
						$this->_jsonData['data']='';
					}else{
						$responce = $this->Services_model->addMyCart($userID,$packageID,$type);
						if($responce != ''){
							$arr=array();
							$arr=explode(",", $friendID);
							foreach($arr as $val)
							{
								$this->Services_model->addMyCartSplit($val,$responce,$price);
							}
							$this->_jsonData['status']=1;
							$this->_jsonData['message']="Package Add Successfully";
							$this->_jsonData['data']='';
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']="Sorry There is some problem";
							$this->_jsonData['data']='';
						}
					}				
				}else{
					$check = $this->Services_model->checkCart($userID,$packageID);
					if($check>0){
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Package is Already Added";
						$this->_jsonData['data']='';
					}else{
						for($i=1;$i<=$quantity;$i++){
							$responce = $this->Services_model->addMyCart($userID,$packageID);
						}
						if($responce != ''){
							$this->_jsonData['status']=1;
							$this->_jsonData['message']="Package Add Successfully";
							$this->_jsonData['data']='';
						}else{
							$this->_jsonData['status']=0;
							$this->_jsonData['message']="Sorry There is some problem";
							$this->_jsonData['data']='';
						}
					}
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function splitData(){
		$userID		  = $this->input->get_post('userID');
		try{
			$resCount=$this->Services_model->splitDataCount($userID);
			if($resCount>0){
				$responce=$this->Services_model->splitData($userID);				
				$this->_jsonData['status']=1;
				$this->_jsonData['message']="split data";
				$this->_jsonData['data']=$responce;
			}else{
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="No split data found";
				$this->_jsonData['data']='';
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function splitApprove(){
		$splitID		   = $this->input->get_post('splitID');
		try{
			$resCount=$this->Services_model->splitApproveCount($splitID);
			if($resCount>0){
				$responce=$this->Services_model->splitApprove($splitID);
				if($responce==TRUE){
					$this->_jsonData['status']=1;
					$this->_jsonData['message']="your amount add successfully";
					$this->_jsonData['data']='';
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="Sorry there is some problem.";
					$this->_jsonData['data']='';
				}
			}else{
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="already approve";
				$this->_jsonData['data']='';
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function CartPackage(){
		$userID		  = $this->input->get_post('userID');
		try{
			if($userID == FALSE || $userID == ''){
				echo json_encode(array("status"=>0,"message"=>"UserID is Required","data"=>""));
			}else{
				$checkUser = $this->Services_model->checkUser($userID);
				if($checkUser>0){
					$responcePackageCount = $this->Services_model->myCartCount($userID);
					if($responcePackageCount>0){
						$responcePackage = $this->Services_model->myCart($userID);
						$json=array();
						
						foreach($responcePackage as $res){
							$percentage=intval($res['discount']);
							$perAmount = ($percentage / 100) * $res['price'];
							$netAmount=$res['price']-$perAmount;
							$rowArray=array();
							if($res['type']=='split'){
								$splitPrice = $this->Services_model->getSplitPrice($res['cartID']);
								$splitPrice2 = $splitPrice[0]['price'];
								$rowArray['packageID']=$res['packageID'];
								$rowArray['cartID']=$res['cartID'];
								$rowArray['ownerID']=$res['ownerID'];
								$rowArray['nameOfClub']=$res['nameOfClub'];
								$rowArray['price']=$res['price'];
								$rowArray['description_attributes']=$res['description_attributes'];
								$rowArray['validity']=$res['validity'];
								$rowArray['discount']=$res['discount'];
								$rowArray['actualAmount']=$splitPrice2;
								$rowArray['userName']=$res['userName'];
								$rowArray['clubType']=$res['clubType'];
								$rowArray['image']=$res['image'];
								$rowArray['type']=$res['type'];
							}else{
								$rowArray['packageID']=$res['packageID'];
								$rowArray['cartID']=$res['cartID'];
								$rowArray['ownerID']=$res['ownerID'];
								$rowArray['nameOfClub']=$res['nameOfClub'];
								$rowArray['price']=$res['price'];
								$rowArray['description_attributes']=$res['description_attributes'];
								$rowArray['validity']=$res['validity'];
								$rowArray['discount']=$res['discount'];
								$rowArray['actualAmount']=$netAmount;
								$rowArray['userName']=$res['userName'];
								$rowArray['clubType']=$res['clubType'];
								$rowArray['image']=$res['image'];
								$rowArray['type']=$res['type'];
							}
							array_push($json,$rowArray);
						}
						echo json_encode(array("status"=>1,"message"=>"club packages","data"=>$json));
					}else{
						echo json_encode(array("status"=>0,"message"=>"No package add","data"=>""));
					}
				}else{
					echo json_encode(array("status"=>0,"message"=>"No package add","data"=>""));
				}
			}
		}catch(Exception $e){
			echo json_encode(array("status"=>0,"message"=>"Error Occured","data"=>""));
		}
	}
	public function cartPackageRemove(){
		$userID		  = $this->input->get_post('userID');
		$packageID	   = $this->input->get_post('packageID');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="UserID is Required";
				$this->_jsonData['data']='';
			}else if($packageID == FALSE || $packageID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="PackageID is Required";
				$this->_jsonData['data']='';
			}else{
				$check = $this->Services_model->checkCart($userID,$packageID);
				if($check>0){
					$responce = $this->Services_model->cartPackageRemove($userID,$packageID);
					if($responce == 1){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Package Delete Successfully";
						$this->_jsonData['data']='';
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Sorry There is some problem";
						$this->_jsonData['data']='';
					}
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="No data Found";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}	
	}
	public function getClubTypes(){
		try{
			$countClubType = $this->Services_model->getClubTypesCount();
			if($countClubType>0){
				$responce = $this->Services_model->getClubTypes();
				$this->_jsonData['status']=1;
				$this->_jsonData['message']="All Types";
				$this->_jsonData['data']=$responce;
			}else{
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="No clubTypes Found";
				$this->_jsonData['data']='';
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function addMyCartPurchased(){
		include('phpqrcode/qrlib.php');
		$userID		  = $this->input->get_post('userID');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="UserID is Required";
				$this->_jsonData['data']='';
			}else{
					$responceCount = $this->Services_model->addMyCartPurchasedCounts($userID);
					foreach($responceCount as $dt){
						$QRname          = time()."_".$dt['id'].'.png';		
						$id=$dt['id'];
						$getDetailsForQR = $this->Services_model->getDetailsForQR($dt['packageID']);
						QRcode::png("Package ID: ".$dt['packageID']. " || Club Name: ".$getDetailsForQR[0]['ownerName'] ." || Package Name: ".$getDetailsForQR[0]['packageName'], 'uploads/qr/'.$QRname);
						$responce = $this->Services_model->addMyCartPurchased($id,$QRname);
					}
					if($responce != ''){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="Package purchased Add Successfully";
						$this->_jsonData['data']='';
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="Sorry There is some problem";
						$this->_jsonData['data']='';
					}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function CartPackagePurchased(){
		$userID		  = $this->input->get_post('userID');
		try{
			if($userID == FALSE || $userID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="UserID is Required";
				$this->_jsonData['data']='';
			}else{
				$checkUser = $this->Services_model->checkUserPurchased($userID);
				if($checkUser>0){
					$responcePackage = $this->Services_model->myCartPurchased($userID);				
					foreach($responcePackage as $dt){
						$idsPackages[] = $dt['packageID'];
					}
					$commaValues = implode(",",$idsPackages);
					$responce = $this->Services_model->allPackagesCartPurchased($commaValues,$userID);
					$this->_jsonData['status']=1;
					$this->_jsonData['message']="Packages";
					$this->_jsonData['data']=$responce;
				}else{
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="No Packages Added";
					$this->_jsonData['data']='';
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	}
	public function availPackage(){
		$availID		  = $this->input->get_post('availID');
		try{
			if($availID == FALSE || $availID == ''){
				$this->_jsonData['status']=0;
				$this->_jsonData['message']="availID is required";
				$this->_jsonData['data']='';
			}else{
				$responceCount=$this->Services_model->availPackageCount($availID);
				if($responceCount>0){
					$this->_jsonData['status']=0;
					$this->_jsonData['message']="This package is already avail";
					$this->_jsonData['data']='';
				}else{
					$responce=$this->Services_model->availPackage($availID);
					if($responce == true){
						$this->_jsonData['status']=1;
						$this->_jsonData['message']="package avail successfully";
						$this->_jsonData['data']='';
					}else{
						$this->_jsonData['status']=0;
						$this->_jsonData['message']="there is some problem, pacakge is not avail";
						$this->_jsonData['data']='';
					}
				}
			}
			echo json_encode($this->_jsonData);
		}catch(Exception $e){
			$this->_jsonData['status']=0;
			$this->_jsonData['message']="Error Occured";
			$this->_jsonData['data']='';
		}
	} 
	//************************** FUNCTIONS IMAGE AND TIME CONVERT ***************************//
	public function upload($picture,$type){
		$date = new DateTime();
		$randomNumber = $date->getTimestamp();				
		$this->config =  array(
		  'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/",
		  'upload_url'      => base_url()."uploads/",
		  'allowed_types'   => "jpg|png|jpeg",
		  'file_name'       => $type."_".$randomNumber,
		  'max_size'        => "1000KB",
		  'max_height'      => "768",
		  'max_width'       => "1024"  
		);
		$this->load->library('upload', $this->config);
		if($this->upload->do_upload()){
			return $responce = "file upload success";
		}else{
		   return $responce = "file upload failed";
		}
	}
	public function time_elapsed_string($datetime, $full = false){
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);
	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;
	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v){
	        if($diff->$k){
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        }else{
	            unset($string[$k]);
	        }
	    }
	    if(!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

	public function pushAndroid(){
		$apiKey = "AIzaSyCL2c9o_td5sxzxetFRZjsRXIu9YeZoIe8";
		$registrationIDs = array("eZFsTDtkb5g:APA91bHUsll9zM8ieYGUztnJ5tRGt-jIvgVkmNG3o8rsK6Rb6m1Q0Xhwc8H36mYyDkjyGnIdF_wSZlhc4heHzv1i");
		$message = array(
			"message"      		  => "hello! nocturna",
			"title"  				=> "Nocturna",
			"vibrate" 	  		  => 1,
			"sound"  				=> 1,
			"smallIcon" 			=> "small_icon"
		);
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
		   'registration_ids'      =>  $registrationIDs,
		   'data'				  =>  $message 
		);
		$headers = array( 
		   'Authorization: key='. $apiKey,
		   'Content-Type: application/json'
		);
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch);
		curl_close($ch);
		
		echo $result;
		$this->_jsonData['status']="SUCCESS";
		$this->_jsonData['message']="Data retrieved successfully";
		$this->_jsonData['data']="";				
	}
	public function iphonePush(){
		$deviceToken = 'c88ef0c4c6aa0fa063b65a4c18f0c30873fb8ede61c545fbde2ad5680ee38c4f';
		$passphrase = '12345';
		$message = 'Nocturna Test';
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		echo 'Connected to APNS' . PHP_EOL;
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
			);
		$payload = json_encode($body);
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		$result = fwrite($fp, $msg, strlen($msg));
		
		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered' . PHP_EOL;
		fclose($fp);
	}
	function checkEmail(){
		$this->email->from('your@example.com', 'Your Name');
		$this->email->to('mustufa.developer@hotmail.com');
		
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		
		$send = $this->email->send();
		
		if($send){
			echo "mail function is working.";
		}
			
	}
	function coreMail(){
		$to      = 'mustufa.enexus@gmail.com';
		$subject = 'the subject';
		$message = 'hello';
		$headers = 'From: webmaster@example.com' . "\r\n" .
			'Reply-To: webmaster@example.com' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		
		mail($to, $subject, $message, $headers);
	}
}
?>