<?php
/**
 * Created by PhpStorm.
 * User: lp-eniola
 * Date: 9/12/18
 * Time: 5:47 AM
 */

require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Cart extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_cart');
        $this->load->helper('url');
    }

    public function index_get()
    {
        $this->response("Cart controller is set up");
    }

    public function addMyCart_post()
    {
        error_reporting(0);
        include('phpqrcode/qrlib.php');
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['packageID'])){
                $response = [
                    "status" => 0,
                    "message" => "Package ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['type'])){
                $response = [
                    "status" => 0,
                    "message" => "Type is required",
                    "data" => ''
                ];
            } elseif(!isset($data['friendID'])){
                $response = [
                    "status" => 0,
                    "message" => "Friend ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['quantity'])){
                $response = [
                    "status" => 0,
                    "message" => "Quantity is required",
                    "data" => ''
                ];
            }elseif(!isset($data['price'])){
                $response = [
                    "status" => 0,
                    "message" => "Price is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $packageID = $data['packageID'];
                $type = $data['type']; //friend & split
                $friendID = $data['friendID'];
                $price = $data['price'];
                $date = date('Y-m-d H:i:s');
                $quantity = $data['quantity'];

                if($type == 'friend'){
                    error_reporting(0);
                    for($i=1; $i <= $quantity;$i++){
                        $QRname = time()."_".$i.'.png';
                        $getDetailsForQR = $this->Model_cart->getDetailsForQR($packageID);
                        QRcode::png("Package ID: ".$packageID. " || Club Name: ".$getDetailsForQR[0]['ownerName'] ." || Package Name: ".$getDetailsForQR[0]['packageName'], 'uploads/qr/'.$QRname);
                        $dataFriend = array(
                            "userID" => $friendID,
                            "packageID" => $packageID,
                            "status" => "pending",
                            "qr" => $QRname,
                            "type" => $type
                        );
                        $result = $this->Model_cart->purchasedFriend($dataFriend);
                    }
                    if($result != ''){
                        $responceNotificationPackage = $this->Model_cart->packageNotification($userID, $friendID);
                        foreach($responceNotificationPackage as $dt){
                            //todo: check out why this is, user_id replaced for friend id
                            $response_data = array(
                                "userID" => $friendID,
                                "friendID" => $userID,
                                "text" => $dt['fullName']."! Buy a package for you.",
                                "notificationType" => "packagePurchased",
                                "isRead" => "0",
                                "date" => $date
                            );
                            $responseNotification = $this->Model_cart->addNotification($response_data);
                            if($dt['friendTypeDevice'] == 'ios'){
                                $deviceToken = $dt['friendDeviceToken'];
                                $passphrase = 'abcd';
                                $message = $dt['fullName']."! Buy a package for you.";
                                $ctx = stream_context_create();
                                stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
                                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                                $fp = stream_socket_client(
                                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                                if (!$fp)
                                    exit("Failed to connect: $err $errstr" . PHP_EOL);
                                echo 'Connected to APNS' . PHP_EOL;
                                $body['aps'] = array(
                                    'alert' => $message,
                                    'sound' => 'default'
                                );
                                $payload = json_encode($body);
                                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                                $result = fwrite($fp, $msg, strlen($msg));

                                if (!$result)
                                    echo 'Message not delivered' . PHP_EOL;
                                else
                                    echo 'Message successfully delivered' . PHP_EOL;
                                fclose($fp);
                            }else if($dt['friendTypeDevice'] == 'android'){
                                $apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
                                $registrationIDs = $dt['friendDeviceToken'];
                                $message = array(
                                    "message" => $dt['fullName']."! Buy a package for you.",
                                    "title" => "Nocturna",
                                    "vibrate" => 1,
                                    "sound" => 1,
                                    "smallIcon" => "small_icon"
                                );
                                $url = 'https://fcm.googleapis.com/fcm/send';
                                $fields = array(
                                    'registration_ids' => $registrationIDs,
                                    'data' => $message
                                );
                                $headers = array(
                                    'Authorization: key='. $apiKey,
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt( $ch, CURLOPT_URL, $url );
                                curl_setopt( $ch, CURLOPT_POST, true );
                                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

                                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch);
                                curl_close($ch);
                            } else{

                            }
                        }
                        $response = [
                            "status" => 1,
                            "message" => "Friend Package Added Successfully",
                            "data" => ''
                        ];
                    }
                }else if($type == 'split'){
                    $check = $this->Model_cart->checkCart($userID, $packageID);
                    if($check>0){
                        $response = [
                            "status" => 0,
                            "message" => "Package is Already Added",
                            "data" => ''
                        ];
                    }else{
                        $result = $this->Model_cart->addMyCart($userID, $packageID, $type);

                        //insert another copy of the same data set into cart_split_backup
                        $result2 = $this->Model_cart->addMyCartBackup($userID, $packageID, $type);

                        if($result != ''){
                            $arr = array();
                            $arr = explode(",", $friendID);
                            foreach($arr as $val)
                            {
                                $this->Model_cart->addMyCartSplit($val,$result,$price);
                            }
                            $response = [
                                "status" => 1,
                                "message" => "Package Added Successfully",
                                "data" => ''
                            ];
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry There was a problem",
                                "data" => ''
                            ];
                        }
                    }
                }else{
                    $check = $this->Model_cart->checkCart($userID,$packageID);
                    if($check > 0){
                        $response = [
                            "status" => 0,
                            "message" => "Package is Already Added",
                            "data" => ''
                        ];
                    }else{
                        for($i=1; $i<=$quantity; $i++){
                            $result = $this->Model_cart->addMyCart($userID,$packageID, $type);
                        }
                        if($result != ''){
                            $response = [
                                "status" => 1,
                                "message" => "Package Added Successfully",
                                "data" => ''
                            ];
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry There is some problem",
                                "data" => ''
                            ];
                        }
                    }
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }


    public function splitApprove_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['splitID'])){
                $response = [
                    "status" => 0,
                    "message" => "Split ID is required",
                    "data" => ''
                ];
                $this->response($response);
            } else {
                $splitID = $data['splitID'];

                $resCount = $this->Model_cart->splitApproveCount($splitID);
                if($resCount > 0){
                    $result = $this->Model_cart->splitApprove($splitID);
                    if($result == TRUE){
                        $response = [
                            "status" => 1,
                            "message" => "Your amount is added successfully",
                            "data" => ''
                        ];

                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry there is a problem",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Already Approved",
                        "data" => ''
                    ];

                }
            }

            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "An error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function getMyCart_post()
    {
        $data = $this->post();
        try{

            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
                $this->response($response);
            } else{
                $userID = $data['userID'];
                $checkUser = $this->Model_cart->checkUser($userID);
                if($checkUser > 0){
                    $responsePackageCount = $this->Model_cart->myCartCount($userID);
                    if($responsePackageCount > 0){
                        $responsePackage = $this->Model_cart->myCart($userID);
                        $json = array();

                        foreach($responsePackage as $res){
                            $percentage = intval($res['discount']);
                            $perAmount = ($percentage / 100) * $res['price'];
                            $netAmount = $res['price']-$perAmount;
                            $rowArray = array();
                            if($res['type'] == 'split'){
                                $splitPrice = $this->Model_cart->getSplitPrice($res['cartID']);
                                $splitPrice2 = $splitPrice[0]['price'];
                                $rowArray['packageID'] = $res['packageID'];
                                $rowArray['cartID'] = $res['cartID'];
                                $rowArray['ownerID'] = $res['ownerID'];
                                $rowArray['nameOfClub'] = $res['nameOfClub'];
                                $rowArray['price'] = $res['price'];
                                $rowArray['description_attributes'] = $res['description_attributes'];
                                $rowArray['validity'] = $res['validity'];
                                $rowArray['discount'] = $res['discount'];
                                $rowArray['actualAmount'] = $splitPrice2;
                                $rowArray['userName'] = $res['userName'];
                                $rowArray['clubType'] = $res['clubType'];
                                $rowArray['image'] = $res['image'];
                                $rowArray['type'] = $res['type'];
                                $rowArray['owner_club'] = $res['owner_club'];
                            }else{
                                $rowArray['packageID'] = $res['packageID'];
                                $rowArray['cartID'] = $res['cartID'];
                                $rowArray['ownerID'] = $res['ownerID'];
                                $rowArray['nameOfClub'] = $res['nameOfClub'];
                                $rowArray['price'] = $res['price'];
                                $rowArray['description_attributes'] = $res['description_attributes'];
                                $rowArray['validity'] = $res['validity'];
                                $rowArray['discount'] = $res['discount'];
                                $rowArray['actualAmount'] = $netAmount;
                                $rowArray['userName'] = $res['userName'];
                                $rowArray['clubType'] = $res['clubType'];
                                $rowArray['image'] = $res['image'];
                                $rowArray['type'] = $res['type'];
                                $rowArray['owner_club'] = $res['owner_club'];
                            }
                            array_push($json,$rowArray);
                        }
                        $response = [
                            "status" => 1,
                            "message" => "Club Packages",
                            "data" => $json
                        ];
                        $this->response($response);
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "No package added",
                            "data" => ''
                        ];
                        $this->response($response);
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No package added",
                        "data" => ''
                    ];
                   $this->response($response);
                }
            }
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "An Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function cartPackageRemove_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['packageID'])){
                $response = [
                    "status" => 0,
                    "message" => "Package ID is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $packageID = $data['packageID'];
                $check = $this->Model_cart->checkCart($userID, $packageID);
                if($check > 0){
                    $result = $this->Model_cart->cartPackageRemove($userID,$packageID);
                    if($result == 1){
                        $response = [
                            "status" => 1,
                            "message" => "Package Deleted Successfully",
                            "data" => ''
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Package deletion not successful",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No data Found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => []
            ];
            $this->response($response);
        }
    }

    public function availablePackage_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['availID'])){
                $response = [
                    "status" => 0,
                    "message" => "availID is required",
                    "data" => ''
                ];
            } else{
                $availID = $data['availID'];
                $responseCount = $this->Model_cart->availPackageCount($availID);
                if($responseCount > 0){
                    $response = [
                        "status" => 0,
                        "message" => "This package is already avail",
                        "data" => ''
                    ];
                }else{
                    $result = $this->Model_cart->availPackage($availID);
                    if($result == true){
                        $response = [
                            "status" => 1,
                            "message" => "Package avail successfully",
                            "data" => ''
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "there is some problem, package is not available",
                            "data" => ''
                        ];
                    }
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function purchaseCart_post()
    {
        include('phpqrcode/qrlib.php');
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } else{
                $userID	= $data['userID'];
                $responseCount = $this->Model_cart->addMyCartPurchasedCounts($userID);
                foreach($responseCount as $dt){
                    $QRname = time()."_".$dt['id'].'.png';
                    $id = $dt['id'];
                    $getDetailsForQR = $this->Model_cart->getDetailsForQR($dt['packageID']);
                    QRcode::png("Package ID: ".$dt['packageID']. " || Club Name: ".$getDetailsForQR[0]['ownerName'] ." || Package Name: ".$getDetailsForQR[0]['packageName'],
                        'uploads/qr/'.$QRname);
                    $result = $this->Model_cart->addMyCartPurchased($id, $QRname);
                }
                if($result != ''){
                    $response = [
                        "status" => 1,
                        "message" => "Package purchased Added Successfully",
                        "data" => ''
                    ];
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Package purchase failed",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }
}