<?php

require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Post extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Model_post');
        $this->load->helper('url');
    }

    public function index_get()
    {
        $this->response("Welcome, this is the post controller");
    }


    public function getStatus_post()
    {
        $data = $this->post();
        $date = date('Y-m-d H:i:s');

        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['checkin'])){
                $response = [
                    "status" => 0,
                    "message" => "Checkin is required",
                    "data" => ''
                ];
            } elseif(!isset($data['status'])){
                $response = [
                    "status" => 0,
                    "message" => "Status is required",
                    "data" => ''
                ];
            }elseif(!isset($data['statusType'])){
                $response = [
                    "status" => 0,
                    "message" => "Status Type is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $picture = isset($_FILES["picture"]["name"])?$_FILES["picture"]["name"]:'';
                $statusType = $data['statusType'];
                $status = $data['status'];
                $checkin = $data['checkin'];

                if(isset($data['video'])){
                    $video = $data['video'];
                }

                if($statusType === 'picture'){
                    $picture = mt_rand().".jpg";
                    $move = move_uploaded_file($_FILES["picture"]["tmp_name"], getcwd() . "/uploads/pictureStatus/" . $picture);
                    if($move == 1){
                        $dataInsert = [
                            "userID" => $userID,
                            "picture" => $picture,
                            "status" => $status,
                            "statusType" => $statusType,
                            "date" => $date
                        ];
                        $response_data=$this->Model_post->status($dataInsert);
                        if($response_data != ''){
                            $response = [
                                "status" => 1,
                                "message" => "Status Uploaded Successfully",
                                "data" => $response_data
                            ];
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry an error occurred",
                                "data" => ''
                            ];
                        }
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry! File was not moved",
                            "data" => ''
                        ];
                    }
                }else if($statusType === 'checkin'){
                    $dataInsert = [
                        "userID" => $userID,
                        "checkIn" => $checkin,
                        "status" => $status,
                        "statusType" => $statusType,
                        "date" => $date
                    ];
                    $response_data=$this->Model_post->status($dataInsert);
                    if($response_data != ''){
                        $response = [
                            "status" => 1,
                            "message" => "Status Upload Successful",
                            "data" => $response_data
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry!  a problem occurred",
                            "data" => ''
                        ];
                    }
                }else if($statusType === 'video'){
                    $video = mt_rand().".mp4";
                    $move = move_uploaded_file($_FILES["video"]["tmp_name"], getcwd() . "/uploads/videoStatus/".$video);
                    if($move == 1){
                        $dataInsert = [
                            "userID" => $userID,
                            "status" => $status,
                            "video" => $video,
                            "statusType" => $statusType,
                            "date" => $date
                        ];

                        $response_data = $this->Model_post->status($dataInsert);
                        if($response_data != ''){
                            $response = [
                                "status" => 1,
                                "message" => "Video Uploaded Successfully",
                                "data" => $response_data
                            ];
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry a problem occurred",
                                "data" => ''
                            ];
                        }
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry! File was not moved",
                            "data" => ''
                        ];
                    }
                }
                else{
                    $response = [
                        "status" => 0,
                        "message" => "Status Type is not defined",
                        "data" => ''
                    ];
                }
            }

            $this->response($response);

        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function newsFeed_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required as userID",
                    "data" => ''
                ];
                $this->response($response);
            } else{
                $userID = $data['userID'];
                $ids = array();
                $ids[] = $userID;
                $JSON = array();
                $getFriendsCount = $this->Model_post->getFriendsCount($userID);
                if($getFriendsCount > 0){
                    $getFriends=$this->Model_post->getFriends($userID);
                    foreach($getFriends as $getIDS){
                        if($getIDS["friendID"] == $userID) {
                            $ids[] = $getIDS["userID"];
                        }
                        else {
                            $ids[] = $getIDS["friendID"];
                        }
                    }
                } else{

                }
                $comma_separated = implode(",", $ids);

                $count=$this->Model_post->newsFeedsCheck($comma_separated, $userID);
                if( $count > 0){
                    $feed_response = $this->Model_post->newsFeeds($comma_separated, $userID);
                    foreach($feed_response as $response_data){
                        $date = $this->time_elapsed_string($response_data['date']);
                        $rowArray = array();
                        if($response_data['statusType'] === 'picture'){
                            $rowArray['picture'] = $response_data['picture'];
                            $rowArray['status'] = $response_data['status'];
                            $rowArray['comments'] = $response_data['comments'];
                            $rowArray['likes'] = $response_data['likes'];
                            $rowArray['isLike']=$response_data['isLike'];
                            $rowArray['userName']=$response_data['userName'];
                            $rowArray['userPicture']=$response_data['userPicture'];
                        }else if($response_data['statusType'] === 'checkin'){
                            $rowArray['status'] = $response_data['status'];
                            $rowArray['checkin'] = $response_data['checkin'];
                            $rowArray['comments'] = $response_data['comments'];
                            $rowArray['likes'] = $response_data['likes'];
                            $rowArray['isLike'] = $response_data['isLike'];
                            $rowArray['userName'] = $response_data['userName'];
                            $rowArray['userPicture'] = $response_data['userPicture'];
                        }else if($response_data['statusType'] === 'video'){
                            $rowArray['status'] = $response_data['status'];
                            $rowArray['video'] = $response_data['video'];
                            $rowArray['comments'] = $response_data['comments'];
                            $rowArray['likes'] = $response_data['likes'];
                            $rowArray['isLike'] = $response_data['isLike'];
                            $rowArray['userName'] = $response_data['userName'];
                            $rowArray['userPicture'] = $response_data['userPicture'];
                        }else if($response_data['statusType'] === 'package'){
                            $rowArray['picture'] = $response_data['picture'];
                            $rowArray['description_attributes'] = $response_data['status'];
                            $rowArray['packageName'] = $response_data['checkin'];
                            $rowArray['discount'] = $response_data['video'];
                            $rowArray['statusType'] = $response_data['statusType'];
                            $rowArray['userName'] = $response_data['userName'];
                            $rowArray['userPicture'] = $response_data['userPicture'];
                        }

                        $rowArray['id'] = $response_data['id'];
                        $rowArray['userID'] = $response_data['userID'];
                        $rowArray['statusType'] = $response_data['statusType'];
                        $rowArray['ago']=$date;

                        array_push($JSON,$rowArray);
                    }
                    $response = [
                        "status" => 1,
                        "message" => "News Feeds",
                        "data" => $JSON
                    ];
                    $this->response($response);
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No news Feeds Found",
                        "data" => ''
                    ];
                    $this->response($response);
                }
            }
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "An error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function time_elapsed_string($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v){
            if($diff->$k){
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            }else{
                unset($string[$k]);
            }
        }
        if(!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function like_post()
    {
        $data = $this->post();
        $date  = date('Y-m-d H:i:s');

        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['statusid'])){
                $response = [
                    "status" => 0,
                    "message" => "Status ID is required",
                    "data" => ''
                ];
            } else {
                $userID = $data['userID'];
                $statusid  = $data['statusid'];
                $post_data = array(
                    "userID"   => $userID,
                    "statusid" => $statusid
                );
                $Count=$this->Model_post->likeCheck($post_data);
                if($Count > 0){
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! You already liked this post",
                        "data" => ''
                    ];
                } else{
                    $like_data = $this->Model_post->like($data);
                    if($like_data != ''){
                        $notificationGetResult=$this->Model_post->notificationLikes($like_data);
                        foreach($notificationGetResult as $dt) {
                            if ($userID == $dt['userID']) {

                            } else {
                                $addDataNotification = [
                                    "userID" => $dt['userID'],
                                    "friendID" => $userID,
                                    "text" => $dt['userName'] . "Like your Post",
                                    "notificationType" => "like",
                                    "isRead" => "0",
                                    "date" => $date
                                ];

                                $pushID = $dt['userID'];
                                $pushNotification = $this->Model_post->pushNotification($pushID);
                                if ($pushNotification[0]['typeDevice'] === 'ios') {
                                    $deviceToken = $pushNotification[0]['deviceToken'];
                                    $passphrase = 'abcd';
                                    $message = $dt['userName'] . " Like your Post";
                                    $ctx = stream_context_create();
                                    stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
                                    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                                    $fp = stream_socket_client(
                                        'ssl://gateway.sandbox.push.apple.com:2195', $err,
                                        $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                                    if (!$fp)
                                        exit("Failed to connect: $err $errstr" . PHP_EOL);
                                    $body['aps'] = array(
                                        'alert' => $message,
                                        'sound' => 'default'
                                    );
                                    $payload = json_encode($body);
                                    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                                    $result = fwrite($fp, $msg, strlen($msg));
                                    fclose($fp);
                                } else if ($pushNotification[0]['typeDevice'] === 'android') {
                                    $apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
                                    $registrationIDs = $pushNotification[0]['deviceToken'];
                                    $message = array(
                                        "message" => $dt['userName'] . " Like your Post",
                                        "title" => "Nocturna",
                                        "vibrate" => 1,
                                        "sound" => 1,
                                        "smallIcon" => "small_icon"
                                    );
                                    $url = 'https://fcm.googleapis.com/fcm/send';
                                    $fields = array(
                                        'registration_ids' => $registrationIDs,
                                        'data' => $message
                                    );
                                    $headers = array(
                                        'Authorization: key=' . $apiKey,
                                        'Content-Type: application/json'
                                    );
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $url);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                                    $result = curl_exec($ch);
                                    curl_close($ch);
                                } else {

                                }
                                $responseNotification = $this->Model_post->addNotification($addDataNotification);
                            }
                        }
                        $response = [
                            "status" => 1,
                            "message" => "Like Successfully Updated",
                            "data" => ''
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry! there is a problem updating like",
                            "data" => ''
                        ];
                    }
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function getAllComments_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['statusID'])){
                $response = [
                    "status" => 0,
                    "message" => "Status ID is required",
                    "data" => ''
                ];
            } else{
                $statusID = $data['statusID'];
                $count = $this->Model_post->getCommentsCount($statusID);
                if($count > 0){
                    $response_data = $this->Model_post->getAllComments($statusID);
                    if($response_data != ''){
                        $response = [
                            "status" => 1,
                            "message" => "Get all comments",
                            "data" => $response_data
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry! There was a problem",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! Status not found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function newComment_post(){
        $data = $this->post();
        $date = date('Y-m-d H:i:s');

        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            }elseif(!isset($data['statusid'])){
                $response = [
                    "status" => 0,
                    "message" => "Status ID is required",
                    "data" => ''
                ];

            }elseif (!isset($data['comment'])){
                $response = [
                    "status" => 0,
                    "message" => "Comment is required",
                    "data" => ''
                ];

            } else{
                $userID = $data['userID'];
                $statusid = $data['statusid'];
                $comment = $data['comment'];
                $comment_data = array(
                    "userID"   => $userID,
                    "statusid" => $statusid,
                    "comment"  => $comment
                );
                $response_data = $this->Model_post->comment($comment_data);
                if($response_data != ''){
                    $notificationGetResult = $this->Model_post->notificationComments($response_data);
                    foreach($notificationGetResult as $dt){
                        if($userID == $dt['userID']){
                            //i am adding the comment to my post
                            $addDataNotification = array(
                                "userID" => $dt['userID'],
                                "friendID" => $userID,
                                "text" => $dt['userName']." commented on your Post",
                                "notificationType" => "comment",
                                "isRead" => "0",
                                "date" => $date
                            );

                        }else{
                            $addDataNotification = array(
                                "userID" => $dt['userID'],
                                "friendID" => $userID,
                                "text" => $dt['userName']." commented on your Post",
                                "notificationType" => "comment",
                                "isRead" => "0",
                                "date" => $date
                            );

                            $pushID = $dt['userID'];
                            $pushNotification = $this->Model_post->pushNotification($pushID);
                            if($pushNotification[0]['typeDevice'] == 'ios'){
                                $deviceToken = $pushNotification[0]['deviceToken'];
                                $passphrase = 'abcd';
                                $message = $dt['userName']." Commented on your Post";
                                $ctx = stream_context_create();
                                stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
                                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                                $fp = stream_socket_client(
                                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                                if (!$fp)
                                    exit("Failed to connect: $err $errstr" . PHP_EOL);

                                $body['aps'] = array(
                                    'alert' => $message,
                                    'sound' => 'default'
                                );
                                $payload = json_encode($body);
                                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken)
                                    .pack('n', strlen($payload)) . $payload;
                                $result = fwrite($fp, $msg, strlen($msg));
                                fclose($fp);

                            }else if($pushNotification[0]['typeDevice'] == 'android'){
                                //$apiKey = "AIzaSyDEqZryn1zt1ylBbJxBvhkclz3cOVwRk5w";
                                $apiKey2 = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
                                $registrationIDs = $pushNotification[0]['deviceToken'];
                                $message = array(
                                    "message" => $dt['userName']." commented on your Post",
                                    "title" => "Nocturna",
                                    "vibrate" => 1,
                                    "sound" => 1,
                                    "smallIcon" => "small_icon"
                                );
                                $url = 'https://fcm.googleapis.com/fcm/send';
                                $fields = array(
                                    'registration_ids' => $registrationIDs,
                                    'data' => $message
                                );
                                $headers = array(
                                    'Authorization: key='. $apiKey2,
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt( $ch, CURLOPT_URL, $url );
                                curl_setopt( $ch, CURLOPT_POST, true );
                                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

                                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch);
                                curl_close($ch);
                            }else{

                            }
                        }
                        $responseNotification=$this->Model_post->addNotification($addDataNotification);
                    }

                    $response = [
                        "status" => 1,
                        "message" => "Comment! Successfully updated",
                        "data" => ''
                    ];
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! there is a problem, please try again.",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function deletePost_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['statusID'])){
                $response = [
                    "status" => 0,
                    "message" => "Status ID is required",
                    "data" => ''
                ];
            } else{
                $statusID = $data['statusID'];
                $countStatus = $this->Model_post->countStatus($statusID);
                if($countStatus > 0){
                    $result = $this->Model_post->statusDelete($statusID);
                    if($result == 1){
                        $response = [
                            "status" => 1,
                            "message" => "",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "There are no status found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);

        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "  Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function updatePost_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['statusID'])){
                $response = [
                    "status" => 0,
                    "message" => "Status ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['status'])){
                $response = [
                    "status" => 0,
                    "message" => "Status is required",
                    "data" => ''
                ];
            } else{
                $statusID = $data['statusID'];
                $status	= $data['status'];
                $countStatus = $this->Model_post->countStatus($statusID);
                if($countStatus > 0){
                    $result = $this->Model_post->statusEdit($statusID, $status);
                    if($result == TRUE){
                        $response = [
                            "status" => 1,
                            "message" => "Status Updated Successfully",
                            "data" => $result
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "There are no status found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

}