<?php

require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Club extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Model_club');
        $this->load->helper('url');
    }

    public function index_get()
    {
        $this->response("Welcome to the club controller");
    }

    public function search_post()
    {
        $data = $this->post();

        try{
            if(!isset($data['nameOfClub'])){
                $response = [
                    "status" => 0,
                    "message" => "Name of Club is required",
                    "data" => ''
                ];
            } else {
                $nameOfClub = $data['nameOfClub'];
                $count = $this->Model_club->clubsCount($nameOfClub);
                if($count > 0){
                    $response_data = $this->Model_club->clubs($nameOfClub);
                    $response = [
                        "status" => 1,
                        "message" => "Clubs Found",
                        "data" => $response_data
                    ];
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! No places found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function nearClub_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['lat'])){
                $response = [
                    "status" => 0,
                    "message" => "Latitude is required",
                    "data" => ''
                ];
            } elseif(!isset($data['long'])){
                $response = [
                    "status" => 0,
                    "message" => "Longitude is required",
                    "data" => ''
                ];
            } else{
                $lat = $data['lat'];
                $long = $data['long'];

                if(!isset($data['radius'])){
                    $radius = 5;
                } else{
                    $radius = $data['radius'];
                }
                $count = $this->Model_club->nearClubsCount($lat,$long,$radius);
                if($count > 0){
                    $response_data = $this->Model_club->nearClubs($lat,$long,$radius);
                    if($response_data != ''){
                        $response = [
                            "status" => 1,
                            "message" => "All near Clubs",
                            "data" => $response_data
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! No Clubs found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }


    public function getClubPackages_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['ownerID'])){
                $response = [
                    "status" => 0,
                    "message" => "Owner ID is required",
                    "data" => ''
                ];
            } else{
                $ownerID  = $data['ownerID'];
                $count = $this->Model_club->clubsPackagesCount($ownerID);
                if($count > 0){
                    $response_data =$this->Model_club->clubsPackages($ownerID);
                    $json = array();
                    foreach($response_data as $res){
                        $percentage=intval($res['discount']);
                        $perAmount = ($percentage / 100) * $res['price'];
                        $netAmount=$res['price']-$perAmount;

                        $rowArray['id']=$res['id'];
                        $rowArray['ownerID']=$res['ownerID'];
                        $rowArray['nameOfClub']=$res['nameOfClub'];
                        $rowArray['price']=$res['price'];
                        $rowArray['description_attributes']=$res['description_attributes'];
                        $rowArray['validity']=$res['validity'];
                        $rowArray['discount']=$res['discount'];
                        $rowArray['actualAmount']=$netAmount;
                        $rowArray['userName']=$res['userName'];
                        $rowArray['clubType']=$res['clubType'];
                        $rowArray['image']=$res['image'];
                        array_push($json,$rowArray);
                    }
                    $response = [
                        "status" => 1,
                        "message" => "Club Packages",
                        "data" => $json
                    ];
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No club packages",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);

        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }
}