<?php
require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Owner extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Owner_model');
        $this->load->helper('url');
    }

    public function index_get()
    {
        //returns all owner data
        $get_users = $this->Owner_model->getOwnerUser();
        $response = [
          'status' => 1,
          'message' => "All owner user",
          'data' => $get_users
        ];

        $this->response($response);
    }

    public function signUp_post()
    {
        $data = $this->post();
        if (!isset($data['accountType'])) {
            $response = array(
                'status' => 0,
                'message' => "Account type is required",
                'data' => ''
            );
            $this->response($response);
        } else {
            try {
                if ($data['accountType'] == 'phone') {

                    if (!isset($data['phoneNumber']) || $data['phoneNumber'] == null) {
                        $response = array(
                            'status' => 0,
                            'message' => "Phone number is required",
                            'data' => ''
                        );
                        $this->response($response);
                    } else {
                        $phoneCount = $this->Owner_model->phoneCount($data['phoneNumber']);
                        if ($phoneCount > 0) {
                            $response = array(
                                "status" => 0,
                                "message" => "Number is already registered",
                                "data" => ''
                            );
                        } else {
                            $code = rand();
                            $data = array(
                                "phoneNumber" => $data['phoneNumber'],
                                "verificationCode" => $code
                            );
                            $phoneRegister = $this->Owner_model->phoneRegister($data);
                            $phone = $this->Owner_model->phone($phoneRegister);

                            $sendNumber = $phone[0]['phoneNumber'];
                            $verificationCode = $phone[0]['verificationCode'];

                            //todo: test that this url works
                            //send phone verification code via sms to phone number
                            $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
                                    array(
                                        'api_key' => '85495afd',
                                        'api_secret' => '8140f35b6784c6f1',
                                        'to' => $sendNumber,
                                        'from' => 'Nocturna',
                                        'text' => $verificationCode . ": Verification Code"
                                    )
                                );

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $curl_response = curl_exec($ch);

                            $response = array(
                                "status" => 1,
                                "message" => "Account created successfully",
                                "data" => $data
                            );
                        }
                    }
                } elseif ($data['accountType'] == "manual") {
                    if (!isset($data['userName'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Username is required",
                            "data" => ''
                        );
                    } else if (!isset($data['nameOfClub'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Name Of Club is Required",
                            "data" => ''
                        );
                    } else if (!isset($data['password'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Password is required",
                            "data" => ''
                        );
                    } else if (!isset($data['confirmPassword'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Confirm Password is Required",
                            "data" => ''
                        );
                    } else if (!isset($data['phoneNumber'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Phone Number is Required",
                            "data" => ''
                        );
                    } else if (!isset($data['address'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Address is Required",
                            "data" => ''
                        );
                    } else if (!isset($data['clubDis'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Club Dis is Required",
                            "data" => ''
                        );
                    } else if (!isset($data['deviceToken'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Device Token is Required",
                            "data" => ''
                        );
                    } else if (!isset($data['typeDevice'])) {
                        $response = array(
                            "status" => 0,
                            "message" => "Type of device is Required",
                            "data" => ''
                        );
                    } else if(!isset($data['clubType'])){
                        $response = array(
                            "status" => 0,
                            "message" => "Club type is Required",
                            "data" => ''
                        );
                    }else if(!isset($data['endTime'])){
                        $response = array(
                            "status" => 0,
                            "message" => "EndTime is Required",
                            "data" => ''
                        );
                    } else {
                        $clubType = $data['clubType'];
                        if (!isset($_FILES["profilePicture"]["name"])) {
                            $address = $data['address'];
                            $Address = urlencode($address);
                            $api_key = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
                            $request_url2 = "https://maps.googleapis.com/maps/api/geocode/xml?address=$Address&key=$api_key";
                            $xml = simplexml_load_file($request_url2) or die("url not loading");
                            $status = $xml->status;

                            if ($status == "OK") {
                                $lat = $xml->result->geometry->location->lat;
                                $long = $xml->result->geometry->location->lng;
                            } else {
                                $lat = null;
                                $long = null;

                                $response = array(
                                    "status" => 0,
                                    "message" => "Latitude and Longitude could not be generated from the given address",
                                    "data" => $Address
                                );
                                $this->response($response);
                            }
                            $countRow = $this->Owner_model->checkUserOwner($data['userName']);
                            if ($countRow > 0) {
                                $response = array(
                                    "status" => 0,
                                    "message" => "This username already exists",
                                    "data" => ''
                                );
                            } else {
                                $response_data = array(
                                    "userName" => $data['userName'],
                                    "nameOfClub" => $data['nameOfClub'],
                                    "startTime" => $data['startTime'],
                                    "endTime" => $data['endTime'],
                                    "clubDis" => $data['clubDis'],
                                    "password" => $data['password'],
                                    "phoneNumber" => $data['phoneNumber'],
                                    "address" => $address,
                                    "email" => $data['email'],
                                    "deviceToken" => $data['deviceToken'],
                                    "clubType" => $clubType,
                                    "typeDevice" => $data['typeDevice'],
                                    "lat" => $lat,
                                    "long" => $long,
                                    "accountActive" => "active",
                                    "audio" => ""
                                );
                                $id = $this->Owner_model->ownerSignup($response_data);
                                $result = $this->Owner_model->ownerSignupData($id);
                                $response = array(
                                    "status" => 1,
                                    "message" => "Owner User Signup Result",
                                    "data" => $result
                                );
                            }
                        } else {
                            $profilePicture = isset($_FILES["profilePicture"]["name"])?$_FILES["profilePicture"]["name"]:'';
                            $Address = urlencode($data['address']);
                            $api_key = "AIzaSyAVhhc8vuWijxfQ-NO0soUBiD3wXV8b-gE";
                            $request_url2 = "https://maps.googleapis.com/maps/api/geocode/xml?address=$Address&key=$api_key";
                            $xml = simplexml_load_file($request_url2) or die("url not loading");
                            $status = $xml->status;

                            if ($status == "OK") {
                                $lat = $xml->result->geometry->location->lat;
                                $long = $xml->result->geometry->location->lng;
                            } else {
                                $lat = " ";
                                $long = " ";
                            }

                            $profilePicture = mt_rand() . ".jpg";
                            move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/owner_user/profile/" . $profilePicture);
                            $countRow = $this->Owner_model->checkUserOwner($data['userName']);
                            if ($countRow > 0) {
                                $response = array(
                                    "status" => 0,
                                    "message" => "This User Name already Exist",
                                    "data" => ''
                                );
                            } else {
                                $response_data = array(
                                    "userName" => $data['userName'],
                                    "nameOfClub" => $data['nameOfClub'],
                                    "startTime" => $data['startTime'],
                                    "endTime" => $data['endTime'],
                                    "clubDis" => $data['clubDis'],
                                    "password" => $data['password'],
                                    "phoneNumber" => $data['phoneNumber'],
                                    "address" => $data['address'],
                                    "email" => $data['email'],
                                    "deviceToken" => $data['deviceToken'],
                                    "clubType" => $data['clubType'],
                                    "typeDevice" => $data['typeDevice'],
                                    "profile" => $profilePicture,
                                    "lat" => $lat,
                                    "long" => $long,
                                    "accountActive" => "active",
                                    "audio" => ""
                                );
                                $id = $this->Owner_model->ownerSignup($response_data);
                                $result = $this->Owner_model->ownerSignupData($id);

                                $response = array(
                                    "status" => 1,
                                    "message" => "Owner User Signup Result",
                                    "data" => $result
                                );
                            }
                        }
                    }
                }
                $this->response($response);
            } catch (Exception $e) {
                $response = array(
                    "status" => 0,
                    "message" => "Error Occurred",
                    "data" => ""
                );
                $this->response($response);
            }
        }
    }


    public function login_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userName'])){
                $response = array(
                    "status" => 0,
                    "message" => "User Name is Required",
                    "data" => ''
                );
            }elseif(!isset($data['password'])){
                $response = array(
                    "status" => 0,
                    "message" => "Password is Required",
                    "data" => ''
                );
            } else {
                $userName = $data['userName'];
                $password = $data['password'];
                $user_data = array(
                    "userName" => $userName,
                    "password" => $password,
                    "accountActive" => "active"
                );
                $rowCount = $this->Owner_model->ownerLoginCount($user_data);
                if($rowCount > 0){
                    $result = $this->Owner_model->ownerLogin($user_data);
                    $response = array(
                        "status" => 1,
                        "message" => "Owner Login Details.",
                        "data" => $result
                    );
                }else{
                    $response = array(
                        "status" => 0,
                        "message" => "No user found.",
                        "data" => ''
                    );
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = array(
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            );
            $this->response($response);
        }
    }


    public function updateTokenDevice_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['ownerID'])){
                $response = [
                    "status" => 0,
                    "message" => "Owner ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['deviceToken'])){
                $response = [
                    "status" => 0,
                    "message" => "Device Token is required",
                    "data" => ''
                ];
            } elseif(!isset($data['typeDevice'])){
                $response = [
                    "status" => 0,
                    "message" => "Type device is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $deviceToken = $data['deviceToken'];
                $typeDevice = $data['typeDevice'];
                $response_data = [
                    "deviceToken" => $deviceToken,
                    "typeDevice" => $typeDevice
                ];
                $count = $this->Owner_model->updateTokenDeviceCount($userID, $response_data);
                if($count > 0){
                    $result = $this->Owner_model->updateTokenDevice($userID, $response_data);
                    $response = [
                        "status" => 1,
                        "message" => "Device Token and Device Type has been updated",
                        "data" => $result
                    ];

                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry Device Token and Device Type cannot be updated",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function feedback_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['feedback'])){
                $response = [
                    "status" => 0,
                    "message" => "Feedback is required",
                    "data" => ''
                ];
            } else{
                $userID	= $data['userID'];
                $feedBack = $data['feedback'];
                $response_data = ["userID"=>$userID,"feedback"=>$feedBack];
                $result = $this->Owner_model->feedBack($response_data);
                if($result != ''){
                    $response = [
                        "status" => 1,
                        "message" => "Feed Back is saved successfully.",
                        "data" => ''
                    ];
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "User does not exist",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function uploadAudio_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($_FILES["audio"]["name"])){
                $response = [
                    "status" => 0,
                    "message" => "Audio is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $audio = isset($_FILES["audio"]["name"])?$_FILES["audio"]["name"]:'';
                $audio = mt_rand().".mp3";

                $response_data = array(
                    "id" => $userID
                );

                $count = $this->Owner_model->ownerUserPicture($response_data);
                if($count > 0){
                    $getPicture = $this->Owner_model->ownerGetPicture($response_data);
                    $audioGet = $getPicture[0]['audio'];
                    if($audioGet == ''){
                        $response_data['audio'] = $audio;
                        $updatePicture = $this->Owner_model->ownerUpdatePicture($userID, $response_data);
                        if($updatePicture == TRUE){
                            $move = move_uploaded_file($_FILES["audio"]["tmp_name"], getcwd() . "/uploads/owner_user/audio/" . $audio);
                            if($move == 1){
                                $data = array(
                                    "id" => $userID
                                );
                                $result = $this->Owner_model->ownerLogin($data);
                                $response = [
                                    "status" => 1,
                                    "message" => "Audio update is successful",
                                    "data" => $result
                                ];
                            } else{
                                $response = [
                                    "status" => 0,
                                    "message" => "Sorry file was not moved",
                                    "data" => ''
                                ];
                            }
                        } else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry there is a problem",
                                "data" => ''
                            ];
                        }
                    } else{
                        $audio_file = getcwd(). "/uploads/owner_user/audio/". $audioGet;
                        if(!is_file($audio_file)){
                            $data = array('audio' => $audio);
                            $updatePicture = $this->Owner_model->ownerUpdatePicture($userID, $data);
                            if($updatePicture == TRUE){
                                $move = move_uploaded_file($_FILES["audio"]["tmp_name"], getcwd() . "/uploads/owner_user/audio/" . $audio);
                                if($move == 1){
                                    $data = array(
                                        "id" => $userID
                                    );
                                    $result = $this->Owner_model->ownerLogin($data);
                                    $response = [
                                        "status" => 1,
                                        "message" => "Audio update is successful",
                                        "data" => $result
                                    ];
                                }else{
                                    $response = [
                                        "status" => 0,
                                        "message" => "Sorry! File was not move",
                                        "data" => ''
                                    ];
                                }
                            }else{
                                $response = [
                                    "status" => 0,
                                    "message" => "Sorry there is some problem",
                                    "data" => ''
                                ];
                            }
                        } else {
                            $unlink = unlink(getcwd() . "/uploads/owner_user/audio/" . $audioGet);
                            if($unlink == 1){
                                $data = array('audio' => $audio);
                                $updatePicture = $this->Owner_model->ownerUpdatePicture($userID, $data);
                                if($updatePicture == TRUE){
                                    $move = move_uploaded_file($_FILES["audio"]["tmp_name"], getcwd() . "/uploads/owner_user/audio/" . $audio);
                                    if($move == 1){
                                        $data = array(
                                            "id" => $userID
                                        );
                                        $result = $this->Owner_model->ownerLogin($data);
                                        $response = [
                                            "status" => 1,
                                            "message" => "Audio update is successful",
                                            "data" => $result
                                        ];
                                    }else{
                                        $response = [
                                            "status" => 0,
                                            "message" => "Sorry! File was not move",
                                            "data" => ''
                                        ];
                                    }
                                }else{
                                    $response = [
                                        "status" => 0,
                                        "message" => "Sorry there is some problem",
                                        "data" => ''
                                    ];
                                }
                            }else{
                                $response = [
                                    "status" => 0,
                                    "message" => "An error occurred while trying to upload new audio",
                                    "data" => ''
                                ];
                            }
                        }
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No user found",
                        "data" => ''
                    ];

                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function ownerEditProfile_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['nameOfClub'])){
                $response = [
                    "status" => 0,
                    "message" => "Name of club is required",
                    "data" => ''
                ];
            } elseif(!isset($data['password'])){
                $response = [
                    "status" => 0,
                    "message" => "Password is required",
                    "data" => ''
                ];
            } elseif (!isset($data['userName'])){
                $response = [
                    "status" => 0,
                    "message" => "Username is required",
                    "data" => ''
                ];
            } elseif(!isset($data['phoneNumber'])){
                $response = [
                    "status" => 0,
                    "message" => "Phone number is required",
                    "data" => ''
                ];
            } elseif(!isset($data['startTime'])){
                $response = [
                    "status" => 0,
                    "message" => "Start Time is required",
                    "data" => ''
                ];
            } elseif(!isset($data['endTime'])){
                $response = [
                    "status" => 0,
                    "message" => "End time is required",
                    "data" => ''
                ];
            } elseif(!isset($data['clubDis'])){
                $response = [
                    "status" => 0,
                    "message" => "Club dis is required",
                    "data" => ''
                ];
            } elseif(!isset($data['address'])){
                $response = [
                    "status" => 0,
                    "message" => "Address is required",
                    "data" => ''
                ];
            } elseif(!isset($data['email'])){
                $response = [
                    "status" => 0,
                    "message" => "Email is required",
                    "data" => ''
                ];
            } elseif(!isset($data['clubType'])){
                $response = [
                    "status" => 0,
                    "message" => "Club type is required",
                    "data" => ''
                ];
            }  else{

                $userID = $data['userID'];
                $data['id'] = $data['userID'];
                unset($data['userID']);

                $login_data = array(
                    "id" => $userID
                );

                $ownerProfile = isset($_FILES["ownerProfile"]["name"])?$_FILES["ownerProfile"]["name"]:'';
                $ownerCover = isset($_FILES["ownerCover"]["name"])?$_FILES["ownerCover"]["name"]:'';
                if($ownerProfile == '' && $ownerCover == ''){
                    $updatePicture = $this->Owner_model->ownerUpdatePicture($userID, $data);
                    if($updatePicture == TRUE){
                        $result = $this->Owner_model->ownerLogin($login_data);
                        $response = [
                            "status" => 1,
                            "message" => "Profile Updated Successfully",
                            "data" => $result
                        ];
                    } else {
                        $response = [
                            "status" => 0,
                            "message" => "Sorry there is some problem",
                            "data" => ''
                        ];
                    }
                }else if($ownerProfile != '' && $ownerCover == ''){
                    $ownerProfile = mt_rand().".jpg";
                    $Count = $this->Owner_model->ownerUserPicture($login_data);
                    if($Count > 0){
                        $getPicture = $this->Owner_model->ownerGetPicture($login_data);
                        $profile = $getPicture[0]['profile'];
                        if($profile == ''){
                            $data['profile'] = $ownerProfile;
                            $updatePicture = $this->Owner_model->ownerUpdatePicture($userID, $data);
                            if($updatePicture == TRUE){
                                move_uploaded_file($_FILES["ownerProfile"]["tmp_name"], getcwd() . "/uploads/owner_user/profile/" . $ownerProfile);
                                $result = $this->Owner_model->ownerLogin($login_data);
                                $response = [
                                    "status" => 1,
                                    "message" => "Profile Updated Successfully",
                                    "data" => $result
                                ];
                            } else {
                                $response = [
                                    "status" => 0,
                                    "message" => "Sorry there is a problem",
                                    "data" => ''
                                ];
                            }
                        }else{
                            $prev_picture =  getcwd() . "/uploads/owner_user/profile/" . $profile;
                            if(is_file($prev_picture)){
                                $unlink = unlink(getcwd() . "/uploads/owner_user/profile/" . $profile);
                                if($unlink == 1){
                                    $data['profile'] = $ownerProfile;
                                    $updatePicture = $this->Owner_model->ownerUpdatePicture($userID, $data);
                                    if($updatePicture == TRUE){
                                        move_uploaded_file($_FILES["ownerProfile"]["tmp_name"], getcwd() . "/uploads/owner_user/profile/" . $ownerProfile);
                                        $result=$this->Owner_model->ownerLogin($login_data);

                                        $response = [
                                            "status" => 1,
                                            "message" => "Profile Updated Successfully",
                                            "data" => $result
                                        ];
                                    } else {
                                        $response = [
                                            "status" => 0,
                                            "message" => "Sorry there is a problem",
                                            "data" => ''
                                        ];
                                    }
                                } else {
                                    $response = [
                                        "status" => 0,
                                        "message" => "There is a problem with our database",
                                        "data" => ''
                                    ];
                                }
                            } else {
                                $data['profile'] = $ownerProfile;
                                $updatePicture = $this->Owner_model->ownerUpdatePicture($userID, $data);
                                if($updatePicture == TRUE){
                                    move_uploaded_file($_FILES["ownerProfile"]["tmp_name"], getcwd() . "/uploads/owner_user/profile/" . $ownerProfile);
                                    $result = $this->Owner_model->ownerLogin($login_data);
                                    $response = [
                                        "status" => 1,
                                        "message" => "Profile Updated Successfully",
                                        "data" => $result
                                    ];
                                } else {
                                    $response = [
                                        "status" => 0,
                                        "message" => "Sorry new profile picture not updated",
                                        "data" => ''
                                    ];
                                }
                            }
                        }
                    } else {
                        $response = [
                            "status" => 0,
                            "message" => "No user Found",
                            "data" => ''
                        ];
                    }
                }else if($ownerProfile == '' && $ownerCover != ''){
                    $ownerCover = mt_rand().".jpg";
                    $Count = $this->Owner_model->ownerUserPicture($login_data);
                    if($Count > 0){
                        $getPicture = $this->Owner_model->ownerGetPicture($data);
                        $profile = $getPicture[0]['cover'];

                        if($profile == ''){
                            $data['cover'] = $ownerCover;
                            $updatePicture = $this->Owner_model->ownerUpdatePicture($userID,$data);
                            if($updatePicture == TRUE){
                                move_uploaded_file($_FILES["ownerCover"]["tmp_name"], getcwd() . "/uploads/owner_user/cover/" . $ownerCover);
                                $result = $this->Owner_model->ownerLogin($login_data);
                                $response = [
                                    "status" => 1,
                                    "message" => "Cover Upload Successfully",
                                    "data" => $result
                                ];
                            } else{
                                $response = [
                                    "status" => 0,
                                    "message" => "Sorry there is some problem",
                                    "data" => ''
                                ];
                            }
                        }else{
                            $unlink = unlink(getcwd() . "/uploads/owner_user/cover/" . $profile);
                            if($unlink == 1){
                                $data['cover'] = $ownerCover;
                                $updatePicture = $this->Owner_model->ownerUpdatePicture($userID,$data);
                                if($updatePicture == TRUE){
                                    move_uploaded_file($_FILES["ownerCover"]["tmp_name"], getcwd() . "/uploads/owner_user/cover/" . $ownerCover);
                                    $result=$this->Owner_model->ownerLogin($login_data);
                                    $response = [
                                        "status" => 1,
                                        "message" => "Cover Upload Successfully",
                                        "data" => $result
                                    ];
                                } else{
                                    $response = [
                                        "status" => 0,
                                        "message" => "Sorry there is some problem",
                                        "data" => ''
                                    ];
                                }
                            } else{
                                $response = [
                                    "status" => 0,
                                    "message" => "There is some problem our database",
                                    "data" => ''
                                ];
                            }
                        }
                    } else{
                        $response = [
                            "status" => 0,
                            "message" => "No user found",
                            "data" => ''
                        ];
                    }
                } else {
                    $response = [
                        "status" => 0,
                        "message" => "Required Parameters",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "An error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function saveQrCode_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['qrCode'])){
                $response = [
                    "status" => 0,
                    "message" => "QR code is required",
                    "data" => ''
                ];
            } else {
                $save_data = array();
                $save_data['owner_id'] = $data['userID'];
                $save_data['qrCode'] = $data['qrCode'];

                $check_user = array(
                    "id" => $data['userID']
                );
                $count = $this->Owner_model->ownerUserPicture($check_user);
                if($count > 0) {

                    //check if qr code exists
                    $check_qr = $this->Owner_model->savedQrCode($save_data);
                    if($check_qr > 0){

                        $response = [
                            "status" => 0,
                            "message" => "Invalid QR code, it has been scanned previously",
                            "data" => ''
                        ];

                    } else {
                        $save_details = $this->Owner_model->saveQRCode($save_data);
                        if($save_details == true){
                            $response = [
                                "status" => 1,
                                "message" => "Owner QR Code is successfully saved",
                                "data" => $data
                            ];
                        } else {
                            $response = [
                                "status" => 0,
                                "message" => "An error occurred while saving data, please try again",
                                "data" => ''
                            ];
                        }
                    }
                } else {
                    $response = [
                        "status" => 0,
                        "message" => "This user is not found",
                        "data" => ""
                    ];
                }
            }

            $this->response($response);
        } catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "An error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function fetchQrCode_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } else {
                $check_user = array(
                    "id" => $data['userID']
                );
                $count = $this->Owner_model->ownerUserPicture($check_user);
                if($count > 0){
                    $id = $data['userID'];
                    $fetchAll = $this->Owner_model->getQrCode($id);
                    $response = [
                        "status" => 1,
                        "message" => "Owner Qr Codes",
                        "data" => $fetchAll
                    ];
                } else {
                    $response = [
                        "status" => 0,
                        "message" => "No user found",
                        "sata" => ''
                    ];
                }
            }
            $this->response($response);
        } catch (Exception $exception){
            $response = [
                "status" => 0,
                "message" => "An error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }
}
