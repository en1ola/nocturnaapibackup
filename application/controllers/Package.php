<?php


require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
class Package extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_package');
        $this->load->model('Model_cart');
        $this->load->helper('url');
    }

    public function index_get()
    {
        echo "Package is set up";
    }


    public function packagePurchase_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['packageID'])){
                $response = [
                    "status" => 0,
                    "message" => "Package is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $packageID = $data['packageID'];
                $response_data = [
                    "userID" => $userID,
                    "packageID" => $packageID,
                ];
                $count = $this->Model_package->packagePurchaseCount($response_data);
                if($count > 0){
                    $response = [
                        "status" => 0,
                        "message" => "This package is already purchased",
                        "data" => ''
                    ];
                }else{

                    $result = $this->Model_package->packagePurchase($data);
                    $response = [
                        "status" => 1,
                        "message" => "Package successfully purchased",
                        "data" => $result
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function cartPurchasedPackage_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } else{
                $userID	= $data['userID'];
                $checkUser = $this->Model_package->checkUserPurchased($userID);
                if($checkUser > 0){
                    $response_package = $this->Model_package->myCartPurchased($userID);
                    foreach($response_package as $dt){
                        $idsPackages[] = $dt['packageID'];
                    }
                    $commaValues = implode(",",$idsPackages);
                    $result = $this->Model_package->allPackagesCartPurchased($commaValues,$userID);
                    $response = [
                        "status" => 1,
                        "message" => "Packages",
                        "data" => $result
                    ];
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No packages found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function splitData_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } else {
                $userID = $data['userID'];
                $resCount = $this->Model_package->splitDataCount($userID);
                if($resCount > 0){
                    $result = $this->Model_package->splitData($userID);
                    if($result > 0){
                        foreach ($result as $splitData){
                            $percentage = intval($splitData['discount']);
                            $perAmount = ($percentage / 100) * $splitData['price'];
                            $netAmount = $splitData['price'] - $perAmount;
                            $rowArray = array();

                            $rowArray['id'] = $splitData['id'];
                            $rowArray['userID'] = $splitData['userID'];
                            $rowArray['type'] = $splitData['type'];
                            $rowArray['splitID'] = $splitData['splitID'];
                            $rowArray['split_price'] = $splitData['split_price'];
                            $rowArray['friendID'] = $splitData['friendID'];
                            $rowArray['status'] = $splitData['status'];
                            $rowArray['username'] = $splitData['userName'];
                            $rowArray['fullName'] = $splitData['fullName'];
                            $rowArray['email'] = $splitData['email'];
                            $rowArray['packageID'] = $splitData['packageID'];
                            $rowArray['cartID'] = $splitData['id'];
                            $rowArray['ownerID'] = $splitData['ownerID'];
                            $rowArray['nameOfClub'] = " " . $splitData['nameOfClub'];
                            $rowArray['price'] = $splitData['price'];
                            $rowArray['description_attributes'] = $splitData['description_attributes'];
                            $rowArray['validity'] = $splitData['validity'];
                            $rowArray['discount'] = $splitData['discount'];
                            $rowArray['split_userName'] = $splitData['userName'];
                            $rowArray['clubType'] = " " . $splitData['clubType'];
                            $rowArray['image'] = $splitData['profilePicture'];
                            $rowArray['split_type'] = $splitData['type'];
                            $rowArray['owner_club'] = " " . $splitData['owner_club'];

                            //save in an array
                            $final_data[] = $rowArray;
                        }
                        $response = [
                            "status" => 1,
                            "message" => "Split Data",
                            "data" => $final_data
                        ];
                    }


                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No split data found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function splitApprove_post()
    {
        $data = $this->post();
        try {
            if (!isset($data['cartID'])) {
                $response = [
                    "status" => 0,
                    "message" => "Cart ID is required",
                    "data" => ''
                ];
            } elseif (!isset($data['friendID'])) {
                $response = [
                    "status" => 0,
                    "message" => "Friend ID is required",
                    "data" => ''
                ];
            } else {
                $cartID = $data['cartID'];
                $friendID = $data['friendID'];

                //validate if split data exist for such and updates the data
                $updateSplit = $this->Model_package->updatePurchasedSplitItem($cartID, $friendID);
                if($updateSplit == 1){
                    //split update is successful, it returns 1
                    $response = [
                        "status" => 1,
                        "message" => "Split is successfully updated / removed",
                        "data" => ''
                    ];


                } else {
                    //split update is not successful, false is returned
                    $response = [
                        "status" => 0,
                        "message" => "Split data not found, update failed",
                        "data" => ''
                    ];

                }
                $this->response($response);
            }
            $this->response($response);
        } catch (Exception $e) {
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function visitedPlaces_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $count = $this->Model_package->visitedPlacesCount($userID);
                if($count > 0){
                    $json = array();
                    $response_data = $this->Model_package->visitedPlaces($userID);
                    foreach($response_data as $dt){
                        $date = $this->time_elapsed_string($dt['date']);
                        $row_array = array();
                        if($dt['statusType'] == 'package'){
                            $row_array['packageID']=$dt['packageID'];
                            $row_array['ownerID']=$dt['ownerID'];
                            $row_array['userName']=$dt['userName'];
                            $row_array['nameOfClub']=$dt['nameOfClub'];
                            $row_array['startTime']=$dt['startTime'];
                            $row_array['date']=$date;
                            $row_array['endTime']=$dt['endTime'];
                            $row_array['clubDis']=$dt['clubDis'];
                            $row_array['phoneNumber']=$dt['phoneNumber'];
                            $row_array['address']=$dt['address'];
                            $row_array['email']=$dt['email'];
                            $row_array['deviceToken']=$dt['deviceToken'];
                            $row_array['clubType']=$dt['clubType'];
                            $row_array['typeDevice']=$dt['typeDevice'];
                            $row_array['profile']=$dt['profile'];
                            $row_array['cover']=$dt['cover'];
                            $row_array['lat']=$dt['lat'];
                            $row_array['long']=$dt['long'];
                            $row_array['statusType']=$dt['statusType'];
                        }else if($dt['statusType'] == 'checkin'){
                            $row_array['id']=$dt['packageID'];
                            $row_array['userID']=$dt['ownerID'];
                            $row_array['userName']=$dt['userName'];
                            $row_array['status']=$dt['nameOfClub'];
                            $row_array['checkin']=$dt['startTime'];
                            $row_array['date']=$date;
                            $row_array['profile']=$dt['profile'];
                            $row_array['statusType']=$dt['statusType'];
                        }else{}
                        array_push($json,$row_array);
                    }
                    $response = [
                        "status" => 1,
                        "message" => "Visited places",
                        "data" => $json
                    ];
                   $this->response($response);
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "No Visited Places",
                        "data" => ''
                    ];
                    $this->response($response);
                }
            }
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }


    public function time_elapsed_string($datetime, $full = false){
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v){
            if($diff->$k){
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            }else{
                unset($string[$k]);
            }
        }
        if(!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }


    public function deletePackage_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['packageID'])){
                $response = [
                    "status" => 0,
                    "message" => "Package ID is required",
                    "data" => ''
                ];
            } else{
                $packageID = $data['packageID'];
                $result = $this->Model_package->deletePackage($packageID);
                //todo: look into making this soft delete instead
                if($result == 1){
                    $response = [
                        "status" => 1,
                        "message" => "Package Deleted Successfully.",
                        "data" => ''
                    ];

                }else {
                    $response = [
                        "status" => 0,
                        "message" => "Request Failed, package not found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function updatePackage_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['packageID'])){
                $response = [
                    "status" => 0,
                    "message" => "Package ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['nameOfClub'])){
                $response = [
                    "status" => 0,
                    "message" => "Name of Club is required",
                    "data" => ''
                ];
            } elseif(!isset($data['price'])){
                $response = [
                    "status" => 0,
                    "message" => "Price is required",
                    "data" => ''
                ];
            } elseif(!isset($data['description_attributes'])){
                $response = [
                    "status" => 0,
                    "message" => "Description Attributes is required",
                    "data" => ''
                ];
            } elseif(!isset($data['discount'])){
                $response = [
                    "status" => 0,
                    "message" => "Discount is required",
                    "data" => ''
                ];
            } else{
                $packageID = $data['packageID'];
                $nameOfClub = $data['nameOfClub'];
                $price = $data['price'];
                $description_attributes = $data['description_attributes'];
                $discount = $data['discount'];
                $image = isset($_FILES["image"]["name"])?$_FILES["image"]["name"]:'';
                if($image == FALSE || $image == ''){
                    $data = [
                        "nameOfClub" =>$nameOfClub,
                        "price" =>$price,
                        "description_attributes" =>$description_attributes,
                        "discount" =>$discount
                    ];

                    $result = $this->Model_package->packageUpdate($packageID,$data);
                    if($result == TRUE){
                        $response = [
                            "status" => 1,
                            "message" => "Package Updated Successfully",
                            "data" => ''
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Sorry! Package Not Updated",
                            "data" => ''
                        ];
                    }
                }else{
                    $image = rand().".jpg";
                    $getImage=$this->Model_package->getPackageImage($packageID);
                    if($getImage[0]['image'] == FALSE || $getImage[0]['image'] == ''){
                        $move = move_uploaded_file($_FILES["image"]["tmp_name"], getcwd() . "/uploads/owner_user/package/" . $image);
                        if($move == 1){
                            $data_source = array("image"=>$image);
                            $result = $this->Model_package->packageUpdate($packageID, $data_source);
                            if($result == TRUE){
                                $response = [
                                    "status" => 1,
                                    "message" => "Package Image Successfully Updated",
                                    "data" => ''
                                ];
                            }else{
                                $response = [
                                    "status" => 0,
                                    "message" => "Sorry! Name is not Inserted",
                                    "data" => ''
                                ];
                            }
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry! File was not Move",
                                "data" => ''
                            ];
                        }
                    }else{
                        $getImage = $this->Model_package->getPackageImage($packageID);
                        $unlink = unlink(getcwd() . "/uploads/owner_user/package/" . $getImage[0]['image']);
                        if($unlink == 1){
                            $move = move_uploaded_file($_FILES["image"]["tmp_name"], getcwd() . "/uploads/owner_user/package/" . $image);
                            if($move == 1){
                                $data=array("image"=>$image);
                                $responce=$this->Model_package->packageUpdate($packageID,$data);
                                if($responce == TRUE){
                                    $response = [
                                        "status" => 1,
                                        "message" => "Package Image Successfully Updated",
                                        "data" => ''
                                    ];
                                }else{
                                    $response = [
                                        "status" => 0,
                                        "message" => "Sorry! Name is not Inserted",
                                        "data" => ''
                                    ];
                                }
                            }else{
                                $response = [
                                    "status" => 0,
                                    "message" => "Sorry! File was not Move",
                                    "data" => ''
                                ];
                            }
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry! File was not Removed",
                                "data" => ''
                            ];
                        }
                    }
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ' '
            ];
            $this->response($response);
        }
    }
}