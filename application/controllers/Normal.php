<?php

require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Normal extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Normal_model');
        $this->load->model('Owner_model');
        $this->load->library('email');
        $this->load->helper('url');
    }

    public function index_get()
    {
        $this->response("Welcome to Nocturna app API");
    }

    public function signUp_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userName'])){
                $response = [
                    "status" => 0,
                    "message" => "Username is required",
                    "data" => ''
                ];
            }elseif (!isset($data['password'])){
                $response = [
                    "status" => 0,
                    "message" => "Password is required",
                    "data" => ''
                ];
            } elseif(!isset($data['confirmPassword'])){
                $response = [
                    "status" => 0,
                    "message" => "Confirm Password is required",
                    "data" => ''
                ];
            }else if($data['confirmPassword'] != $data['password']){
                $response = [
                    "status" => 0,
                    "message" => "Password does not match",
                    "data" => ''
                ];
            }elseif (!isset($data['deviceToken'])){
                $response = [
                    "status" => 0,
                    "message" => "Device Token is required",
                    "data" => ''
                ];
            }elseif(!isset($data['typeDevice'])){
                $response = [
                    "status" => 0,
                    "message" => "Type of Device is required",
                    "data" => ''
                ];
            }elseif (!isset($data['fullName'])){
                $response = [
                    "status" => 0,
                    "message" => "Full name is required",
                    "data" => ''
                ];
            } else{
                $userName = $data['userName'];
                $fullName = $data['fullName'];
                $password = $data['password'];
                $confirmPassword = $data['confirmPassword'];
                $deviceToken = $data['deviceToken'];
                $typeDevice = $data['typeDevice'];
                $profilePicture = isset($_FILES["profilePicture"]["name"])?$_FILES["profilePicture"]["name"]:'';

                if($password !== $confirmPassword){
                    $response = [
                        "status" => 0,
                        "message" => "Confirm password does not match with password",
                        "data" => ''
                    ];
                    $this->response($response);
                }

                if(!isset($data['email'])){
                    $email = " ";
                } else {
                    $email = $data['email'];
                }
                $query_result = $this->Normal_model->checkUserNormal($userName);
                $countRow = $query_result['countRow'];
                if($countRow > 0){
                    $the_user = $query_result['user'][0];
                    $response = array(
                        "status" => 0,
                        "message" => "This username already exists",
                        "data" => $the_user
                    );
                } else {
                    if(!isset($profilePicture) || $profilePicture == ''){
                        $data = array(
                            "userName" => $userName,
                            "fullName" => $fullName,
                            "email" => $email,
                            "password" => $password,
                            "deviceToken" => $deviceToken,
                            "typeDevice" => $typeDevice
                        );
                        $id = $this->Normal_model->normalSignup($data);
                        $result = $this->Normal_model->normalSignupData($id);
                        $response = array(
                            "status" => 1,
                            "message" => "User Signup Result",
                            "data" => $result
                        );
                    } else {
                        $profilePicture = mt_rand().".jpg";
                        move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/normal_user/profile/".$profilePicture);
                        $data=array(
                            "userName" => $userName,
                            "fullName" => $fullName,
                            "email" => $email,
                            "password" => $password,
                            "deviceToken" => $deviceToken,
                            "typeDevice" => $typeDevice,
                            "profilePicture" => $profilePicture
                        );

                        $id = $this->Normal_model->normalSignup($data);
                        $result = $this->Normal_model->normalSignupData($id);
                        $response = array(
                            "status" => 1,
                            "message" => "Normal User Signup Result",
                            "data" => $result
                        );
                    }
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = array(
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            );
            $this->response($response);
        }
    }

    public function login_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userName'])){
                $response = [
                    "status" => 0,
                    "message" => "Username is required",
                    "data" => ''
                ];
            }elseif (!isset($data['password'])){
                $response = [
                    "status" => 0,
                    "message" => "Password is required",
                    "data" => ''
                ];
            } else{
                $userName = $data['userName'];
                $password = $data['password'];
                $post_data = array(
                    "userName" => $userName,
                    "password"=> $password
                );
                $rowCount=$this->Normal_model->normalLoginCount($post_data);
                if($rowCount>0){
                    $result=$this->Normal_model->normalLogin($post_data);
                    $response = array (
                        "status" => 1,
                        "message" => "Normal Login Details",
                        "data" => $result
                    );
                }else{
                    $response = array (
                        "status" => 0,
                        "message" => "No user found",
                        "data" => ''
                    );
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = array (
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            );
            $this->response($response);
        }
    }

    public function production_post()
    {
        $data = $this->post();
        $response = [
            "status"  => 1,
            "message" => "Production changes occurred due to last push",
            "data" => $data

        ];

        $this->response($response);
    }


    public function normalUsers_post()
    {
        $data = $this->post();
        if(!isset($data['userID'])){
            $response = [
                "status" => 0,
                "message" => "User id is required",
                "data" => ''
            ];
        }elseif (!isset($data['type'])){
            $response = [
                "status" => 0,
                "message" => "Type is required",
                "data" => ''
            ];
        } else {
            $userID = $data['userID'];
            $type = $data['type'];
            try{
                if($type == 'package'){
                    $user_data = [
                        "id" => $userID
                    ];
                    $rowCount = $this->Owner_model->ownerLoginCount($user_data);
                    if($rowCount > 0){
                        $result=$this->Owner_model->ownerLogin($user_data);
                        $response = [
                            "status" => 1,
                            "message" => "Owner Details",
                            "data" => $result
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "No user found",
                            "data" => ''
                        ];
                        $this->response($response);
                    }
                }else{
                    $user_data = [
                        "id" => $userID
                    ];
                    $getPlacesCount=$this->Normal_model->getPlacesCount($userID);
                    if($getPlacesCount > 0){
                        $getPlaces = $this->Normal_model->getPlaces($userID);
                        foreach($getPlaces as $places){
                            $placesArray[]=$places['checkIn'];
                        }
                        $commaPlaces = implode(" - ",$placesArray);
                    }
                    $rowCount = $this->Normal_model->normalLoginCount($user_data);
                    if($rowCount > 0) {
                        $result = $this->Normal_model->normalUsersDetails($user_data);
                        if($commaPlaces === ''){
                            $commaPlaces = 'No places';
                        }
                        foreach($result as $res){
                            $rowArray['id'] = $res['id'];
                            $rowArray['userName'] = $res['userName'];
                            $rowArray['fullName'] = $res['fullName'];
                            $rowArray['phoneNumber'] = $res['phoneNumber'];
                            $rowArray['mostVisitedPlaces'] = $commaPlaces;
                            $rowArray['dis'] = $res['dis'];
                            $rowArray['profilePicture'] = $res['profilePicture'];
                        }
                        $response = [
                            "status" => 1,
                            "message" => "User Details",
                            "data" => $rowArray
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "No user found",
                            "data" => ''
                        ];
                    }
                }
            }catch(Exception $e){
                $response = [
                    "status" => 0,
                    "message" => "Error Occurred",
                    "data" => ''
                ];
            }
        }
        $this->response($response);
    }

    public function normalUserUpdate_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User id is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $profilePicture = isset($_FILES["profilePicture"]["name"])?$_FILES["profilePicture"]["name"]:'';
                if($profilePicture != ""){
                    $profilePicture = mt_rand().".jpg";
                    $data = array(
                        "id" => $userID
                    );
                    $Count = $this->Normal_model->normalUserPicture($data);
                    if($Count > 0){
                        $getPicture = $this->Normal_model->normalGetPicture($data);
                        $profile = $getPicture[0]['profilePicture'];
                        if($profile == ''){
                            $picture_data = array('profilePicture' => $profilePicture);
                            $updatePicture = $this->Normal_model->normalUpdatePicture($userID, $picture_data);
                            if($updatePicture == TRUE){
                                $move = move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/normal_user/profile/" . $profilePicture);
                                if($move == 1){
                                    $response_data = $this->Normal_model->userFullObj($userID);
                                    $response = [
                                        "status" => 1,
                                        "message" => "Profile Updated Successfully",
                                        "data" => $response_data
                                    ];
                                }else{
                                    $response = [
                                        "status" => 0,
                                        "message" => "Sorry! File was not moved",
                                        "data" => ''
                                    ];
                                }
                            }else{
                                $response = [
                                    "status" => 0,
                                    "message" => "Sorry! A problem Occurred, please try again",
                                    "data" => ''
                                ];
                            }
                        }else{
                            $path = "uploads/normal_user/profile/" . $profile;
                            if(is_file($path)){
                                $unlink = unlink($path);
                                if($unlink == 1){
                                    $link_data = array('profilePicture' => $profilePicture);
                                    $updatePicture = $this->Normal_model->normalUpdatePicture($userID, $link_data);
                                    if($updatePicture == TRUE){
                                        $move = move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/normal_user/profile/" . $profilePicture);
                                        if($move == 1){
                                            $response_data=$this->Normal_model->userFullObj($userID);
                                            $response = [
                                                "status" => 1,
                                                "message" => "Profile Updated Successfully",
                                                "data" => $response_data
                                            ];
                                        }else{
                                            $response = [
                                                "status" => 0,
                                                "message" => "Sorry! File was not moved",
                                                "data" => ''
                                            ];
                                        }

                                    }else{
                                        $response = [
                                            "status" => 0,
                                            "message" => "Sorry! profile picture wasn't successfully updated",
                                            "data" => ''
                                        ];
                                    }
                                }else{
                                    $response = [
                                        "status" => 0,
                                        "message" => "Trying to delete a non existing previous profile picture",
                                        "data" => ''
                                    ];
                                }
                            } else {
                                //no previous profile picture exist, just upload this
                                $profilePicture = mt_rand().".jpg";
                                $new_profile_picture = array('profilePicture' => $profilePicture);
                                $updatePicture = $this->Normal_model->normalUpdatePicture($userID, $new_profile_picture);
                                if($updatePicture == 1){
                                    $move = move_uploaded_file($_FILES["profilePicture"]["tmp_name"], getcwd() . "/uploads/normal_user/profile/" . $profilePicture);
                                    if($move == 1){
                                        $response_data = $this->Normal_model->userFullObj($userID);
                                        $response = [
                                            "status" => 1,
                                            "message" => "Profile Updated Successfully",
                                            "data" => $response_data
                                        ];
                                    }else{
                                        $response = [
                                            "status" => 0,
                                            "message" => "Sorry! File was not moved",
                                            "data" => ''
                                        ];
                                    }
                                } else {
                                    $response = [
                                        "status" => 0,
                                        "message" => "Profile picture update was not successful, please try again",
                                        "data" => ''
                                    ];
                                }
                            }
                        }
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "No user found",
                            "data" => ''
                        ];
                    }
                } else{
                    if(!isset($data['fullName'])){
                        $response = [
                            "status" => 0,
                            "message" => "Full Name is required",
                            "data" => ''
                        ];
                    } elseif(!isset($data['email'])){
                        $response = [
                            "status" => 0,
                            "message" => "Email is required",
                            "data" => ''
                        ];
                    } elseif (!isset($data['password'])){
                        $response = [
                            "status" => 0,
                            "message" => "Password is required",
                            "data" => ''
                        ];
                    } elseif (!isset($data['interest'])){
                        $response = [
                            "status" => 0,
                            "message" => "Interest is required",
                            "data" => ''
                        ];
                    } elseif (!isset($data['dis'])){
                        $response = [
                            "status" => 0,
                            "message" => "Dis is required",
                            "data" => ''
                        ];
                    } elseif (!isset($data['userName'])){
                        $response = [
                            "status" => 0,
                            "message" => "Username is required",
                            "data" => ''
                        ];
                    } else {
                        $fullName = $data['fullName'];
                        $email = $data['email'];
                        $password = $data['password'];
                        $interest = $data['interest'];
                        $userName = $data['userName'];
                        $dis = $data['dis'];
                        $response_data = array(
                            "fullName" => $fullName,
                            "email" => $email,
                            "password" => $password,
                            "intrest" => $interest,
                            "dis" => $dis,
                            "userName" => $userName
                        );
                        $updatePicture=$this->Normal_model->normalUpdate($userID, $response_data);
                        if($updatePicture == TRUE){
                            $result = $this->Normal_model->userFullObj($userID);
                            $response = [
                                "status" => 1,
                                "message" => "Profile Updated Successfully",
                                "data" => $result
                            ];
                        }else{
                            $response = [
                                "status" => 0,
                                "message" => "Sorry a problem occurred, please try again ",
                                "data" => ''
                            ];
                        }
                    }
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error occurred",
                "data" => ''
            ];

            $this->response($response);
        }
    }

    public function getNotification_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User id is required",
                    "data" => ''
                ];
            } else{
                $userID = $data['userID'];
                $count = $this->Normal_model->notificationsCount($userID);
                if($count>0){
                    $response_data = $this->Normal_model->notifications($userID);
                    if($response_data != ''){
                        $response = [
                            "status" => 1,
                            "message" => "All Notifications",
                            "data" => $response_data
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry! No Notification Found",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);

        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function addPackage_post()
    {
        $data = $this->post();
        $date = date('Y-m-d H:i:s');
        try{
            if(!isset($data['ownerID'])){
                $response = [
                    "status" => 0,
                    "message" => "Owner ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['nameOfClub'])){
                $response = [
                    "status" => 0,
                    "message" => "Name of club is required",
                    "data" => ''
                ];
            } elseif(!isset($data['price'])){
                $response = [
                    "status" => 0,
                    "message" => "Price is required",
                    "data" => ''
                ];
            } else if(!isset($data['description_attributes'])){
                $response = [
                    "status" => 0,
                    "message" => "Description Attributes is required",
                    "data" => ''
                ];
            } else if(!isset($data['validity'])){
                $response = [
                    "status" => 0,
                    "message" => "Validity is required",
                    "data" => ''
                ];
            } else if(!isset($data['discount'])){
                $response = [
                    "status" => 0,
                    "message" => "Discount is required",
                    "data" => ''
                ];
            } elseif(!isset($_FILES["image"]["name"])?$_FILES["image"]["name"]:''){
                $response = [
                    "status" => 0,
                    "message" => "Image is required",
                    "data" => ''
                ];
            } else {
                $ownerID = $data['ownerID'];
                $nameOfClub = $data['nameOfClub'];
                $price = $data['price'];
                $description_attributes = $data['description_attributes'];
                $validity = $data['validity'];
                $discount = $data['discount'];
                $image = isset($_FILES["image"]["name"])?$_FILES["image"]["name"]:'';
                $image = mt_rand().".jpg";

                $discountNumber = intval($discount);
                $packageImage = move_uploaded_file($_FILES["image"]["tmp_name"], getcwd() . "/uploads/owner_user/package/" . $image);
                if($packageImage == 1){
                    $package_data = array(
                        "ownerID" => $ownerID,
                        "nameOfClub" => $nameOfClub,
                        "price" => $price,
                        "description_attributes" => $description_attributes,
                        "validity" => $validity,
                        "discount" => $discountNumber."% off",
                        "image" => $image,
                        "date" => $date
                    );
                    $response_data = $this->Normal_model->addPackages($package_data);
                    if($response_data != '')
                    {
                        $getPackage=$this->Normal_model->GetPackages($response_data);
                        $response = [
                            "status" => 1,
                            "message" => "Package Successfully inserted.",
                            "data" => $getPackage
                        ];
                    }else{
                        $response = [
                            "status" => 0,
                            "message" => "Package not inserted.",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "Sorry there was a problem please try again later.",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    public function forgetPassword_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['email'])){
                $response = [
                    "status" => 0,
                    "message" => "Email is required",
                    "data" => ''
                ];
            } else{
                $email = $data['email'];
                $checkUser = array("email" => $email);
                $response_user = $this->Normal_model->CheckUserForget($checkUser);
                if($response_user > 0){
                    $getResponse = $this->Normal_model->getUser($checkUser);
                    $toName = $getResponse[0]['email'];
                    $message = "Hi, ".$getResponse[0]['fullName']." 
					We need to make sure you are human. Please Check your Password and get started using your Profile account on Nocturna.				
					Your ID: ".$getResponse[0]['id']."								
					And your Password is this for login your profile. Password: ".$getResponse[0]['password']." And Email is this. Email ".$getResponse[0]['email']."";

                    $this->email->from('Mailer@nocturna.com', 'Nocturna Mailer');
                    $this->email->to($toName);
                    $this->email->subject("Confirmation from Nocturna to ".$getResponse[0]['fullName']."");
                    $this->email->message($message);
                    $mailSent=$this->email->send();
                    if($mailSent != 1){
                        $response = [
                            "status" => 0,
                            "message" => "Sorry There is a problem",
                            "data" => ''
                        ];
                    }else{
                        $response = [
                            "status" => 1,
                            "message" => "Please check your email.",
                            "data" => ''
                        ];
                    }
                }else{
                    $response = [
                        "status" => 0,
                        "message" => "User does not exist",
                        "data" => ''
                    ];
                }
            }
            $this->response($response);
        }catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "Error Occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }


    //todo: Refactor this function to suite normal users
    public function saveQrCode_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } elseif(!isset($data['qrCode'])){
                $response = [
                    "status" => 0,
                    "message" => "QR code is required",
                    "data" => ''
                ];
            } else {
                $save_data = array();
                $save_data['owner_id'] = $data['userID'];
                $save_data['qrCode'] = $data['qrCode'];

                $check_user = array(
                    "id" => $data['userID']
                );
                $count = $this->Owner_model->ownerUserPicture($check_user);
                if($count > 0) {

                    //check if qr code exists
                    $check_qr = $this->Owner_model->savedQrCode($save_data);
                    if($check_qr > 0){

                        $response = [
                            "status" => 0,
                            "message" => "Invalid QR code, it has been scanned previously",
                            "data" => ''
                        ];

                    } else {
                        $save_details = $this->Owner_model->saveQRCode($save_data);
                        if($save_details == true){
                            $response = [
                                "status" => 1,
                                "message" => "Owner QR Code is successfully saved",
                                "data" => $data
                            ];
                        } else {
                            $response = [
                                "status" => 0,
                                "message" => "An error occurred while saving data, please try again",
                                "data" => ''
                            ];
                        }
                    }
                } else {
                    $response = [
                        "status" => 0,
                        "message" => "This user is not found",
                        "data" => ""
                    ];
                }
            }

            $this->response($response);
        } catch(Exception $e){
            $response = [
                "status" => 0,
                "message" => "An error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

    //todo: Refactor this function to suite normal users
    public function fetchQrCode_post()
    {
        $data = $this->post();
        try{
            if(!isset($data['userID'])){
                $response = [
                    "status" => 0,
                    "message" => "User ID is required",
                    "data" => ''
                ];
            } else {
                $check_user = array(
                    "id" => $data['userID']
                );
                $count = $this->Owner_model->ownerUserPicture($check_user);
                if($count > 0){
                    $id = $data['userID'];
                    $fetchAll = $this->Owner_model->getQrCode($id);
                    $response = [
                        "status" => 1,
                        "message" => "Owner Qr Codes",
                        "data" => $fetchAll
                    ];
                } else {
                    $response = [
                        "status" => 0,
                        "message" => "No user found",
                        "sata" => ''
                    ];
                }
            }
            $this->response($response);
        } catch (Exception $exception){
            $response = [
                "status" => 0,
                "message" => "An error occurred",
                "data" => ''
            ];
            $this->response($response);
        }
    }

}