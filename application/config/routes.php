<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = "normal/index";

//Owner Controller Endpoints
$route['owner/index'] = "owner/index";
$route['owner/signup'] = "owner/signUp";
$route['owner/login'] = "owner/login";
$route['user/feedback'] = "owner/feedback";
$route['owner/editProfile'] = "owner/ownerEditProfile";
$route['owner/saveQr'] = "owner/saveQrCode";
$route['owner/getQr'] = "owner/fetchQrCode";

//Normal Controller Endpoint
$route['normal/index'] = "normal/index";
$route['normal/signup'] = "normal/signUp";
$route['normal/login'] = "normal/login";
$route['normal/production'] = "normal/production";
$route['normal/profile'] = "normal/normalUsers";
$route['normal/update'] = "normal/normalUserUpdate";
$route['normal/forgotPassword'] = "normal/forgetPassword";

//Post Controller Endpoint
$route['post/index'] = "post/index";
$route['post/status'] = "post/getStatus";
$route['post/like'] = "post/like";
$route['news/feed'] = "post/newsFeed";
$route['post/allComment'] = "post/getAllComments";
$route['post/addComment'] = "post/newComment";
$route['post/delete'] = "post/deletePost";
$route['post/update'] = "post/updatePost";

//Friends Controller Endpoint
$route['friends/index'] = "friend/index";
$route['friend/fetchall'] = "friend/getFriendList";
$route['friend/find'] = "friend/findFriend";
$route['friend/new'] = "friend/newFriend";
$route['friend/accept/remove'] = "friend/acceptRemoveFriend";

//Club Controller Endpoint
$route['club/search'] = "club/search";
$route['club/near'] = "club/nearClub";
$route['club/package'] = "club/getClubPackages";


//Package Controller Endpoint
$route['package/index'] = "package/index";
$route['package/purchase'] = "package/packagePurchase";
$route['package/cartPurchased'] = "package/cartPurchasedPackage";
$route['package/splitData'] = "package/splitData";
$route['package/visitedPlaces'] = "package/visitedPlaces";
$route['package/delete'] = "package/deletePackage";
$route['package/update'] = "package/updatePackage";
$route['split/remove'] = "package/splitApprove";

//Notification Endpoint
$route['normal/notification'] = "normal/getNotification";
$route['package/add'] = "normal/addPackage";
$route['device/token'] = "owner/updateTokenDevice";

//Cart Controller Endpoints
$route['cart/index'] = "cart/index";
$route['cart/add'] = "cart/addMyCart";
$route['cart/split'] = "cart/splitApprove";
$route['cart/get'] = "cart/getMyCart";
$route['cart/remove'] = "cart/cartPackageRemove";
$route['cart/availPackage'] = "cart/availablePackage";
$route['cart/purchase'] = "cart/purchaseCart";

//Audio endpoint
$route['uploadAudio'] = "owner/uploadAudio";


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
